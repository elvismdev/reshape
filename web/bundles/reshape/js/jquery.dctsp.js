/**
 * jCarouselLite - jQuery plugin to navigate images/any content in a carousel style widget.
 * @requires jQuery v1.2 or above
 * @cat Plugins/Image Gallery
 * @author Ganeshji Marwaha/ganeshread@gmail.com
 */

(function($) {                                          // Compliant with jquery.noConflict()
$.fn.jCarouselLite = function(o) {
    o = $.extend({
        btnPrev: null,
        btnNext: null,
        btnGo: null,
        mouseWheel: false,
        auto: null,
        speed: 200,
        easing: null,
        vertical: false,
        circular: true,
        visible: 3,
        start: 0,
        scroll: 1,

        beforeStart: null,
        afterEnd: null
    }, o || {});

    return this.each(function() {                           // Returns the element collection. Chainable.

		var running = false, animCss=o.vertical?"top":"left", sizeCss=o.vertical?"height":"width";
        var div = $(this), ul = $(".carousel > ul", div), tLi = $("> li", ul), tl = tLi.size(), v = o.visible;
		var w = div.width();
		
        if(o.circular) {
            ul.prepend(tLi.slice(tl-v-1+1).clone())
              .append(tLi.slice(0,v).clone());
            o.start += v;
        }
		
        var li = $("> li", ul), itemLength = li.size(), curr = o.start;
        div.css("visibility", "visible");

        li.css({overflow: "hidden", float: o.vertical ? "none" : "left"});
        ul.css({margin: "0", padding: "0", position: "relative", "list-style-type": "none", "z-index": "1"});
        div.css({overflow: "hidden", position: "relative", "z-index": "2", left: "0px"}).addClass('dctsp-active');
		
		var p = parseInt(curr-1);
		var n = parseInt(curr+1);
		var iconp = $('.dctsp-icon:eq('+p+')',ul);
		var iconn = $('.dctsp-icon:eq('+n+')',ul);
		$('.btn-carousel.prev').html(iconp.clone());
		$('.btn-carousel.next').html(iconn.clone());

        var liSize = o.vertical ? height(li) : w;   // Full li size(incl margin)-Used for animation
        var ulSize = liSize * itemLength;                   // size of full ul(total length, not just for the visible items)
        var divSize = liSize * v;                           // size of entire div(total length for just the visible items)

        li.css({width: w+'px', height: li.height()});
        ul.css(sizeCss, ulSize+"px").css({height: li.height()}).css(animCss, -(curr*liSize));

        div.css(sizeCss, divSize+"px");                     // Width of the DIV. length of visible images
		
        if(o.btnPrev)
            $(o.btnPrev).click(function() {
                return go(curr-o.scroll);
            });

        if(o.btnNext)
            $(o.btnNext).click(function() {
                return go(curr+o.scroll);
            });

        if(o.btnGo)
            $.each(o.btnGo, function(i, val) {
                $(val).click(function() {
                    return go(o.circular ? o.visible+i : i);
                });
            });

        if(o.mouseWheel && div.mousewheel)
            div.mousewheel(function(e, d) {
                return d>0 ? go(curr-o.scroll) : go(curr+o.scroll);
            });

        if(o.auto)
            setInterval(function() {
                go(curr+o.scroll);
            }, o.auto+o.speed);

        function vis() {
            return li.slice(curr).slice(0,v);
        };

        function go(to) {
            if(!running) {

                if(o.beforeStart)
                    o.beforeStart.call(this, vis());

                if(o.circular) {            // If circular we are in first or last, then goto the other end
                    if(to<=o.start-v-1) {           // If first, then goto last
                        ul.css(animCss, -((itemLength-(v*2))*liSize)+"px");
						
						var p = parseInt(1);
						var n = parseInt(itemLength);

                        // If "scroll" > 1, then the "to" might not be equal to the condition; it can be lesser depending on the number of elements.
                        curr = to==o.start-v-1 ? itemLength-(v*2)-1 : itemLength-(v*2)-o.scroll;
                    } else if(to>=itemLength-v+1) { // If last, then goto first
                        ul.css(animCss, -( (v) * liSize ) + "px" );
						
						var p = parseInt(itemLength-1);
						var n = parseInt(1);
		
                        // If "scroll" > 1, then the "to" might not be equal to the condition; it can be greater depending on the number of elements.
                        curr = to==itemLength-v+1 ? v+1 : v+o.scroll;
                    } else curr = to;
					
						var p = parseInt(curr-1);
						var n = parseInt(curr+1);
				
                } else {                    // If non-circular and to points to first or last, we just return.
                    if(to<0 || to>itemLength-v) return;
                    else curr = to;
                }                           // If neither overrides it, the curr will still be "to" and we can proceed.
				
                if(curr==(itemLength-1)){
					//var p = parseInt(itemLength-2);
					var n = parseInt(2);
				} else if(curr==0){
					var p = parseInt(itemLength-3);
					//var n = parseInt(2);
				}
			//	alert(curr+' - '+itemLength+' - '+p+' - '+n);
				var iconp = $('.dctsp-icon:eq('+p+')',ul);
				var iconn = $('.dctsp-icon:eq('+n+')',ul);
				$('.btn-carousel.prev').html(iconp.clone());
				$('.btn-carousel.next').html(iconn.clone());
				
				running = true;

                ul.animate(
                    animCss == "left" ? { left: -(curr*liSize) } : { top: -(curr*liSize) } , o.speed, o.easing,
                    function() {
                        if(o.afterEnd)
                            o.afterEnd.call(this, vis());
                        running = false;
                    }
                );
				
                // Disable buttons when the carousel reaches the last/first, and enable when not
                if(!o.circular) {
                    $(o.btnPrev + "," + o.btnNext).removeClass("disabled");
                    $( (curr-o.scroll<0 && o.btnPrev)
                        ||
                       (curr+o.scroll > itemLength-v && o.btnNext)
                        ||
                       []
                     ).addClass("disabled");
                }

            }
            return false;
        };
    });
};

function css(el, prop) {
    return parseInt($.css(el[0], prop)) || 0;
};
function height(el) {
    return el[0].offsetHeight + css(el, 'marginTop') + css(el, 'marginBottom');
};

})(jQuery);

/*
 * DC TSP Accordion
 * Copyright (c) 2011 Design Chemical
 * 	http://www.designchemical.com
 */
 
(function($) {
	$.fn.dctspAccordion = function(options) {
		
	var defaults = {			
		autoClose: 1,
		content: '.dctsp-posts',
		active: 'active'
	};
		
	this.each(function() {
		
		var obj = $(this);
		var o = $.extend(defaults, options);
		
		accordion = {
				init: function(){
					accordion.setup();
					accordion.clicked();
				},
				setup: function(){
					$('h3',obj).each(function(){
						var sub = $(this).next(o.content);
						if(!$('li',sub).length){
							$(this).hide();
							$(sub).hide();
						}
					});
				},
				clicked: function(){
					nav = $('h3 a',obj);
					nav.click(function(e){
						if($(this).hasClass(o.active))
						{
							$(this).removeClass(o.active);
							accordion.close();
						}
						else
						{
							if(o.autoClose == 1){
								$('h3 a',obj).removeClass(o.active);
								accordion.close();
							}
							$(this).addClass(o.active).parent().next(o.content).slideToggle();
						}
						e.preventDefault();
					});
				},
				close: function(){
					$(o.content,obj).slideUp();
				}
			}
			accordion.init();
		});
	};
})(jQuery);

jQuery(document).ready(function($) {
	$(".carousel").each(function(){
		var w = $(".carousel").outerWidth();
		var h = findMaxValue($("> li",this));
		$(this).css({height: h+"px", width: w+"px"});
		$('.carousel-slide',this).css({height: h+"px", width: w+"px"});
	});
	$('.dctsp-thumb img').removeAttr('height').attr('width','40');
});
function findMaxValue(element){
    var maxValue = undefined;
    jQuery(element).each(function(){
        var val = parseInt(jQuery(this).height());
        if (maxValue === undefined || maxValue < val) {
            maxValue = val;
        }
    });
    return maxValue;
}