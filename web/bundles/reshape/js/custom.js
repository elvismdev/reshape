$(document).ready(function () {
    //countryState();

    // The maximum number of options
    var MAX_OPTIONS = 5;
    var count = $('#count').val();

    $('#editForm')
        // Add button click handler
        .on('click', '.addButton', function () {
          var $template = $('#div' + $(this).attr('id')),
          $clone = $template
          .clone()
          .addClass('time')
          .removeClass('hide')
          .removeAttr('id')
          .insertBefore($template),
          $option = $clone.find('[name="option[]"]');

          count++;
          $('#count').val(count);
            // Add new field
            $('#surveyForm').bootstrapValidator('addField', $option);

            $('.time').find('.dpd1').addClass('dpd'+$('#count').val()+'-1');
            $('.time').find('.dpd1').removeClass('dpd1');
            $('.time').find('.dpd2').addClass('dpd'+$('#count').val()+'-2');
            $('.time').find('.dpd2').removeClass('dpd2');

            calendar($('#count').val());
          })

        // Remove button click handler
        .on('click', '.removeButton', function () {
          var $row = $(this).parents('.form-group'),
          $option = $row.find('[name="option[]"]');

            // Remove element containing the option
            $row.remove();

            // Remove field
            $('#surveyForm').bootstrapValidator('removeField', $option);
          })

        // Called after adding new field
        .on('added.field.bv', function (e, data) {
            // data.field   --> The field name
            // data.element --> The new field element
            // data.options --> The new field options

            if (data.field === 'option[]') {
              if ($('#surveyForm').find(':visible[name="option[]"]').length >= MAX_OPTIONS) {
                $('#surveyForm').find('.addButton').attr('disabled', 'disabled');
              }
            }
          })

        // Called after removing the field
        .on('removed.field.bv', function (e, data) {
          if (data.field === 'option[]') {
            if ($('#surveyForm').find(':visible[name="option[]"]').length < MAX_OPTIONS) {
              $('#surveyForm').find('.addButton').removeAttr('disabled');
            }
          }
        });

    // Calendar for Birth Date field
    $('#dpYears').datepicker();

    // Redirect to requested Second TAB Default
    $('#Tab2').trigger('click');

    // Bootstrap form wizard
    $('#rootwizard').bootstrapWizard({onNext: function (tab, navigation, index) {
        // if(index==2) {
        //     // Make sure we entered the name
        //     if(!$('#name').val()) {
        //       alert('You must enter your name');
        //       $('#name').focus();
        //       return false;
        //     }
        //   }

        // Set the name for the next tab
        // $('#tab3').html('Hello, ' + $('#name').val());

      }, onTabShow: function (tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;
        var $percent = ($current / $total) * 100;
        $('#rootwizard').find('.bar').css({width: $percent + '%'});

        if ($current >= $total) {
          $('#rootwizard').find('.pager .next').hide();
          $('#rootwizard').find('.pager .finish').show();
          $('#rootwizard').find('.pager .finish').removeClass('disabled');
        } else {
          $('#rootwizard').find('.pager .next').show();
          $('#rootwizard').find('.pager .finish').hide();
        }
      }});

// Serialize form update/save when clicked next button
$("#editForm").submit(function (e) {
  e.preventDefault();
  var $url = $(this).attr('action');
  var $data = $("#editForm :input[value!='']").serialize();
  $.ajax({
    type: "POST",
    url: $url,
    data: $data
  }).done(function (result) {
    if (result.success) {
    }
  });
});

$('.next').click(function () {
  finish = 0;
  if ($(this).attr('id') == 'finish') {
    finish = 1;
  }
  $('#currentstep').val(finish);
  $('#editForm').submit();
});

    //Datepickers in calendar
    calendar(c);

    //called when key is pressed in textbox
    $("#ninerlabs_reshapebundle_professionaluser_zip").keypress(function (e) {

     if ($('#ninerlabs_reshapebundle_professionaluser_country').val() != 'US1' && $('#ninerlabs_reshapebundle_professionaluser_country').val() != 'US')
        return true;

     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
        return false;
      }
    });


    //Disable State DropDown if country is USA
    $("#ninerlabs_reshapebundle_professionaluser_country").change(function() {
      countryState();
    });

  });

function countryState() {
    if ($('#ninerlabs_reshapebundle_professionaluser_country').val() == 'US1' || $('#ninerlabs_reshapebundle_professionaluser_country').val() == 'US') {
        $('#ninerlabs_reshapebundle_professionaluser_state').prop('disabled', false);
        $('#ninerlabs_reshapebundle_professionaluser_zip').val('');
    }
    else
        $('#ninerlabs_reshapebundle_professionaluser_state').prop('disabled', true);
}

function calendar(c) {
  if (!c)
    c = 0;

    if (c > 0) {
        for (i = 1; i <=c; i++) {
            applyCalendar(i);
        }
    } else {
        applyCalendar(c);
    }
}

function applyCalendar(c) {
    // Calendars in profession dynamic fields
    var checkin = $('.dpd'+c+'-1').datepicker({
        onRender: function (date) {
            return '';
        }
    }).on('changeDate',function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('.dpd'+c+'-2')[0].focus();
        }).data('datepicker');
    var checkout = $('.dpd'+c+'-2').datepicker({
        onRender: function (date) {
            //return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            return '';
        }
    }).on('changeDate',function (ev) {
            checkout.hide();
        }).data('datepicker');
}