$(document).ready(function () {
  $('#registerForm').bootstrapValidator({
    fields: {
      'ninerlabs_reshapebundle_professionaluser[firstName]': {
        container: '#firstNameMessage',
        validators: {
          notEmpty: {
            message: 'First Name is required and cannot be empty'
          }
        }
      },
      'ninerlabs_reshapebundle_professionaluser[lastName]': {
        container: '#lastNameMessage',
        validators: {
          notEmpty: {
            message: 'Last Name is required and cannot be empty'
          }
        }
      },
      'ninerlabs_reshapebundle_professionaluser[email]': {
        container: '#emailFieldMessage',
        validators: {
          notEmpty: {
            message: 'E-Mail is required and cannot be empty'
          }
        }
      },
      'ninerlabs_reshapebundle_professionaluser[password][first]': {
        container: '#passFieldMessage',
        validators: {
          notEmpty: {
            message: 'Password is required and cannot be empty'
          }
        }
      },
      'ninerlabs_reshapebundle_professionaluser[password][second]': {
        container: '#secondpassFieldMessage',
        validators: {
          notEmpty: {
            message: 'Re-enter Password is required and cannot be empty'
          }
        }
      },
      'ninerlabs_reshapebundle_professionaluser[tc]': {
        container: '#tcFieldMessage',
        validators: {
          notEmpty: {
            message: 'Terms and Conditions must be accepted'
          }
        }
      }
    }
  });
});
