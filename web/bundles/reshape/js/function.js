	/*
	Theme Name: The Wall
	Version: 1.0
	Author: seven media
	
	This theme was designed and built by seven media,
	http://themeforest.net/user/sevenmedia
	in Themeforest
	*/
	
	/*Preloader*/
	$(window).load(function() {
	'use strict';
    $('#status').fadeOut();
    $('#preloader').delay(350).fadeOut('slow'); <!--fade in preloader 350ms -->
    $('body').delay(350).css({'overflow':'visible'}); <!--fade in site 350ms-->
    })
	
	/*pretty Photo Function / Reset*/
	$(document).ready(function(){
	'use strict';
    $("area[rel^='prettyPhoto']").prettyPhoto();
    $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',slideshow:8000, autoplay_slideshow: false}); <!--speed intervall-->
    $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',slideshow:3000, hideflash: true}); <!--speed open/close-->
    });