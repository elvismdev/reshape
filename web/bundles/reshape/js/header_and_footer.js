var header_img = null;
var footer_img = null;

var mainWidth = 900;
var headerAndFooterImageWidth = 900;
var windowW = 0;
var windowH = 0;

var resizeHandlerId_to;
var removeIcons_to;


//##################################//
/* initialize page */
//##################################//
function init(){

	header_img = document.getElementById("headerImage");
	footer_img = document.getElementById("footerImage");
	positionStuff();

	if(window.addEventListener){
		window.addEventListener("resize", onResizeHandler);
	}else if(window.attachEvent){
		window.attachEvent("onresize", onResizeHandler);
	}
	
	setTimeout( function(){
		positionStuff();
		}, 50);
}

//#####################################//
/* resize handler */
//#####################################//
function onResizeHandler(){
	if(FWDUtils.isMobile){
		clearTimeout(resizeHandlerId_to); 
		resizeHandlerId_to = setTimeout(positionStuff, 90);
	}else{
		positionStuff();
		clearTimeout(resizeHandlerId_to); 
		resizeHandlerId_to = setTimeout(positionStuff, 90);
	}
}

//#####################################//
/* position stuff */
//#####################################//
function positionStuff(){
	windowW = document.documentElement.clientWidth || window.innerWidth;
	windowH = document.documentElement.clientHeight || window.innerHeight;
	var headerAndFooterImageX = parseInt((windowW - headerAndFooterImageWidth)/2);
	
	header_img.style.left = headerAndFooterImageX  + "px";
	footer_img.style.left = headerAndFooterImageX  + "px";
}
