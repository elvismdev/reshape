// JavaScript Document


var videoInitIndex = { };

$(document).ready(function() {
	
	$(".carouselContent").attr("alreadyLoaded", 0);
	$('.carouselContent').roundabout({
		childSelector: ".teaser",
		btnNext: ".right",
		btnPrev: ".left",
		enableDrag: true,
		dropAnimateTo: "nearest",
		minOpacity:1.0,
		minScale: 0.01,
	});
	
	$(".teaser, .right, .left").click(function(e) {
		window.setTimeout(fixReflection,610);
			
	});
	
	$(".contentContainer:first").click();
	
	function fixReflection(){
			
			$(".reflection").removeAttr("style");
			/* CALC OF NEW SIZE IF NEEDED
			$(".reflection").each(function(index, element) {
			
				var dimensionCanvas = 54/588;
				var el = $(this).parent(".contentContainer");
				var newWidth = parseInt(el.width());
				var newHeight = parseInt(newWidth*dimensionCanvas, 0);
				
				var orgWidth = $(this).children("canvas").width();
				var orgHeight = $(this).children("canvas").height();
				
				var scale = parseInt(orgHeight/newHeight,0);
				
				//alert("nW:" + newWidth + " nH:"+ newHeight +" orgW:"+ orgWidth+" orgH: "+orgHeight+" Scale: " + scale);
				
				//Update canvas width and height
				$(this).children("canvas").attr("style", "transform:scale("+ scale +", "+scale+"); ms-transform:scale("+ scale +", "+scale+"); -webkit-transform:scale("+ scale +", "+scale+");");
			});
			*/
	}
	
	//VIDEO
	
	$(".teaser, .right, .left").click(function(e) {
		
		//Video Controls
		var videoElementContainer = $(".videoPlayerContainer:visible");
		
		if(videoInitIndex["idHere"] == true && $(this).is(".teaser") == true && $(this).hasClass("roundabout-in-focus") == true)
		{
			//If click on playing youtube video
		}
		else if(videoInitIndex["idHere"] == true && videoElementContainer.hasClass("roundabout-in-focus") == false && videoElementContainer.length > 0)
		{
			videoInitIndex["idHere"] = false;	
			videoElementContainer.hide();
			videoElementContainer.parent(".video").children(".videoThumb").show();
		}
		
	});
	
	$(".video").click(function(e) {
		if($(this).parent(".teaser").hasClass("roundabout-in-focus") == true){
			$(this).children(".videoThumb").hide();
			$(this).children(".videoPlayerContainer").show();
			
			
		}
	});
	
	window.onYouTubePlayerReady = function (playerId) {
		ytplayer = document.getElementById(playerId);
		ytplayer.playVideo();
		videoInitIndex["idHere"] = true;
	};	
	
	/******************************** If a section meet trainer is available update text ********************************/
	var firstTeaser = $(".teaser:first");
	
	if(firstTeaser.length > 0){
		
		var standardText = firstTeaser.attr("text");
		var standardHeading = firstTeaser.attr("header");
		
		$(".meetTrainer .header").html(standardHeading);
		$(".meetTrainer .text").html(standardText);
		
		$(".teaser, .right, .left").click(function(e) {
			setTimeout(carouselCallBack,700);
		});
	}
	
	function carouselCallBack(){
		var teaser = $(".roundabout-in-focus");
			
		var text = String(teaser.attr("text"));
		var heading = String(teaser.attr("header"));
		
		var firstTeaser = $(".teaser:first");
		var standardText = firstTeaser.attr("text");
		var standardHeading = firstTeaser.attr("header");
		
		if(text == "undefined" || text.length == 0){
			var text = standardText;
		}
		if(heading == "undefined" || heading.length == 0){
			var heading = standardHeading;
		}
		
		$(".meetTrainer .header").html(heading);
		$(".meetTrainer .text").html(text);
	}
	
	/******************************** If a section meet trainer is available update text END ********************************/	
});




	 




