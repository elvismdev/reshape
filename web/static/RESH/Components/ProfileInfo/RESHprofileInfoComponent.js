$(document).ready(function(e) {
    $(".tabsList li").click(function (e)
    {

	    var this_num = $(this).attr("num");
	    var id = $(".tabContainer[num='" + this_num + "'] iframe").attr("id");

	    if (id)
	    {
	        var iframeEl = document.getElementById(id);
	        var prevSrc = $(".tabContainer[num='" + this_num + "']").attr("data-start-url");
	        iframeEl.src = "";
	        iframeEl.src = prevSrc + "?" + Math.random();

	        document.getElementById("iframeAppendDiv").setAttribute( "data-cur-iframe", id );
	    }

		$(".tabsList li").removeClass("active");
		$(this).addClass("active");
		$(".tabContainer").hide();
		$(".tabContainer[num='"+ this_num +"']").fadeIn();
	});
});