﻿
function CheckSetup()
{
    var os            = new OS();
    var browser       = new Browser();
    var internetSpeed = new InternetSpeed();

    

}

window.Browser = function ()
{
    this.Name         = null;
    this.IsValid      = false;
    this.MajorVersion = null;
    this.FullVersion  = null;

    this.Init = function ()
    {
        this.GetBrowserDetails();
        this.IsValid = this.GetIsValid();
    };

    this.GetIsValid()
    {
        if (this.Name == "Chrome" || this.Name == "Safari" || this.Name == "Microsoft Internet Explorer" || this.Name == "Firefox")
            return true;

        else return false;
    };

    this.GetBrowserDetails = function ()
    {
        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browserName = navigator.appName;
        var fullVersion = '' + parseFloat(navigator.appVersion);
        var majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;

        // In Opera, the true version is after "Opera" or after "Version"
        if ((verOffset = nAgt.indexOf("Opera")) != -1)
        {
            browserName = "Opera";
            fullVersion = nAgt.substring(verOffset + 6);
            if ((verOffset = nAgt.indexOf("Version")) != -1)
                fullVersion = nAgt.substring(verOffset + 8);
        }
            // In MSIE, the true version is after "MSIE" in userAgent
        else if ((verOffset = nAgt.indexOf("MSIE")) != -1)
        {
            browserName = "Microsoft Internet Explorer";
            fullVersion = nAgt.substring(verOffset + 5);
        }
            // In Chrome, the true version is after "Chrome" 
        else if ((verOffset = nAgt.indexOf("Chrome")) != -1)
        {
            browserName = "Chrome";
            fullVersion = nAgt.substring(verOffset + 7);
        }
            // In Safari, the true version is after "Safari" or after "Version" 
        else if ((verOffset = nAgt.indexOf("Safari")) != -1)
        {
            browserName = "Safari";
            fullVersion = nAgt.substring(verOffset + 7);
            if ((verOffset = nAgt.indexOf("Version")) != -1)
                fullVersion = nAgt.substring(verOffset + 8);
        }
            // In Firefox, the true version is after "Firefox" 
        else if ((verOffset = nAgt.indexOf("Firefox")) != -1)
        {
            browserName = "Firefox";
            fullVersion = nAgt.substring(verOffset + 8);
        }
            // In most other browsers, "name/version" is at the end of userAgent 
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) <
                  (verOffset = nAgt.lastIndexOf('/')))
        {
            browserName = nAgt.substring(nameOffset, verOffset);
            fullVersion = nAgt.substring(verOffset + 1);
            if (browserName.toLowerCase() == browserName.toUpperCase())
            {
                browserName = navigator.appName;
            }
        }
        // trim the fullVersion string at semicolon/space if present
        if ((ix = fullVersion.indexOf(";")) != -1)
            fullVersion = fullVersion.substring(0, ix);
        if ((ix = fullVersion.indexOf(" ")) != -1)
            fullVersion = fullVersion.substring(0, ix);

        majorVersion = parseInt('' + fullVersion, 10);
        if (isNaN(majorVersion))
        {
            fullVersion = '' + parseFloat(navigator.appVersion);
            majorVersion = parseInt(navigator.appVersion, 10);
        }

        this.Name         = browserName;
        this.FullVersion  = fullVersion;
        this.MajorVersion = majorVersion;

    };

    this.Init();
};


window.OS = function ()
{
    this.OS = null;
    this.IsValid = null;

    this.Init = function ()
    {
        this.OS = this.GetOperatingSystem();
        this.IsValid = this.GetIsValid();
    };

    this.GetIsValid = function ()
    {
        var os = GetOperatingSystem();

        if (os == "Windows" || os == "MacOS")
            return true;

        else return false;
    };


    this.GetOperatingSystem = function ()
    {

        // This script sets OSName variable as follows:
        // "Windows"    for all versions of Windows
        // "MacOS"      for all versions of Macintosh OS
        // "Linux"      for all versions of Linux
        // "UNIX"       for all other UNIX flavors 
        // "Unknown OS" indicates failure to detect the OS

        var OSName = "Unknown OS";
        if (navigator.appVersion.indexOf("Win") != -1) OSName = "Windows";
        if (navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
        if (navigator.appVersion.indexOf("X11") != -1) OSName = "UNIX";
        if (navigator.appVersion.indexOf("Linux") != -1) OSName = "Linux";

        return OSName;
    };

    this.Init();
};

window.InternetSpeed = function ()
{
    this.SpeedMbps = null;
    this.IsValid   = null;

    this.Init = function ()
    {
        this.GetSpeed();
        this.IsValid = this.GetIsValid();
    };

    this.GetIsValid = function ()
    {
        if (this.SpeedMbps >= 1.2)
            return true;

        else return false;
    };

    this.GetSpeed = function()
    {
        var imageAddr = "http://static.reshape.net/RESH/Images/Admin/signInBackground.jpg" + "?n=" + Math.random();
        var startTime, endTime;
        var downloadSize = 396978;
        var download = new Image();
        download.onload = function () {
            endTime = (new Date()).getTime();
            showResults();
        }
        startTime = (new Date()).getTime();
        download.src = imageAddr;

        var duration = (endTime - startTime) / 1000;
        var bitsLoaded = downloadSize * 8;
        var speedBps = (bitsLoaded / duration).toFixed(2);
        var speedKbps = (speedBps / 1024).toFixed(2);
        var speedMbps = (speedKbps / 1024).toFixed(2);
        
        this.SpeedMbps = speedMbps;
    };

    this.Init();
};