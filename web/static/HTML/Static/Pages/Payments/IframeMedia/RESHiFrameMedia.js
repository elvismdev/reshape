﻿window.UpdateIframeAttr = function ()
{
    var iframeAppendDiv = parent.document.getElementById("iframeAppendDiv");

    iframeAppendDiv.setAttribute("data-last-iframe-url", iframeAppendDiv.getAttribute("data-cur-iframe-url"));
    iframeAppendDiv.setAttribute("data-cur-iframe-url", this.location);
};

window.onload = function ()
{
    UpdateIframeAttr();
}