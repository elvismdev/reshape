﻿$(document).ready(function ()
{
    $(".confirmBtn").click(function ()
    {
        var parentEl = $(this).closest(".payment-booking-page");

        var amount = parentEl.find(".amount-input-container input").val();

        if ( !isNaN(parseFloat(amount)) && isFinite(amount) )
        {
            parentEl.find("input[type='hidden'][class='value']").val(amount);
            parentEl.find("form").submit();
        }
        else
            alert("Error: No valid amount");
    });
});