// JavaScript Document

$(document).ready(function(e) {
	$(".connectWithTrainer .profileButtons li:has(div)").mouseenter(function(){
	    var num        = $(this).attr("num");
		var curVisible = $(".connectExplain:visible").attr("num");
		var iframeUrl  = $(this).attr("data-iframe-url");

		if (num != curVisible)
		{
			$(".connectExplain").hide();
			$(".connectExplain[num='"+ num +"']").fadeIn("slow");
		}

		if (iframeUrl)
		{
		    var iframeEl = document.getElementById("iframe-service");
		    iframeEl.style.display = "block";
		    iframeEl.style.opacity = "0";
		    iframeEl.src = iframeUrl;

		    iframeEl.onload = function ()
		    {
		        this.style.opacity = "1";
		    };
            
		}
		
	});
	
	$(".connectWithTrainer .profileButtons li[num]").mouseenter(function(e) {
		$(".connectWithTrainer .profileButtons li[num]").removeClass("buttonSelected");
        $(this).addClass("buttonSelected");
	});


	$(document).on("click", ".singleMediaView .bg, .singleMediaView .close", function ()
	{
	    closeFullImage( );
	});

	$(document).keyup(function (e)
	{
	    if (e.keyCode == 27) // esc
	    {
	        closeFullImage();
	    }   
	});

	function closeFullImage()
	{
	    $("#iframeAppendDiv").html("");
	}

});