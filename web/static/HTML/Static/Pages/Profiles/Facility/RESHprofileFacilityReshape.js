﻿$("document").ready(function ()
{
    $(".link").click(function ()
    {
        var id = $(this).attr("data-id");
        var name = $(this).html();
        $(".iframes-container").fadeIn();

        $(".iframes-container .heading").html(name);
        $(".iframe-container").hide();
        $(".iframe-container[data-id='" + id + "']").show();
    });

    $(".close").click(function (e)
    {
        $(".iframes-container").fadeOut();
    });

    $(".iframe-back").click(function (e)
    {
        var el = $(".iframe-container:visible iframe").get(0);
        if (el)
            el.contentWindow.history.back();
    });

    function IframeBackButtonShowOrHide()
    {
        var el = $(".iframe-container:visible iframe").get(0);
        if (el)
        {
            var historyLenght = el.contentWindow.history.length;

            if (historyLenght > 0)
                $(".iframe-back").show();
            else
                $(".iframe-back").hide();
        }
        else
            $(".iframe-back").hide();

    }

    //setInterval(IframeBackButtonShowOrHide, 500);

    if ( window.location.hash )
    {

        var hash = window.location.hash.substring(1);
        var id   = "";

        if (hash == "classes")
            id = "1";
        else if ( hash == "schedule" )
            id = "2";
        else if ( hash == "events" )
            id = "3";
        else if ( hash == "team" )
            id = "4";
        else if ( hash == "contact" )
            id = "5";

        if (id != "")
        {
            $(".iframes-container").fadeIn();
            $(".iframes-container .heading").html(hash.toUpperCase());
            $(".iframe-container[data-id='" + id + "']").show();
            
        }
    }

    function UpdateIframesLink()
    {
        console.log("IframeUpdateLink called");
        $(".iframeLink[data-link]").each(function (index, val)
        {
            var el = $(this);
            var link = el.attr("data-link");
            var text = el.html();
            console.log( link + "applied");
            el.html("<a href='" + link + "'>" + text + "</a>");
        });
    }

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
    {
        console.log("Tablet");
        UpdateIframesLink();
        
    }

    else if(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
        console.log("Safari");
        UpdateIframesLink();
    }



});