﻿$(document).ready(function ()
{
    $(".session-container .icon[data-id]").click(function ()
    {
        var dataId   = $(this).attr("data-id");
        var parentEl = $(this).closest(".session-container");

        $(".session-container .expandable:visible").slideUp();

        var el = parentEl.find(".expandable[data-id='" + dataId + "']");
        if( el.is(":visible") )
            el.slideUp();
        else 
            el.slideDown();
    });

    $(".change-view").click(function ()
    {
        if ($(this).hasClass("open-all-session-containers"))
        {
            $(".session-container").addClass("open");
            changeText($(this));
            $(this).addClass("close-all-session-containers").removeClass("open-all-session-containers");
        }
        else
        {
            hideAllForms();
            CloseAllLists();
            changeText($(this));
            $(this).removeClass("close-all-session-containers").addClass("open-all-session-containers");
        }

    });

    function changeText(el)
    {
        var newText = el.attr("data-alt-text");

        if (newText.length > 0 )
        {
            var oldText = el.html();
            el.attr("data-alt-text", oldText);

            el.html(newText);
        }

    }

    $(".session-container").click(function ()
    {

        if (!$(this).hasClass("open"))
        {
            hideAllForms();
            CloseAllLists();
            $(this).addClass("open");

        }
    });

    $(".session-container .close").click(function ()
    {
        hideAllForms();
        setTimeout(CloseAllLists, 100);

    });

    function CloseAllLists()
    {
        $(".session-container").removeClass("open");
        
    }

    $(".open-profile-widget").click(function ()
    {
        var parentEl = $(this).closest(".session-container");
        var dataId = $(this).attr("data-id");

        parentEl.find(".profile-widget-container[data-id='" + dataId + "']:hidden").fadeIn();
    });

    $(".profile-widget .icon").mouseenter(function ()
    {
        $(this).find(".explain").show();
    });

    $(".profile-widget .item").click(function ()
    {
        var action = $(this).attr("data-id");

        if( action == "close")
            $(this).closest(".profile-widget-container").fadeOut();
    });

    $(".profile-widget .icon").mouseleave(function ()
    {
        $(this).find(".explain").hide();
    });

    $(".profile-widget .icon .image-container").click(function ()
    {
        var parentEl = $(this).closest(".icon");

        parentEl.find(".explain").hide();
        var textEl = parentEl.find(".text");

        if (textEl.is(":visible"))
            textEl.fadeOut();
        else
        {
            $(".icon .text").hide();
            textEl.fadeIn();
        }
    });
    
    $(".icon[data-id='respond']").click(function ()
    {
        var parentEl = $(this).closest(".sessions-container");
        var formEl = parentEl.find(".respond-container");

        if (formEl.is(":visible"))
            formEl.hide();
        else
        {
            hideAllForms();
            formEl.fadeIn();
        }
    });

    $(".button[data-id='new-dates']").click(function ()
    {
        var parentEl = $(this).closest(".respond-container");
        var closeEl = parentEl.find(".suggested-times");
        var el = parentEl.find(".new-times");

        closeEl.hide();
        el.fadeIn();
    });

    $(".button[data-id='cancel-new-dates']").click(function ()
    {
        var parentEl = $(this).closest(".respond-container");
        var closeEl = parentEl.find(".suggested-times");
        var el = parentEl.find(".new-times");

        el.hide();
        closeEl.fadeIn();
    });

    $(".close-all").click(function ()
    {
        $(".respond-container").hide();
    });

    function hideAllForms()
    {
        $(".session-container .expandable:visible").slideUp();
    }

    $(".profile-widget-container .background").click(function ()
    {
        $(this).closest(".profile-widget-container").fadeOut();
    });
});