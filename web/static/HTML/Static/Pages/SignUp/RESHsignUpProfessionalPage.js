// JavaScript Document

$(document).ready(function(e) {
$( "#signUpProForm" ).validate({
		submitHandler: function( form ){
			$("#errorsExplained").html("");
			var curStep = parseInt($(".signUpPro .step:visible").attr("step"));
			var nextStep = curStep;
			nextStep++;

			if(nextStep != 6){
				changeStep(nextStep);
				return false;
			}
			else
			{
			    form.submit();
            }
		},
		ignore: ':hidden',
		errorLabelContainer: $("#errorsExplained"),
        errorElement: 'li',
		rules: {
			password: "required",
			password_confirm: {
				equalTo: "#password"
			},
			sex: { valueNotEquals: "NotAnOption" }
		},
		messages:	{
			sex: { valueNotEquals: "Please select your gender" }
		}
	});
	
	 $.validator.addMethod("valueNotEquals", function(value, element, arg){
	  return arg != value;
	 }, "Please select a option");
	
	/* 
	
	jQuery.validator.addMethod('checkboxCheck', function(value, element){
			if ($("input [type='checkbox']").is("[required]")) {
				if (value === '') {
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}
		}, 'Please accept the terms and conditions');
	
	/******************* MASKS *******************/
	
	$("input[type='tel']").mask("(999) 999-9999",{placeholder:" "});
	
	/***************** MASKS END *****************/
	
	/******************* PROFILE PIC *******************/
	
	var fileDiv = document.getElementById("addProfilePic");
	var fileInput = document.getElementById("profilePic");
	console.log(fileInput);
	fileInput.addEventListener("change",function(e){
	  var files = this.files
	  showThumbnail(files)
	},false)
	
	fileDiv.addEventListener("click",function(e){
	  $(fileInput).show().focus().click().hide();
	  e.preventDefault();
	},false)
	
	fileDiv.addEventListener("dragenter",function(e){
	  e.stopPropagation();
	  e.preventDefault();
	},false);
	
	fileDiv.addEventListener("dragover",function(e){
	  e.stopPropagation();
	  e.preventDefault();
	},false);
	
	fileDiv.addEventListener("drop",function(e){
	  e.stopPropagation();
	  e.preventDefault();
	
	  var dt = e.dataTransfer;
	  var files = dt.files;
	
	  showThumbnail(files)
	},false);
	
	function showThumbnail(files){
	  for(var i=0;i<files.length;i++){
	    var file = files[i]
	    var imageType = /image.*/
	    
	    if(!file.type.match(imageType)){
	      console.log("Not an Image");
	      continue;
	  }
	
	    var image = document.createElement("img");
	    // image.classList.add("")
	    var thumbnail = document.getElementById("addProfilePic");
	    thumbnail.innerHTML = "";
	    thumbnail.style.background = "";
	    image.file = file;
	    thumbnail.appendChild(image)
	
	    var reader = new FileReader()
	    reader.onload = (function(aImg){
	      return function(e){
	        aImg.src = e.target.result;
	      };
	    }(image))
	    var ret = reader.readAsDataURL(file);
	    var canvas = document.createElement("canvas");
	    ctx = canvas.getContext("2d");
	    image.onload= function(){
	      ctx.drawImage(image,160,160)
	    }
	  }
	}
	
	/******************* PROFILE PIC END *******************/
	
	
	$(".back").click(function(e) {
        var curStep = parseInt($(".signUpPro .step:visible").attr("step"));
		var step = curStep;
		step--;
		
		changeStep(step);
		
		
    });
	
	function changeStep(step){
		$(".signUpPro .step").hide();
		$(".signUpPro .step[step='"+ step +"']").fadeIn("slow");
		$(".signUpPro .curStep").html(step);
		
		var stepTitle = $(".step[step='"+ step +"']").attr("stepTitle");
		$(".signUpPro .stepTitle").html(stepTitle);
		
		if(step > 1){
			$(".signUpPro .back").show();
		} else {
			$(".signUpPro .back").hide();
		}
		
		if(step == 2){
			var name = $("#firstname").val() + " " + $("#lastname").val();
			$(".signUpPro .name").html(name);
			$(".signUpPro .logo").hide();
			$(".signUpPro .name").show("slow");
		}
		
		window.scrollTo(0,0);
		
		
	}
	
	$(".signUpPro .addMore").click(function(e){
		var what = $(this).attr("what");
		var contentToApply = $(".signUpPro .standard[name='" + what + "']").html();
		$(".signUpPro .addMoreArea[name='" + what + "']").append(contentToApply);
	});
	
	$(".signUpPro").on("click", ".uploadAction", function(e) {
        $(this).parent(".documentProof").children(".fileUpload").click();
    });
	
	$(".signUpPro").on("mouseenter", ".uploadAction", function(){
		var curText = $(this).html();
	
		$(this).html("Click to Upload");
		
		$(this).parent().on("mouseleave", ".uploadAction", function(e){
			$(this).parent(".documentProof").children(".uploadAction").html(curText);
		});
	});
	
	$(".signUpPro").on("change", ".fileUpload", function(e){
		checkIfUploaded();
	});
	
	$(".signUpPro .addMore").click(function(e) {
        checkIfUploaded();
    });
	
	function checkIfUploaded(){
		var elements = $(".signUpPro .fileUpload");
		
		$(elements).each(function(index, element) {
            var fileName = $(element).val();
			
			if(fileName.length > 0){
				$(element).parent(".documentProof").children(".fileUploadedSuccessfully").show();
				$(element).parent(".documentProof").children(".uploadAction").html("Uploaded");
			} else {
				$(element).parent(".documentProof").children(".fileUploadedSuccessfully").hide();
				$(element).parent(".documentProof").children(".uploadAction").html("Verification");
			}
        });
		
	}
	
});