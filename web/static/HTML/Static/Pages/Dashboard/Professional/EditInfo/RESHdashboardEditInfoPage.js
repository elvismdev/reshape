@charset "UTF-8";
/* CSS Document */
.editAccountPro{
	position:relative;
	font-family: "Open Sans";
}
.editAccountPro .headerDiv{
	position: relative;
	text-align: left;
	color:#000;
	width:620px;
}
.editAccountPro h1{
	font-size:32px;
}
.editAccountPro .heading{
	text-align:left;
	font-weight:bold;
	font-size:20px;
	padding-bottom:10px;
	border-bottom:1px solid #ccc;
	margin-bottom:30px;
	margin-top:30px;
}
.editAccountPro .heading:first-child{
	margin-top:0px;
}
.editAccountPro .subheading{
	text-align:left;
	font-size:15px;
	padding-bottom:10px;
	border-bottom:1px solid #eaeaea;
	margin-bottom:30px;
	font-weight:bold;
	margin-top:30px;
}
.editAccountPro .subheading:first-child{
	margin-top:0;
}
.editAccountPro input{
	padding: 6px;
	-moz-border-radius: 7px;
	border-radius: 7px;
	border: none;
	background-color: #f7f7f7;
	-moz-box-shadow: inset 0 4px 10px #eaeaea;
	-webkit-box-shadow: inset 0 4px 10px #eaeaea;
	box-shadow: inset 0 4px 8px #eaeaea;
	width: 280px;
	height: 24px;
	font-size: 14px;
	text-indent:5px;
}
.editAccountPro input:focus{
	background-color: #eaeaea;
	-moz-box-shadow: inset 0 4px 10px #ccc;
	-webkit-box-shadow: inset 0 4px 10px #ccc;
	box-shadow: inset 0 4px 8px #ccc;
}
.editAccountPro label{
	width: 150px;
	float: left;
	line-height: 36px;
	font-size: 14px;
	color: #666;
	cursor:default;
	text-align:right;
	margin-right:20px;
}
.editAccountPro .labelFix{
	margin-left:170px;
}
.editAccountPro .zip{
	width:60px;
}
.editAccountPro .zipLabel{
	width:30px;
}
.editAccountPro .dob{
	margin-left:10px;
	margin-right:10px;
	width:127px;
}
.editAccountPro .gender{
	width:40px;
}
.editAccountPro .step{
	width:640px;
	margin:auto;
}
.editAccountPro .row{
	margin-bottom:15px;
}
.editAccountPro .left{
	float:left;
}
.editAccountPro .middle{
	float:left;
}
.editAccountPro .right{
	float:right;
}
.editAccountPro .center{
	display:inline-block;
	margin:auto;
}
.editAccountPro .center input{
	width:370px;
}
.editAccountPro input[type='submit'], .editAccountPro input[type='button']{
	margin-top:20px;
	margin-bottom:30px;
	-moz-box-shadow:none;
	-webkit-box-shadow: none;
	box-shadow: none;
	height: 38px;
	width: 150px;
	background: #eeeeee; /* Old browsers */
	background: -moz-linear-gradient(top, #eeeeee 0%, #d3d3d3 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#eeeeee), color-stop(100%,#d3d3d3)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #eeeeee 0%,#d3d3d3 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #eeeeee 0%,#d3d3d3 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #eeeeee 0%,#d3d3d3 100%); /* IE10+ */
	background: linear-gradient(to bottom, #eeeeee 0%,#d3d3d3 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#d3d3d3',GradientType=0 ); /* IE6-9 */
}

.editAccountPro input[type='submit']:hover, .editAccountPro input[type='button']:hover{
	background: #d3d3d3; /* Old browsers */
	background: -moz-linear-gradient(top, #d3d3d3 0%, #eeeeee 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d3d3d3), color-stop(100%,#eeeeee)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #d3d3d3 0%,#eeeeee 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #d3d3d3 0%,#eeeeee 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #d3d3d3 0%,#eeeeee 100%); /* IE10+ */
	background: linear-gradient(to bottom, #d3d3d3 0%,#eeeeee 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d3d3d3', endColorstr='#eeeeee',GradientType=0 ); /* IE6-9 */
}
.editAccountPro .back{
	width: 80px !important;
	margin-right:10px;
	display:none;
}
.editAccountPro input[type='checkbox']{
	width:15px;
}
.editAccountPro .row input[type='checkbox']{
	margin-top:5px;
}
.editAccountPro .tc{
	width:280px;
	margin:auto;
}
.editAccountPro a{
	color:#fff;
}
.editAccountPro .tc .iAccept{
	margin-top:12px;
	float:left;
	font-size:12px;
	color:#fff;
}
.editAccountPro .tc .checkBox{
	margin-top: 6px;
	margin-right: 8px;
	margin-left:30px;
	float:left;
}
.editAccountPro h3{
	font-style:italic;
	font-size:14px;
	color:#eaeaea;
	margin-bottom:15px;
}
.editAccountPro .rowThree .left input{
	width:248px !important;
}
.editAccountPro .rowThree .middle input{
	width:117px !important;
	margin-left:20px;
}
.editAccountPro .rowThree .right input{
	width:180px !important;
}
.editAccountPro .addMore{
	text-align: left;
	color: #999;
	font-size: 12px;
	margin-top: 10px;
}
.editAccountPro .addMore:hover{
	cursor:pointer;
	color:#ce2127;
}
.editAccountPro .leftColumn{
	float:left;
	width:48%;
}
.editAccountPro .rightColumn{
	float:right;
	width:48%;
}
.editAccountPro .addProfilePicContainer{
	text-align:center;
	margin-top: 50px;
	display: inline-block;
	margin-bottom: 20px;
}
.editAccountPro .addProfilePic{
	background:url(../images/signUpPro/add-profile-image.png);
	width:193px;
	height:145px;
}
.editAccountPro .addProfilePic:hover{
	background-position:0px -145px;
	cursor:pointer;
}
.editAccountPro .facebook{
	background:url(../images/signUpPro/facebook.png) no-repeat right 10px center #d3d3d3;
}
.editAccountPro .twitter{
	background:url(../images/signUpPro/twitter.png) no-repeat right 10px center #d3d3d3;
}
.editAccountPro .instagram{
	background:url(../images/signUpPro/instagram.png) no-repeat right 10px center #d3d3d3;
}
.editAccountPro .youtube{
	background:url(../images/signUpPro/youtube.png) no-repeat right 10px center #d3d3d3;
}
.editAccountPro select {
    padding: 10px;
	-moz-border-radius: 7px;
	border-radius: 7px;
	border: none;
	background-color: #f7f7f7;
	-moz-box-shadow: inset 0 4px 10px #eaeaea;
	-webkit-box-shadow: inset 0 4px 10px #eaeaea;
	box-shadow: inset 0 4px 8px #eaeaea;
	width: 280px;
	height: 36px;

	font-size: 14px;
	display: inline-block;
    -webkit-appearance:none;
    -moz-appearance:none;
    appearance:none;
    cursor:pointer;
	text-indent:5px;
}
.editAccountPro textarea{
	padding: 6px;
	-moz-border-radius: 7px;
	border-radius: 7px;
	border: none;
	background-color: #f7f7f7;
	-moz-box-shadow: inset 0 4px 10px #eaeaea;
	-webkit-box-shadow: inset 0 4px 10px #eaeaea;
	box-shadow: inset 0 4px 8px #eaeaea;
	width: 280px;
	height: 72px;
	font-size: 14px;
	text-indent:5px;
}
.editAccountPro textarea:focus{
	background-color: #eaeaea;
	-moz-box-shadow: inset 0 4px 10px #ccc;
	-webkit-box-shadow: inset 0 4px 10px #ccc;
	box-shadow: inset 0 4px 8px #ccc;
}
.editAccountPro input[type='file']{
	background: none;
	-moz-box-shadow: none;
	-webkit-box-shadow: none;
	box-shadow: none;
	text-indent: 0px;
}
.editAccountPro .removeRow{
	background:url(../images/forms/minus.png);
	width:16px;
	height:16px;
	position: absolute;
	left: 473px;
	margin-top: -27px;
}
.editAccountPro .removeRow:hover{
	background-position:0px 16px;
	cursor:pointer;
}
.editAccountPro .removable:first-child .removeRow{
	display:none;
}
.editAccountPro .addMoreArea .removable:first-child .removeRow{
	display:block;
}
.editAccountPro .carouselContent .removeRow{
	left: -8px;
	margin-top: -161px;
}

.editAccountPro .carouselContent img{
	width:178px;
	float:left; 
	margin-right:20px;
}

.editAccountPro .iOfferThis{
	font-size: 12px;
	margin-top: -20px;
	margin-bottom: 20px;
}

.editAccountPro .iOfferThis input[type='checkbox']{
	margin-top:-2px !important;
	vertical-align:middle !important;
	margin-right:5px;
}

.editAccountPro .priceAd{
	font-size:12px;
}

.editAccountPro .priceAd input{
	width:70px;
	margin-left:20px;
}

.showSocialHub
{
	float:			right;
	font-size:		11px;
	color:			#333;
	line-height:	33px;
}

.showSocialHub input
{
	margin-right:		7px;
	vertical-align:		middle;
	margin-top:			0px !important
}



.editAccountPro .bigTextarea{
	padding:10px;
	width:600px;
	height:185px;
	text-indent:0px;
	line-height:18px;
	font-size:12px;
}

/* Targetting Webkit browsers only. FF will show the dropdown arrow with so much padding. */
@media screen and (-webkit-min-device-pixel-ratio:0) {
	.editAccountPro select {
		padding-right:30px;
	}
	
}
.editAccountPro .state {
	float:left !important;

}
.editAccountPro .state select{
	width:150px !important;
}

.editAccountPro .labelCheat {
	position:relative;
	width:auto;
	float:none;
}

.editAccountPro .labelCheat:after {
    content:'<>';
    font:11px "Consolas", monospace;
    color:#333;
    -webkit-transform:rotate(90deg);
    -moz-transform:rotate(90deg);
    -ms-transform:rotate(90deg);
    transform:rotate(90deg);
    right:6px; 
	top:0px;
    padding:5px 3px 0px 3px;
    border-bottom:1px solid #999;
    position:absolute;
	height:17px;
    pointer-events:none;
	background:#f7f7f7;
}
.editAccountPro .labelCheat:before {
    content:'';
    right:6px; top:0px;
    width:20px; height:20px;
    position:absolute;
    pointer-events:none;
    display:block;
}

