﻿$(document).ready(function ()
{
$(".icon").mouseenter(function ()
    {
        $(this).find(".explain").show();
    });

    $(".icon").mouseleave(function ()
    {
        $(this).find(".explain").hide();
    });

    $(".icon .image-container").click(function ()
    {
        var parentEl = $(this).closest(".icon");

        parentEl.find(".explain").hide();
        var textEl = parentEl.find(".text");

        if (textEl.is(":visible"))
            textEl.fadeOut();
        else
        {
            $(".icon .text").hide();
            textEl.fadeIn();
        }
    });

    $(".icon[data-id='note']").click(function ()
    {
        var parentEl = $(this).closest(".client");
        var formEl = parentEl.find(".note-container");

        if( formEl.is(":visible") )
            formEl.hide();
        else
        {
            hideAllForms();
            formEl.fadeIn();
        }
    });

    $(".icon[data-id='date']").click(function ()
    {
        var parentEl = $(this).closest(".client");
        var formEl = parentEl.find(".select-container");

        if (formEl.is(":visible"))
            formEl.hide();
        else
        {
            hideAllForms();
            formEl.fadeIn();
        }
    });

    $(".icon[data-id='settings']").click(function ()
    {
        var parentEl = $(this).closest(".client");
        var formEl = parentEl.find(".edit-container");

        if (formEl.is(":visible"))
            formEl.hide();
        else
        {
            hideAllForms();
            formEl.fadeIn();
        }
    });

    $(".icon[data-id='respond']").click(function ()
    {
        var parentEl = $(this).closest(".client");
        var formEl = parentEl.find(".respond-container");

        if (formEl.is(":visible"))
            formEl.hide();
        else
        {
            hideAllForms();
            formEl.fadeIn();
        }
    });

    var selectOpened = false;
    $('select').click(function (e)
    {
        selectOpened = !selectOpened;
        e.stopPropagation();
    });
    $('body').click(function ()
    {
        if (selectOpened)
            selectOpened = false;
    })

    function hideAllForms()
    {
        $(".client .note-container").hide();
        $(".client .select-container").hide();
        $(".client .edit-container").hide();
    }

    $(".client").mouseleave(function ()
    {
        if (!selectOpened)
            hideAllForms();
    });

    $(".close-all").click(function ()
    {
        $(".client .respond-container").hide();
    });

    $(".button[data-id='new-dates']").click(function ()
    {
        var parentEl = $(this).closest(".respond-container");
        var closeEl  = parentEl.find(".suggested-times");
        var el       = parentEl.find(".new-times");

        closeEl.hide();
        el.fadeIn();
    });

    $(".button[data-id='cancel-new-dates']").click(function ()
    {
        var parentEl = $(this).closest(".respond-container");
        var closeEl = parentEl.find(".suggested-times");
        var el = parentEl.find(".new-times");

        el.hide();
        closeEl.fadeIn();
    });
});