﻿$(document).ready(function ()
{
    window.loadCarouselEdit();
});

window.loadCarouselEdit = function ()
{
    window.InitCarouselForm();

    $("html").on("click", ".left, .right", function (e)
    {
        window.InitCarouselForm();
    });

    $("html").on("click", ".teaser", function ()
    {
        if (!$(this).hasClass("roundabout-in-focus"))
            window.InitCarouselForm();
    });

    $('.carouselContent').bind('animationEnd', function ()
    {
        window.InitCarouselForm();
    });
};

window.InitCarouselForm = function ()
{
    $(".form").hide();
    setTimeout(window.fadeInRoundAbout, 100);
};

window.fadeInRoundAbout = function ()
{
    var id = $(".roundabout-in-focus").attr("data-num");
    $(".form[data-num='" + id + "']").fadeIn();
};