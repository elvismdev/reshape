﻿$(document).ready(function ()
{
    $(".view-all-link").click(function ()
    {
        $(".service").removeClass("selected");
        $(this).addClass("selected");
    });

    $(".service").click(function ()
    {
        var url = $(this).attr("data-iframe-url");
        $("#iframe-1").attr("src", url);
        $("#iframe-2").hide();
        $("#iframe-3").hide();

        $(".service, .view-all-link").removeClass("selected");
        $(this).addClass("selected");
    });
});

jQuery.fn.scrollTo = function ()
{
    $('html, body').animate({ 'scrollTop': this.offset().top }, 'slow', 'swing');
};

window.IframeNavigation = function (link, fromIframe, toIframe)
{
    var iframe1el = window.parent.document.getElementById("iframe-1");
    var iframe2el = window.parent.document.getElementById("iframe-2");
    var iframe3el = window.parent.document.getElementById("iframe-3");
    var iframeAct = window.parent.document.getElementById("iframe-" + toIframe);

    if (toIframe == 1)
    {
        HideIframe(iframe2el);
        HideIframe(iframe3el);
    }
    else if (toIframe <= 2)
        HideIframe(iframe3el);

    if (fromIframe == 1 && toIframe == 3)
        HideIframe(iframe2el);

    ShowIframe(iframeAct, link);

    $("#iframe-" + toIframe, window.parent.document).scrollTo();

};

window.HideIframe = function (iframeEl)
{
    iframeEl.style.display = "none";
};

window.ShowIframe = function (iframeEl, src)
{
    iframeEl.setAttribute("src", src);
    iframeEl.style.display = "block";
};

window.ResizeIframe = function (iframeNum)
{
    setTimeout(function ()
    {
        var curHeight = document.body.scrollHeight;
        var iframeEl = window.parent.document.getElementById("iframe-" + iframeNum);
        iframeEl.style.height = curHeight + 150 + "px";
    }, 200);
};

window.UpdateAfterSubmit = function()
{
    window.parent.document.getElementById("iframe-3").style.display = 'none';
    setTimeout(function ()
    {
        window.parent.document.getElementById("iframe-1").contentWindow.location.reload();
    }, 500);

    var iframe2el = window.parent.document.getElementById("iframe-2");

    if (iframe2el.style.display == "block")
    {
        iframe2el.contentWindow.location.reload();
        setTimeout(ResizeIframe(2),500);
    }
    
};