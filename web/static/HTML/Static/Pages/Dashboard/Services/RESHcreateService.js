﻿$(document).ready(function ()
{
    var maxEl          = $("#maxParticipants");
    var serviceSel     = $("#serviceSelect");
    var maxElContainer = maxEl.closest("div");

    if (maxEl.length > 0 && serviceSel.length > 0 )
    {
        serviceSel.change(function ()
        {
            var val = serviceSel.find("option:selected").text();
            console.log("Val: " + val);
            var serviceType = val.match(/\(([^)]+)\)/)[1];

            if (serviceType)
            {
                if (serviceType == "One on One")
                {
                    maxEl.val(1);
                    maxElContainer.hide();
                }
                else if (serviceType == "Class")
                {
                    maxElContainer.show();
                }
                else if (serviceType == "Group Session")
                {
                    maxEl.val(12);
                    maxElContainer.show();
                }
                else if (serviceType == "Broadcast")
                {
                    maxElContainer.show();
                }
                else if (serviceType == "In Person")
                {
                    maxElContainer.show();
                }
                else
                {
                    maxElContainer.show();
                    console.log("No Service type found");
                }

                
            }
            else
                console.log("No Service Type could be find in selected string");
        });
    }

});