// JavaScript Document

$(document).ready(function(e) {
    $(".category .mainChoise, .category .subChoise").mouseenter(function(){
		//Remove all existing Hover classes
		$(".category .mainChoise, .category .subChoise").removeClass("choiseHover");
		$(".category .mainChoise, .category .subChoise").children(".arrow").removeClass("arrowHover");
		
		//Add Hover Classes to current
		$(this).addClass("choiseHover");
		$(this).children(".arrow").addClass("arrowHover");
	});
	$(".category .mainChoise, .category .subChoise").mouseleave(function(){
		//Remove all existing Hover classes
		$(".category .mainChoise, .category .subChoise").removeClass("choiseHover");
		$(".category .mainChoise, .category .subChoise").children(".arrow").removeClass("arrowHover");
	});
});