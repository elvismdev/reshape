// JavaScript Document

$(document).ready(function(e) {
    
	var arrayElements = new Array(); 	//Array with all navigation objects
	var arrayPositions = new Array(); 	//Position of each navigation object
	
	var iNav = 0;
	
	
	$("section[nav='1']").each(function(index, element) {
        iNav++;
		
		$(this).attr("navId", iNav);
		var name = $(this).attr("name");
		
		arrayElements[iNav] = name;
		
    });
	
	//Add Navigation Boxes
	for(var i =1; i < arrayElements.length; i++){
		var name = arrayElements[i];
		$(".navigationBoxesContainer").append("<div class='navigationBox' navId='"+ i +"'><div class='text'>"+ name +"</div></div>");
	}
	
	function updatePositions(){
		for(var i =1; i < arrayElements.length; i++){
			var pos = $("section[navId='"+ i +"']").offset().top;
			var additionalHeight = $("header").height()-1;
			var scrollPos = pos-additionalHeight;
			
			arrayPositions[i] = scrollPos;
		}
	}
	
	function scrollToSection(navId){
		var scrollPos = arrayPositions[navId];
		$('html, body').animate({
			scrollTop: scrollPos
		}, 1000);
		
		updateCurSelectedNavigationBox();
	}
	
	function hideNavigation(){
		var width = $(window).width();
		var element = $(".navigationBoxesContainer");
		
		if(width < 1100 && element.is(':visible')){
			element.fadeOut("slow");
		} else if(width >= 1100 && element.is(':hidden')) {
			element.fadeIn("slow");
		}
	}
	
	function updateCurSelectedNavigationBox(){
		var curPos = $(document).scrollTop();
		var selected = 1;
		for(var i =1; i < arrayElements.length; i++){
			var thisPos = arrayPositions[i]-10;
			if(curPos >= thisPos){
				var selected = i;
			}
		}
		if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        	var selected = arrayElements.length-1;
    	}
		
		$(".navigationBoxes .navigationBox").removeClass("selected");
		$(".navigationBoxes .navigationBox[navId='"+ selected +"']").addClass("selected");
		
	}
	
	//Function Checked when site Loaded
	hideNavigation();
	updatePositions();
	updateCurSelectedNavigationBox();
	
	$(window).scroll(function(){
		updateCurSelectedNavigationBox();
	});
	
	$(window).resize(function() {
		hideNavigation();
	});
	
	$(document).click(function(e) {
        updatePositions();
    });
	
	$(".navigationBox").click(function(e) {
		updatePositions();
		var navId = $(this).attr("navId");
		
		var el = $("section[navId='"+ navId +"'] div[showIfNavigationClicked='1']");
		
		if(el.length > 0){
			var divClass = el.attr("class");
			if($("." + divClass).is(":visible") == false){ //Check if something is already visible
				el.fadeIn();
			} 
		}
		
        scrollToSection(navId);
    });
	
});