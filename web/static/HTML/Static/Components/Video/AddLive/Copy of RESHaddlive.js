﻿/* ALWAYS */
/**
* @fileoverview
* @TODO file description
*
* @author Tadeusz Kozak
* @date 26-06-2012 13:23
*/

/**
* @namespace Namespace for all AddLive tutorials definitions.
*/

(function (w)
{
    'use strict';
    var ADLT = w.ADLT || {};

    // IE shim - for IE 8+ the console object is defined only if the dev tools
    // are acive
    if (!window.console)
    {
        window.console = {
            log: function ()
            {
            },
            error: function ()
            {
            },
            warn: function ()
            {
            }
        };
    }

    ADLT.SCOPE_ID = 'reshape'; // Put your scope here;

    ADLT.APP_ID = 472; // Put your app Id here;

    ADLT.API_KEY = 'ADL_UK8l21fiEpnV0pbfAnjdpOQxNRGgmfrcDOO5PsRSR0GPw6alao7moA3'; // Put your API key here;

    function _nop()
    {
    }


    /**
  * @const
  * @type {String}
  */
    ADLT.PLUGIN_CONTAINER_ID = 'pluginContainer';


    ADLT.initAddLiveLogging = function ()
    {
        ADL.initLogging(function (lev, msg)
        {
            switch (lev)
            {
                case ADL.LogLevel.DEBUG:
                    console.log('[ADL] ' + msg);
                    break;
                case ADL.LogLevel.WARN:
                    console.warn('[ADL] ' + msg);
                    break;
                case ADL.LogLevel.ERROR:
                    console.error('[ADL] ' + msg);
                    break;
                case ADL.LogLevel.INFO:
                    console.log('[ADL] ' + msg);
                    break;
                default:
                    console.warn('Got unsupported log level: ' + lev + '. Message: ' +
                        msg);
            }
        }, true);
    };

    /**
  * Initializes the AddLive SDK.
  */
    ADLT.initializeAddLiveQuick = function (completeHandler, options)
    {
        ADLT.initAddLiveLogging();
        console.log('Initializing the AddLive SDK');
        var initListener = new ADL.PlatformInitListener();

        // Define the handler for initialization progress changes - just in case
        // the page has #initProgressBar progressbar
        initListener.onInitProgressChanged = function (e)
        {
            console.log('Platform init progress: ' + e.progress);
            var $pBar = $('#initProgressBar');
            if ($pBar.length)
            {
                $('#initProgressBar').progressbar('value', e.progress);
            }
        };

        initListener.onInitStateChanged = function (e)
        {
            switch (e.state)
            {
                case ADL.InitState.ERROR:
                    console.error('Failed to initialize the AddLive SDK');
                    var msg = e.errMessage + ' (' + e.errCode + ')';
                    console.error('Reason: ' + msg);
                    window.alert('There was an error initialising the media plug-in: ' +
                        msg + ' Please reinstall it.');
                    break;
                case ADL.InitState.INITIALIZED:
                    completeHandler();
                    break;
                case ADL.InitState.DEVICES_INIT_BEGIN:
                    console.log('Devices initialization started');
                    break;
                case ADL.InitState.INSTALLATION_REQUIRED:
                    console.log('AddLive Plug-in installation required');
                    $('#installBtn').
                        attr('href', e.installerURL).
                        css('display', 'block');
                    break;
                case ADL.InitState.INSTALLATION_COMPLETE:
                    console.log('AddLive Plug-in installation complete');
                    $('#installBtn').hide();
                    break;
                case ADL.InitState.BROWSER_RESTART_REQUIRED:
                    // This state indicates that AddLive SDK performed auto-update and in
                    // order to accomplish this process, browser needs to be restarted.
                    console.log('Please restart your browser in order to complete platform auto-update');
                    break;


                default:
                    console.warn('Got unsupported init state: ' + e.state);
            }
        };
        ADL.initPlatform(initListener, options);
    };


    ADLT.initDevicesSelects = function ()
    {
        $('#camSelect').change(ADLT.getDevChangedHandler('VideoCapture'));
        $('#micSelect').change(ADLT.getDevChangedHandler('AudioCapture'));
        $('#spkSelect').change(ADLT.getDevChangedHandler('AudioOutput'));
    };

    ADLT.getDevChangedHandler = function (devType)
    {
        return function ()
        {
            var selectedDev = $(this).val();
            ADL.getService()['set' + devType + 'Device'](
                ADL.createResponder(),
                selectedDev);
        };
    };

    /**
  * Fills the selects with the currently plugged in devices.
  */
    ADLT.populateDevicesQuick = function ()
    {
        ADLT.populateDevicesOfType('#camSelect', 'VideoCapture');
        ADLT.populateDevicesOfType('#micSelect', 'AudioCapture');
        ADLT.populateDevicesOfType('#spkSelect', 'AudioOutput');
    };

    /**
  * Fills the audio output devices select.
  */
    ADLT.populateDevicesOfType = function (selectSelector, devType)
    {
        var devsResultHandler = function (devs)
        {
            var $select = $(selectSelector);
            $select.empty();
            $.each(devs, function (devId, devLabel)
            {
                $('<option value="' + devId + '">' + devLabel + '</option>').
                    appendTo($select);
            });
            var getDeviceHandler = function (device)
            {
                $select.val(device);
            };
            ADL.getService()['get' + devType + 'Device'](
                ADL.createResponder(getDeviceHandler));
        };
        ADL.getService()['get' + devType + 'DeviceNames'](
            ADL.createResponder(devsResultHandler));
    };

    ADLT.genRandomUserId = function ()
    {
        var userId;

        var userIdEl = $("#userId");

        if (userIdEl)
            userId = parseInt( userIdEl.val() );
        else
            userId = Math.floor(Math.random() * 10000);

        return userId;
    };

    ADLT.randomString = function (len, charSet)
    {
        charSet = charSet ||
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var str = '';
        for (var i = 0; i < len; i++)
        {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            str += charSet.substring(randomPoz, randomPoz + 1);
        }
        return str;
    };


    /**
  * Generates sample authentication details. For more info about authentication,
  * please refer to: http://www.addlive.com/docs.html#authentication
  */
    ADLT.genAuth = function (scopeId, userId)
    {

        // New Auth API
        var dateNow = new Date();
        var now = Math.floor((dateNow.getTime() / 1000));
        var authDetails = {
            // Token valid 5 mins
            expires: now + (5 * 60),
            userId: userId,
            salt: ADLT.randomString(100)
        };
        var signatureBody =
            ADLT.APP_ID +
                scopeId +
                userId +
                authDetails.salt +
                authDetails.expires +
                ADLT.API_KEY;
        authDetails.signature =
            w.CryptoJS.SHA256(signatureBody).toString(w.CryptoJS.enc.Hex).toUpperCase();
        return authDetails;
    };

    // Export the ADLT namespace
    w.ADLT = ADLT;

})(window);












/**
* @fileoverview
* @TODO file description
*
* @author Tadeusz Kozak
* @date 26-06-2012 10:37
*/


(function (w)
{
    'use strict';


    // Scope constants
    // To set your own APP_ID (check shared-assets/scripts.js)
    var APPLICATION_ID = ADLT.APP_ID,

        /**
  * Configuration of the streams to publish upon connection established
  * @type {Object}
  */
            CONNECTION_CONFIGURATION = {
                videoStream: {
                    maxWidth: 1280,
                    maxHeight: 720,
                    maxFps: 24,
                    useAdaptation: true
                },
                /**
        * Flags defining that both streams should be automatically published upon
        * connection.
        */
                autopublishVideo: true,
                autopublishAudio: true
            },

        mediaConnType2Label = {};

    mediaConnType2Label[ADL.ConnectionType.NOT_CONNECTED] = 'not connected';
    mediaConnType2Label[ADL.ConnectionType.TCP_RELAY] = 'RTP/TCP relayed';
    mediaConnType2Label[ADL.ConnectionType.UDP_RELAY] = 'RTP/UDP relayed';
    mediaConnType2Label[ADL.ConnectionType.UDP_P2P] = 'RTP/UDP in P2P';

    // Scope variables
    var scopeId, userId, localVideoStarted = false,
        /**
  * @type {ADL.MediaConnection}
  */
            mediaConnection;

    /**
  * Document ready callback - starts the AddLive platform initialization.
  */
    function onDomReady()
    {
        console.log('DOM loaded');

        // assuming the initAddLiveLogging is exposed via ADLT namespace.
        // (check shared-assets/scripts.js)
        ADLT.initAddLiveLogging();
        initUI();
        var initOptions = { applicationId: APPLICATION_ID, enableReconnects: true };
        // Initializes the AddLive SDK. Please refer to ../shared-assets/scripts.js
        ADLT.initializeAddLiveQuick(onPlatformReady, initOptions);
    }

    function initUI()
    {
        console.log('Initializing the UI');
        $('#publishAudioChckbx').change(onPublishAudioChanged);
        $('#publishVideoChckbx').change(onPublishVideoChanged);

        $('#camSelect').change(function ()
        {
            var selectedDev = $(this).val();
            ADL.getService().setVideoCaptureDevice(
                ADL.r(startLocalVideoMaybe), selectedDev);
        });

        $('#micSelect').change(ADLT.getDevChangedHandler('AudioCapture'));
        $('#spkSelect').change(ADLT.getDevChangedHandler('AudioOutput'));

        console.log('UI initialized');
    }

    function onPlatformReady()
    {
        console.log('==============================================================');
        console.log('==============================================================');
        console.log('AddLive SDK ready - setting up the application');

        // assuming the populateDevicesQuick is exposed via ADLT namespace.
        // (check shared-assets/scripts.js)
        ADLT.populateDevicesQuick();
        startLocalVideoMaybe();
        initServiceListener();

        ADLListener.onMessage = function (e)
        {
            console.log('Got new message from ' + e.srcUserId);
            var msg = JSON.parse(e.data);
            appendMessage(e.srcUserId, msg.text, msg.direct);
        };
        ADL.getService().addServiceListener(ADL.r(onListenerAdded), ADLListener);
    }





    /**
  * ==========================================================================
  * Beginning of the AddLive service events handling code
  * ==========================================================================
  */


    function initServiceListener()
    {
        console.log('Initializing the AddLive Service Listener');

        // 1. Instantiate the listener
        var listener = new ADL.AddLiveServiceListener();


        // 2. Define the handler for the user event
        listener.onUserEvent = function (e)
        {
            console.log('Got new user event: ' + e.userId);
            if (e.isConnected)
            {
                onUserJoined(e);
            } else
            {
                console.log('User with id: ' + e.userId + ' left the media scope');
                $('#renderingWidget' + e.userId).html('').remove();
            }

            ResizeVideos();
        };

        // 3. Define the handler for streaming status changed event
        listener.onMediaStreamEvent = function (e)
        {
            console.log('Got new media streaming status changed event');
            switch (e.mediaType)
            {
                case ADL.MediaType.AUDIO:
                    onRemoteAudioStreamStatusChanged(e);
                    break;
                case ADL.MediaType.VIDEO:
                    onRemoteVideoStreamStatusChanged(e);
                    break;
                default:
                    console.warn('Got unsupported media type in media stream event: ' +
                        e.mediaType);
            }
        };

        // 4. Define the handler for the media connection type changed event
        listener.onMediaConnTypeChanged = function (e)
        {
            console.log('Got new media connection type: ' + e.connectionType);
            $('#connTypeLbl').html(mediaConnType2Label[e.connectionType]);
        };

        // 5. Define the handler for the connection lost event
        listener.onConnectionLost = function (e)
        {
            console.warn('Got connection lost notification: ' + JSON.stringify(e));
            disconnectHandler();
        };

        // 6. Define the handler for the reconnection event
        listener.onSessionReconnected = function (e)
        {
            console.log('Connection successfully reestablished!');
            postConnectHandler();
        };

        // 7. Prepare the success handler
        var onSucc = function ()
        {
            console.log('AddLive service listener registered');
            $('#connectBtn').click(connect).removeClass('disabled');
           
            InitUsersInfo();
        };

        // 8. Finally register the AddLive Service Listener
        ADL.getService().addServiceListener(ADL.r(onSucc), listener);

    }

    function onUserJoined(e)
    {
        console.log('Got new user with id: ' + e.userId);

        // 1. Prepare a rendering widget for the user.
        var renderer = $('#rendererTmpl').clone();
        renderer.attr('id', 'renderingWidget' + e.userId);
        renderer.find('.render-wrapper').attr('id', 'renderer' + e.userId);
        renderer.find('.user-id-wrapper').html(e.userId);
        renderer.attr('data-user-id', e.userId);

        // 2. Append it to the rendering area.
        $('#renderingWrapper').append(renderer);
        if (e.videoPublished)
        {
            // 3a. Render the sink if the video stream is being published.
            ADL.renderSink({
                sinkId: e.videoSinkId,
                containerId: 'renderer' + e.userId
            });
        }
        else
        {
            // 3b. Just show the no video stream published indicator.
            renderer.find('.no-video-text').show();
            renderer.find('.allowReceiveVideoChckbx').hide();
        }

        // 4. Show the 'audio muted' indicator if user does not publish audio stream
        if (!e.audioPublished)
        {
            renderer.find('.muted-indicator').show();
            renderer.find('.allowReceiveAudioChckbx').hide();
        }

        ResizeVideos();
        InitUsersInfo();
    }

    function onRemoteVideoStreamStatusChanged(e)
    {
        console.log('Got change in video streaming for user with id: ' + e.userId +
            ' user just ' +
            (e.videoPublished ? 'published' : 'stopped publishing') +
            ' the stream');
        // 1. Grab the rendering widget corresponding to the user
        var renderingWidget = $('#renderingWidget' + e.userId);

        if (e.videoPublished)
        {
            // 2a. If video was just published - render it and hide the
            // 'No video from user' indicator
            ADL.renderSink({
                sinkId: e.videoSinkId,
                containerId: 'renderer' + e.userId
            });
            renderingWidget.find('.no-video-text').hide();
        } else
        {
            // 2b. If video was just unpublished - clear the renderer and show the
            // 'No video from user' indicator
            renderingWidget.find('.render-wrapper').empty();
            renderingWidget.find('.no-video-text').show();
        }

    }

    function onRemoteAudioStreamStatusChanged(e)
    {
        console.log('Got change in audio streaming for user with id: ' + e.userId +
            ' user just ' +
            (e.audioPublished ? 'published' : 'stopped publishing') +
            ' the stream');

        // 1. Find the 'Audio is muted' indicator corresponding to the user
        var muteIndicator = $('#renderingWidget' + e.userId).find('.muted-indicator');
        if (e.audioPublished)
        {
            // 2a. Hide it if audio stream was just published
            muteIndicator.hide();
        } else
        {
            // 2.b Show it if audio was just unpublished
            muteIndicator.show();
        }
    }

    /**
  * ==========================================================================
  * End of the AddLive service events handling code
  * ==========================================================================
  */


    function startLocalVideoMaybe()
    {
        if (localVideoStarted)
        {
            return;
        }
        console.log('Starting local preview of current user');
        // 1. Define the result handler
        var resultHandler = function (sinkId)
        {
            console.log('Local preview started. Rendering the sink with id: ' + sinkId);
            ADL.renderSink({
                sinkId: sinkId,
                containerId: 'renderLocalPreview',
                mirror: true
            });
            localVideoStarted = true;
        };

        // 2. Request the SDK to start capturing local user's preview
        ADL.getService().startLocalVideo(ADL.r(resultHandler));
    }

    /**
  * ==========================================================================
  * Beginning of the connection management code
  * ==========================================================================
  */

    function connect()
    {
        console.log('Establishing a connection to the AddLive Streaming Server');

        // 1. Disable the connect button to avoid a cascade of connect requests
        $('#connectBtn').unbind('click').addClass('disabled');

        // 2. Get the scope id and generate the user id.
        scopeId = $('#scopeIdTxtField').val();

        // Assign Random User
        userId = ADLT.genRandomUserId();

        // 3. Define the result handler - delegates the processing to
        // the postConnectHandler
        var connDescriptor = genConnectionDescriptor(scopeId, userId);

        /**
    *
    * @param {ADL.MediaConnection} mConn
    */
        var onSucc = function (mConn)
        {
            mediaConnection = mConn;
            postConnectHandler();
            var updateTimeInterval = setInterval(updateDisplayTime, 1000);
            
        };

        // 4. Define the error handler - enabled the connect button again
        var onErr = function ()
        {
            $('#connectBtn').click(connect).removeClass('disabled');
        };

        // 5. Request the SDK to establish a connection
        ADL.getService().connect(ADL.r(onSucc, onErr), connDescriptor);
        ResizeVideos();
        InitUsersInfo();
    }

    function disconnect()
    {
        console.log('Terminating a connection to the AddLive Streaming Server');

        // 1. Define the result handler
        var succHandler = function ()
        {
            scopeId = undefined;
            userId = undefined;
            mediaConnection = undefined;
            disconnectHandler();
        };
        mediaConnection.disconnect(ADL.r(succHandler));
    }

    /**
  * Common post disconnect handler - used when user explicitly terminates the
  * connection or if the connection gets terminated due to the networking issues.
  *
  * It just resets the UI to the default state.
  */
    function disconnectHandler()
    {

        // 1. Toggle the active state of the Connect/Disconnect buttons
        $('#connectBtn').click(connect).removeClass('disabled');
        $('#disconnectBtn').unbind('click').addClass('disabled');

        // 2. Reset the connection type label
        $('#connTypeLbl').html('none');

        // 3. Clear the remote user renderers
        $('#renderingWrapper').find('.remote-renderer').html('').remove();

        // 4. Clear the local user id label
        $('#localUserIdLbl').html('undefined');
    }

    /**
  * Common post connect handler - used when user manually establishes the
  * connection or connection is being reestablished after being lost due to the
  * Internet connectivity issues.
  *
  */

    function postConnectHandler()
    {
        console.log('Connected. Disabling connect button and enabling the disconnect');

        // 1. Enable the disconnect button
        $('#disconnectBtn').click(disconnect).removeClass('disabled');

        // 2. Disable the connect button
        $('#connectBtn').unbind('click').addClass('disabled');

        // 3. Update the local user id label
        $('#localUserIdLbl').html(userId);
    }

    function genConnectionDescriptor(scopeId, userId)
    {
        // Prepare the connection descriptor by cloning the configuration and
        // updating the URL and the token.
        var connDescriptor = $.extend({}, CONNECTION_CONFIGURATION);
        connDescriptor.scopeId = scopeId;
        connDescriptor.authDetails = ADLT.genAuth(scopeId, userId);
        connDescriptor.autopublishAudio = $('#publishAudioChckbx').is(':checked');
        connDescriptor.autopublishVideo = $('#publishVideoChckbx').is(':checked');
        return connDescriptor;
    }


    /**
  * ==========================================================================
  * End of the connection management code
  * ==========================================================================
  */

    /**
  * ==========================================================================
  * Beginning of the user's events handling code
  * ==========================================================================
  */

    /**
  * Handles the change of the 'Publish Audio' checkbox
  */
    function onPublishAudioChanged()
    {
        if (!scopeId)
        {
            // If the scope id is not defined, it means that we're not connected and
            // thus there is nothing to do here.
            return;
        }

        // Since we're connected we need to either start or stop publishing the
        // audio stream, depending on the new state of the checkbox
        if ($('#publishAudioChckbx').is(':checked'))
        {
            mediaConnection.publishAudio(ADL.r());
        } else
        {
            mediaConnection.unpublishAudio(ADL.r());
        }

    }

    /**
  * Handles the change of the 'Publish Audio' checkbox
  */
    function onPublishVideoChanged()
    {
        if (!scopeId)
        {

            // If the scope id is not defined, it means that we're not connected and
            // thus there is nothing to do here.
            return;
        }

        // Since we're connected we need to either start or stop publishing the
        // audio stream, depending on the new state of the checkbox
        if ($('#publishVideoChckbx').is(':checked'))
        {
            mediaConnection.publishVideo(ADL.r());
        } else
        {
            mediaConnection.unpublishVideo(ADL.r());
        }

    }

    /**
  * ==========================================================================
  * End of the user's events handling code
  * ==========================================================================
  */

    function ResizeVideos()
    {
        var countRemote = $(".remote-renderer .render-wrapper").length - 1;

        if( countRemote > 0 )
        {
            var aspectRatio  = 3 / 4;
            var screenHeight = $(window).height();
            var screenWidth  = $(window).width();

            var minScreenW   = 1100;

            if (screenWidth < minScreenW)
                screenWidth = minScreenW;


            var takenSpace = 511;
            var marginWidth = 20;
            var maxHeight = screenHeight - 185;

            var remoteWidth  = ( screenWidth - takenSpace ) / countRemote;
            remoteWidth = Math.round(remoteWidth);

            
            var remoteHeight = Math.round(remoteWidth * aspectRatio);

            if (remoteHeight > maxHeight)
                remoteHeight = maxHeight;

            console.log( "[RESH] Screen Sizes: Width " + remoteWidth + ", Height: " + remoteHeight );
            console.log( "[RESH] Calculation: ( " + screenWidth + " - " + takenSpace + " ) / " + countRemote );

            $(".remote-renderer .render-wrapper").css("height", remoteHeight + "px").css("width", remoteWidth + "px");

        }

    }

    function InitUsersInfo()
    {
        console.log("[RESH] InitUsersInfo called");
        var i = 0;
        $(".remote-renderer[data-user-id]").each( function (key, val)
        {
            var userId = $(this).attr("data-user-id");

            if (userId != "undefined" && userId != "")
                GetUserInfo( userId );
        });
    }

    function httpGet(theUrl)
    {
        console.log("[RESH] httpGet Called");
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            var xmlhttp = new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function ()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                console.log("[RESH] httpGet status: 200, url:" + theUrl);
                console.log("[RESH] Response: " + xmlhttp.responseText);
                return xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", theUrl, false);
        xmlhttp.send();
    }

    function GetUserInfo(userId)
    {
        console.log("[RESH] GETUserInfo called");
        var url = "http://live.reshape.net/api/v1/user/user-info?token=jkadsfkjahs343assjkdfasdr2345234a&userId=" + userId;
        
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            var xmlhttp = new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function ()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                console.log("[RESH] httpGet status: 200, url:" + url);
                var response = xmlhttp.responseText;

                var jsonArray = response.split("<!-- __PTS_MVC_VIEW_OUTPUT__ -->");
                var json = jsonArray[1];
                var jsonfied = JSON.stringify(json);
                console.log(jsonfied);
                var obj = JSON.parse(jsonfied);
                console.log(obj);

                if (typeof obj == "object")
                    console.log("Object");
                else
                    console.log("NO object");
                    

                for (var key in obj)
                {
                    console.log(key + ":" + obj[key]);

                    if (key == "name")
                        name = obj[key];

                    else if (key == "profile-pic")
                        imgSrc = obj[key];
                }

                var el = $(".remote-renderer[data-user-id='" + userId + "']");

                if (el)
                {
                    console.log("[RESH] Update info" + name + ", " + location + ", " + imgSrc);
                    el.find(".user-name").html(name);
                    el.find(".user-location").html(location);
                    el.find(".user-img").attr("src", imgSrc);
                }
                else
                {
                    console.log("[RESH] Render element not found");
                }
            }
        }
        xmlhttp.open("GET", url, false);
        xmlhttp.send();

        
    }

    function updateDisplayTime()
    {
        var mainEl = $(".timer");
        var value = 0;

        var textVal = mainEl.attr("data-value");
        if (textVal )
            value = parseInt(textVal, 10);

        value++;
        mainEl.attr("data-value", value);


        var totalSec = value;
        var hours = parseInt(totalSec / 3600) % 24;
        var minutes = parseInt(totalSec / 60) % 60;
        var seconds = totalSec % 60;

        var result = (hours < 10 ? "" : hours + ":") + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);


        mainEl.find('.text').text(result);



    }

    $(window).resize(ResizeVideos);

    /**
  * Register the document ready handler.
  */
    $(onDomReady);
})(window);

$(document).ready(function ()
{
    /*** RESHAPE FUNCTIONS **/
    $(".startBtn").click(function ()
    {
        $("#connectBtn").click();
        $(this).hide();
        $(".stopBtn").show();
    });

    $(".stopBtn").click(function ()
    {
        $("#disconnectBtn").click();
        $(this).hide();
        $(".startBtn").show();
    });

    $(".mute-video").click(function ()
    {
        var el = $("#publishVideoChckbx");

        el.click();

        if (el.is(":checked"))
            $(this).find(".text").html("HIDE");
        else
            $(this).find(".text").html("SHOW");
    });

    $(".mute-sound").click(function ()
    {
        var el = $("#publishAudioChckbx");

        el.click();

        if( el.is(":checked") )
            $(this).find(".text").html("MUTE");
        else
            $(this).find(".text").html("UNMUTE");
    });

    $(".settingsBtn").click(function ()
    {
        $("#settings-container").show();
    });

    $(".close-settings").click(function ()
    {
        $("#settings-container").hide();
    });

    $("#renderingWrapper").on("mouseenter", ".remote-renderer[data-user-id]", function ()
    {
        console.log("[RESH] Show User info ");
        $(this).find(".user-info").fadeIn();
    });

    $("#renderingWrapper").on("mouseleave", ".remote-renderer[data-user-id]", function ()
    {
        console.log("[RESH] Hide User info ");
        $(this).find(".user-info").fadeOut();
    });
    


    
});