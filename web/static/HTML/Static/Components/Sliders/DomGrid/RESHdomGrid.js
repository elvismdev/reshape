﻿$(document).ready(function ()
{
    $(".dom-grid .item").mouseenter(function ()
    {
        $(this).find(".content-container").fadeIn();
    });

    $(".dom-grid .item").mouseleave(function ()
    {
        $(this).find(".content-container").fadeOut();
    });
});