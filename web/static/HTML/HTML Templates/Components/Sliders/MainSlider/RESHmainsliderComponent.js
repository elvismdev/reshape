// JavaScript Document

$(document).ready(function(e) {
	function changePhoto(nextNum){
		var autoChangePhoto = parseInt($("#autoChangePhoto").val());
		
		if(autoChangePhoto == 0 && nextNum == null){
			return;
		}
		else if(nextNum == null){
			var curNum = parseInt($(".slide:visible").attr("num"));
			var maxNum = parseInt($(".slide").last().attr("num"));
			
			if(curNum == maxNum){
				var nextNum = 1;
			} else {
				var nextNum = curNum;
				nextNum++;
			}
		}
		
		$(".navigate").removeClass("navigateSelected");
		$(".navigate[num='"+ nextNum +"']").addClass("navigateSelected");
		
		$(".slide").hide();
		$(".slide[num='"+ nextNum +"']").fadeIn();
		
		var header = $(".slide[num='"+ nextNum +"']").children(".headerToDisplay").html();
		var content = $(".slide[num='"+ nextNum +"']").children(".contentToDisplay").html();
		
		if(header.length > 0){
			$(".headerToUpdate").html(header);
			$(".contentToUpdate").html(content);
		}
	}
	
	function stopChangingPhotos(){
		$("#autoChangePhoto").val("0");
	}
	
	$(".navigate").click(function(e) {
		var thisNum = parseInt($(this).attr("num"));
		changePhoto(thisNum);
		stopChangingPhotos();
	});
	
	$(".arrowRight").click(function(e) {
		var curNum = parseInt($(".slide:visible").attr("num"));
		var maxNum = parseInt($(".slide").last().attr("num"));
			
			if(curNum == maxNum){
				var nextNum = 1;
			} else {
				var nextNum = curNum;
				nextNum++;
			}
		changePhoto(nextNum);
		stopChangingPhotos();
	});
	$(".arrowLeft").click(function(e) {
		var curNum = parseInt($(".slide:visible").attr("num"));
		var maxNum = parseInt($(".slide").last().attr("num"));
			
			if(curNum == 1){
				var nextNum = maxNum;
			} else {
				var nextNum = curNum;
				nextNum--;
			}
			
			changePhoto(nextNum);
			stopChangingPhotos();
	});
	
	
	setInterval(changePhoto, 4000);
	
});