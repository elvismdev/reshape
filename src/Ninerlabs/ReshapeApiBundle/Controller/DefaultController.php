<?php

namespace Ninerlabs\ReshapeApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ReshapeApiBundle:Default:index.html.twig', array('name' => $name));
    }
}
