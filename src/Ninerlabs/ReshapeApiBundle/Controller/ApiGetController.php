<?php

namespace Ninerlabs\ReshapeApiBundle\Controller;

use Ninerlabs\ReshapeApiBundle\Entity\Slide;
use Ninerlabs\ReshapeApiBundle\Entity\ServiceCode;
use Ninerlabs\ReshapeApiBundle\Entity\Service;
use Ninerlabs\ReshapeApiBundle\Entity\Attrgroup;
//use Ninerlabs\ReshapeApiBundle\Entity\Userclassification;
use Ninerlabs\ReshapeApiBundle\Entity\Socialmedia;
use Ninerlabs\ReshapeApiBundle\Entity\Account;
use Ninerlabs\ReshapeApiBundle\Entity\Album;
use Ninerlabs\ReshapeApiBundle\Entity\Albumitem;
use Ninerlabs\ReshapeApiBundle\Entity\Lineitem;
use Ninerlabs\ReshapeApiBundle\Entity\Pageinfo;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ninerlabs\ReshapeApiBundle\Entity\User;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ApiGetController extends Controller
{
    public function getfromapiAction($token = '1234')
    {
        set_time_limit(3600); // 60 minutes

        $error = array();
        $em = $this->getDoctrine()->getManager('db3');

        $this->importUsers($token);

        $total = $em->createQuery('SELECT count(u) FROM ReshapeApiBundle:User u WHERE u.id > 0')->getSingleScalarResult();

        for ($i = 0; $i <= $total; $i += 20) {
            $error[] = $this->importUsersAssets($token, $total, $i, 20);
        }

        $total = $em->createQuery('SELECT Count(u) FROM ReshapeApiBundle:User u WHERE u.id > 0')->getSingleScalarResult();

        return new Response('<h1>Import process... finished!</h1>');
    }

    private function getApiUrl($api = 'user', $token, $uid=null)
    {
        $apis = array(
            'users' => "http://dash.reshape.net/api/ajax/search/activeUsers?token=$token",
            'slides' => "http://dash.reshape.net/api/ajax/profile/showcase?token=$token&id=$uid",
            'services' => "http://dash.reshape.net/api/ajax/profile/services?token=$token&id=$uid",
            'classifications' => "http://dash.reshape.net/api/ajax/profile/classif?token=$token&id=$uid",
            'albums' => "http://dash.reshape.net/api/ajax/profile/media?token=$token&id=$uid",
            'socials' => "http://dash.reshape.net/api/ajax/profile/social?token=$token&id=$uid",
            'full' => "http://dash.reshape.net/api/ajax/profile/full?token=$token&id=$uid"
        );

        return $apis[$api];
    }

    private function getUrlContent($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (curl_exec($ch) !== false)
            return json_decode(curl_exec($ch));

        throw $this->createNotFoundException('Response Error in: ' . $url);
    }

    private function importUsers($token)
    {
        $em = $this->getDoctrine()->getManager('db3');

        $response_users = $this->getUrlContent($this->getApiUrl('users', $token));

        if (!isset($response_users->error) || $response_users->error == 'true')
            throw $this->createNotFoundException('Requested URL Error Users.');

        foreach ($response_users->users as $u) {
            $user = $em->getRepository('ReshapeApiBundle:User')->findOneByEmail($u->email);

            if (!$user) {
                $user = new User();
            }

            // Saving single user
            $user->setId($u->id);
            $user->setName($u->name);
            $user->setTagline($u->tagLine);
            $user->setDescrShort($u->descr_short);
            $user->setPictureUrl($u->picture_url);
            $user->setVideoId($u->video_id);
            $user->setLogoUrl($u->logo_url);
            $user->setProfileUrl($u->profile_url);
            $user->setSortpriority($u->sortPriority);
            $user->setEmail($u->email);
            $user->setPhone($u->phone);

            $em->persist($user);
            $em->flush();

            if ($file = $this->download_image($user->getPictureUrl(), $user->getId(), 'photo'))
                $user->setPictureLocal('/bundles/reshape/profile/' . $user->getId() . $file);
            if ($file = $this->download_image($user->getLogoUrl(), $user->getId(), 'logo'))
                $user->setLogoLocal('/bundles/reshape/profile/' . $user->getId() . $file);

            $em->persist($user);
            $em->flush();
        }
    }

    private function download_image($image_url, $folder, $image_file, $path = '/bundles/reshape/profile/')
    {
        $extension = pathinfo($image_url);
        if (!isset($extension['extension']))
            $extension = '.jpg';
        else
            $extension = '.' . strtolower($extension['extension']);

        $dir = $this->get('kernel')->getRootDir() . '/../web' . $path . $folder;
        $dir_file = '/' . $image_file . $extension;

        $fs = new Filesystem();

        if (!$fs->exists($dir))
            $fs->mkdir($dir);

        if ($fs->exists($dir . $dir_file)) {
            $fs->remove($dir . $dir_file);
        }

        $fp = fopen($dir . $dir_file, 'w+'); // open file handle

        $ch = curl_init($image_url);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // enable if you want
        curl_setopt($ch, CURLOPT_FILE, $fp); // output to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000); // some large value to allow curl to run for a long time
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
        // curl_setopt($ch, CURLOPT_VERBOSE, true);   // Enable this line to see debug prints
        curl_exec($ch);

        curl_close($ch); // closing curl handle
        fclose($fp); // closing file handle

        return $dir_file;
    }

    private function importUsersAssets($token, $total, $start = 0, $limit = 20)
    {
        $error = array();

        $em = $this->getDoctrine()->getManager('db3');

        $users = $em->createQuery('SELECT u.id FROM ReshapeApiBundle:User u WHERE u.id > 0')
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getResult();

        for ($i=$start; $i<$limit; $i++) {
            $uid = $users[$i]['id'];

            // Saving user slides
            $response_slides = $this->getUrlContent($this->getApiUrl('slides', $token, $uid));

            if (!isset($response_slides->error) || $response_slides->error == 'true') {
                $error[] = 'Requested URL Error Slides.';
                continue;
            }

            foreach ($response_slides->slides as $s) {
                $slide = $em->getRepository('ReshapeApiBundle:Slide')->findOneByTitle($s->title);

                if (!$slide) {
                    $slide = new Slide();
                }

                $slide->setId($s->slideId);
                $slide->setTitle($s->title);
                $slide->setDescription($s->description);
                $slide->setSortOrder($s->sortorder);
                $slide->setPictureUrl($s->picture_url);
                $slide->setVideoId($s->video_id);
                $slide->setIsactive($s->isActive == 'true' ? true : false);
                $slide->setUserid($uid);

                $em->persist($slide);
                $em->flush();

                if ($file = $this->download_image($slide->getPictureUrl(), $uid, 'slide' . $slide->getId())) {
                    $slide->setPictureLocal('/bundles/reshape/profile/' . $uid . $file);
                    $em->persist($slide);
                    $em->flush();
                }
            }

            $response_services = $this->getUrlContent($this->getApiUrl('services', $token, $uid));

            if (!isset($response_services->error) || $response_services->error == 'true') {
                $error[] = 'Requested URL Error Services.';
                continue;
            }

            foreach ($response_services->serviceCodes as $c) {
                $code = $em->getRepository('ReshapeApiBundle:ServiceCode')->findOneByCode($c->code);

                if (!$code) {
                    $code = new ServiceCode();
                }

                $code->setCode($c->code);
                $code->setName($c->name);

                $em->persist($code);
            }
            $em->flush();

            foreach ($response_services->services as $s) {
                $service = $em->getRepository('ReshapeApiBundle:Service')->findOneByName($s->name);

                if (!$service) {
                    $service = new Service();
                }

                $service->setName($s->name);
                $code = $em->getRepository('ReshapeApiBundle:ServiceCode')->findOneByCode($s->typeCode);
                if ($code)
                    $service->setTypecode($code->getCode());
                $service->setId($s->serviceId);
                //$service->setTypelabel($s->typelabel);
                $service->setPrice($s->price);
                $service->setCurrencyprice($s->currencyPrice);
                $service->setMinutes($s->minutes);
                $service->setDescription($s->description);
                $service->setSortorder($s->sortorder);
                $service->setIsactive($s->isActive == 'true' ? true : false);
                $service->setPictureUrl($s->picture_url);
                $service->setVideoId($s->video_id);
                $service->setLogoUrl($s->logo_url);
                $service->setUserId($uid);

                $em->persist($service);
                $em->flush();

                if ($file = $this->download_image($service->getPictureUrl(), $uid, 'service_picture' . $service->getId()))
                    $service->setPictureLocal('/bundles/reshape/profile/' . $uid . $file);
                if ($file = $this->download_image($service->getLogoUrl(), $uid, 'service_logo' . $service->getId()))
                    $service->setLogoLocal('/bundles/reshape/profile/' . $uid . $file);

                $em->persist($service);
                $em->flush();
            }

            $response_classifications = $this->getUrlContent($this->getApiUrl('classifications', $token, $uid));

            if (!isset($response_classifications->error) || $response_classifications->error == 'true') {
                $error[] = 'Requested URL Error Classifications.';
                continue;
            }

            foreach ($response_classifications->attrGroups as $c) {
                $attrGroup = $em->getRepository('ReshapeApiBundle:Attrgroup')->findOneByName($c->name);

                if (!$attrGroup) {
                    $attrGroup = new AttrGroup();
                }

                $attrGroup->setId($c->id);
                $attrGroup->setName($c->name);
                $attrGroup->setTagline($c->tagLine);
                $attrGroup->setDescrShort($c->descr_short);
                $attrGroup->setPictureUrl($c->picture_url);
                $attrGroup->setVideoId($c->video_id);
                $attrGroup->setLogoUrl($c->logo_url);
                $attrGroup->setProfileUrl($c->profile_url);
                $attrGroup->setSortPriority($c->sortPriority);
                $attrGroup->setUserId($uid);

                $em->persist($attrGroup);
                $em->flush();

                if ($file = $this->download_image($attrGroup->getPictureUrl(), $uid, 'group' . $attrGroup->getId())) {
                    $attrGroup->setPictureLocal('/bundles/reshape/profile/' . $uid . $file);
                    $em->persist($attrGroup);
                    $em->flush();
                }

                foreach ($c->attributes as $s) {
                    $son = $em->getRepository('ReshapeApiBundle:Attrgroup')->findOneBy(
                        array(
                            'name' => $s->name,
                            'groupId' => $attrGroup->getId())
                    );

                    if (!$son) {
                        $son = new AttrGroup();
                    }

                    $son->setId($s->id);
                    $son->setName($s->name);
                    $son->setTagline($s->tagLine);
                    $son->setDescrShort($s->descr_short);
                    $son->setPictureUrl($s->picture_url);
                    $son->setVideoId($s->video_id);
                    $son->setLogoUrl($s->logo_url);
                    $son->setProfileUrl($s->profile_url);
                    $son->setSortPriority($s->sortPriority);
                    $son->setGroupId($attrGroup->getId());
                    $son->setUserId($uid);

                    $em->persist($son);
                    $em->flush();

                    if ($file = $this->download_image($son->getPictureUrl(), $uid, 'group' . $son->getId())) {
                        $son->setPictureLocal('/bundles/reshape/profile/' . $uid . $file);
                        $em->persist($son);
                        $em->flush();
                    }
                }
            }

            // Saving user albums
            $response_albums = $this->getUrlContent($this->getApiUrl('albums', $token, $uid));

            if (!isset($response_albums->error) || $response_albums->error == 'true') {
                $error[] = 'Requested URL Error Albums.';
                continue;
            }

            foreach ($response_albums->albums as $a) {
                $album = $em->getRepository('ReshapeApiBundle:Album')->findOneByName($a->name);

                if (!$album) {
                    $album = new Album();
                }

                $album->setId($a->id);
                $album->setName($a->name);
                $album->setTagline($a->tagLine);
                $album->setDescrShort($a->descr_short);
                $album->setPictureUrl($a->picture_url);
                $album->setVideoId($a->video_id);
                $album->setLogoUrl($a->logo_url);
                $album->setProfileUrl($a->profile_url);
                $album->setSortpriority($a->sortPriority);
                $album->setIsactive($a->isActive == 'true' ? true : false);
                $album->setAlbumsortpriority($a->albumSortPriority);
                $album->setCreatedon($a->createdOn);
                $album->setAlbumownerid($a->albumOwnerId);
                $album->setAlbumdescription($a->albumDescription);

                $em->persist($album);
                $em->flush();

                if ($file = $this->download_image($album->getPictureUrl(), $uid, 'album' . $album->getId())) {
                    $album->setPictureLocal('/bundles/reshape/profile/' . $uid . $file);
                    $em->persist($album);
                    $em->flush();
                }

                foreach ($a->albumItems as $it) {
                    $item = $em->getRepository('ReshapeApiBundle:Albumitem')->findOneById($it->id);

                    if (!$item) {
                        $item = new AlbumItem();
                    }

                    $item->setId($it->id);
                    $item->setVideoid($it->videoId);
                    $item->setDescription($it->description);
                    $item->setDatetime($it->dateTime);
                    $item->setThumbUrl($it->thumbUrl);
                    $item->setFullsizeurl($it->fullSizeUrl);
                    $item->setFilewidth($it->fileWidth);
                    $item->setFileheight($it->fileHeight);
                    $item->setFilesize($it->fileSize);
                    $item->setFilename($it->fileName);
                    $item->setFileext($it->fileExt);
                    $item->setAlbumId($album->getId());
                    $item->setIscover($it->isCover == 'true' ? true : false);

                    $em->persist($item);
                    $em->flush();

                    if ($file = $this->download_image($item->getThumbUrl(), $uid, 'item_thumb' . $item->getId()))
                        $item->setThumbLocal('/bundles/reshape/profile/' . $uid . $file);
                    if ($file = $this->download_image($item->getFullSizeUrl(), $uid, 'item_full_size' . $item->getId()))
                        $item->setFullsizeLocal('/bundles/reshape/profile/' . $uid . $file);

                    $em->persist($item);
                    $em->flush();
                }
            }

            $response_social = $this->getUrlContent($this->getApiUrl('socials', $token, $uid));

            if (!isset($response_social->error) || $response_social->error == 'true') {
                $error[] = 'Requested URL Error Socials.';
                continue;
            }

            foreach ($response_social->socialMedia->accounts as $s) {
                $social = $em->getRepository('ReshapeApiBundle:Account')->findOneBy(array('account' => $s->account, 'userId' => $uid));

                if (!$social) {
                    $social = new Account();
                }

                $social->setLabel($s->label);
                $social->setIsactive($s->isActive == 'true' ? true : false);
                $social->setAccount($s->account);
                $social->setKey($s->key);
                $social->setUrl($s->url);
                $social->setUserid($uid);

                $em->persist($social);
            }
            $em->flush();

            $response_full = $this->getUrlContent($this->getApiUrl('full', $token, $uid));

            if (!isset($response_full->error) || $response_full->error == 'true') {
                $error[] = 'Requested URL Error Full Profile.';
                continue;
            }

            foreach ($response_full->page->pageInfo->pageData->lineItems as $l) {
                $line = $em->getRepository('ReshapeApiBundle:Lineitem')->findOneBy(array('name' => $l->name, 'userId' => $uid));

                if (!$line) {
                    $line = new Lineitem();
                }

                $line->setName($l->name);
                $line->setType($l->type);
                $line->setUserId($uid);

                $em->persist($line);
            }
            $em->flush();

            foreach ($response_full->page as $l) {
                //print_r($l->pageData->publicAlias);exit;
                $page = $em->getRepository('ReshapeApiBundle:Pageinfo')->findOneBy(array('publicalias' => $l->pageData->publicAlias, 'userId' => $uid));

                if (!$page) {
                    $page = new Pageinfo();
                }

                $page->setIsdefined($l->isDefined == 'true' ? true : false);
                $page->setPublicalias($l->pageData->publicAlias);
                $page->setTagline($l->pageData->tagLine);
                $page->setTitle($l->pageData->title);
                $page->setBio($l->pageData->bio);
                $page->setCity($l->pageData->city);
                $page->setRegion($l->pageData->region);
                $page->setMaindegree($l->pageData->mainDegree);
                $page->setMainschool($l->pageData->mainSchool);
                $page->setBackcolor($l->pageData->backColor);
                $page->setWebsite($l->pageData->website);
                $page->setUserid($uid);

                $em->persist($page);
            }
            $em->flush();

            unset($users[$i]);
        }

        return $error;
    }
}
