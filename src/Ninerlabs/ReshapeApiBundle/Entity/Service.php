<?php

namespace Ninerlabs\ReshapeApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 */
class Service
{
    /**
     * @var integer
     */
    private $sfid;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $typelabel;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $currencyprice;

    /**
     * @var integer
     */
    private $minutes;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $sortorder;

    /**
     * @var boolean
     */
    private $isactive;

    /**
     * @var string
     */
    private $pictureUrl;

    /**
     * @var string
     */
    private $videoId;

    /**
     * @var string
     */
    private $logoUrl;

    /**
     * @var string
     */
    private $pictureLocal;

    /**
     * @var string
     */
    private $logoLocal;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $typecode;


    /**
     * Get sfid
     *
     * @return integer 
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Service
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set typelabel
     *
     * @param string $typelabel
     * @return Service
     */
    public function setTypelabel($typelabel)
    {
        $this->typelabel = $typelabel;

        return $this;
    }

    /**
     * Get typelabel
     *
     * @return string 
     */
    public function getTypelabel()
    {
        return $this->typelabel;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Service
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set currencyprice
     *
     * @param string $currencyprice
     * @return Service
     */
    public function setCurrencyprice($currencyprice)
    {
        $this->currencyprice = $currencyprice;

        return $this;
    }

    /**
     * Get currencyprice
     *
     * @return string 
     */
    public function getCurrencyprice()
    {
        return $this->currencyprice;
    }

    /**
     * Set minutes
     *
     * @param integer $minutes
     * @return Service
     */
    public function setMinutes($minutes)
    {
        $this->minutes = $minutes;

        return $this;
    }

    /**
     * Get minutes
     *
     * @return integer 
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Service
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sortorder
     *
     * @param integer $sortorder
     * @return Service
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;

        return $this;
    }

    /**
     * Get sortorder
     *
     * @return integer 
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }

    /**
     * Set isactive
     *
     * @param boolean $isactive
     * @return Service
     */
    public function setIsactive($isactive)
    {
        $this->isactive = $isactive;

        return $this;
    }

    /**
     * Get isactive
     *
     * @return boolean 
     */
    public function getIsactive()
    {
        return $this->isactive;
    }

    /**
     * Set pictureUrl
     *
     * @param string $pictureUrl
     * @return Service
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    /**
     * Get pictureUrl
     *
     * @return string 
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * Set videoId
     *
     * @param string $videoId
     * @return Service
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return string 
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set logoUrl
     *
     * @param string $logoUrl
     * @return Service
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    /**
     * Get logoUrl
     *
     * @return string 
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     * Set pictureLocal
     *
     * @param string $pictureLocal
     * @return Service
     */
    public function setPictureLocal($pictureLocal)
    {
        $this->pictureLocal = $pictureLocal;

        return $this;
    }

    /**
     * Get pictureLocal
     *
     * @return string 
     */
    public function getPictureLocal()
    {
        return $this->pictureLocal;
    }

    /**
     * Set logoLocal
     *
     * @param string $logoLocal
     * @return Service
     */
    public function setLogoLocal($logoLocal)
    {
        $this->logoLocal = $logoLocal;

        return $this;
    }

    /**
     * Get logoLocal
     *
     * @return string 
     */
    public function getLogoLocal()
    {
        return $this->logoLocal;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Service
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    public function setTypecode($typecode)
    {
        $this->typecode = $typecode;

        return $this;
    }

    public function getTypecode()
    {
        return $this->typecode;
    }
}
