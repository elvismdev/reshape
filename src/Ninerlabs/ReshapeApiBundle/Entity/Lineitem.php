<?php

namespace Ninerlabs\ReshapeApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lineitem
 */
class Lineitem
{
    /**
     * @var integer
     */
    private $sfid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    private $userId;


    /**
     * Get sfid
     *
     * @return integer 
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Lineitem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Lineitem
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
