<?php

namespace Ninerlabs\ReshapeApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Attrgroup
 */
class Attrgroup
{
    /**
     * @var integer
     */
    private $sfid;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $tagline;

    /**
     * @var string
     */
    private $descrShort;

    /**
     * @var string
     */
    private $pictureUrl;

    /**
     * @var string
     */
    private $pictureLocal;

    /**
     * @var string
     */
    private $videoId;

    /**
     * @var string
     */
    private $logoUrl;

    /**
     * @var string
     */
    private $profileUrl;

    /**
     * @var integer
     */
    private $sortpriority;

    /**
     * @var integer
     */
    private $groupId;

    /**
     * @var integer
     */
    private $userId;

    /**
     * Get sfid
     *
     * @return integer 
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Attrgroup
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Attrgroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tagline
     *
     * @param string $tagline
     * @return Attrgroup
     */
    public function setTagline($tagline)
    {
        $this->tagline = $tagline;

        return $this;
    }

    /**
     * Get tagline
     *
     * @return string 
     */
    public function getTagline()
    {
        return $this->tagline;
    }

    /**
     * Set descrShort
     *
     * @param string $descrShort
     * @return Attrgroup
     */
    public function setDescrShort($descrShort)
    {
        $this->descrShort = $descrShort;

        return $this;
    }

    /**
     * Get descrShort
     *
     * @return string 
     */
    public function getDescrShort()
    {
        return $this->descrShort;
    }

    /**
     * Set pictureUrl
     *
     * @param string $pictureUrl
     * @return Attrgroup
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    /**
     * Get pictureUrl
     *
     * @return string 
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * Set videoId
     *
     * @param string $videoId
     * @return Attrgroup
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return string 
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set logoUrl
     *
     * @param string $logoUrl
     * @return Attrgroup
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    /**
     * Get logoUrl
     *
     * @return string 
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     * Set profileUrl
     *
     * @param string $profileUrl
     * @return Attrgroup
     */
    public function setProfileUrl($profileUrl)
    {
        $this->profileUrl = $profileUrl;

        return $this;
    }

    /**
     * Get profileUrl
     *
     * @return string 
     */
    public function getProfileUrl()
    {
        return $this->profileUrl;
    }

    /**
     * Set sortpriority
     *
     * @param integer $sortpriority
     * @return Attrgroup
     */
    public function setSortpriority($sortpriority)
    {
        $this->sortpriority = $sortpriority;

        return $this;
    }

    /**
     * Get sortpriority
     *
     * @return integer 
     */
    public function getSortpriority()
    {
        return $this->sortpriority;
    }

    /**
     * Set groupId
     *
     * @param integer $groupId
     * @return Attrgroup
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId
     *
     * @return integer 
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param string $pictureLocal
     */
    public function setPictureLocal($pictureLocal)
    {
        $this->pictureLocal = $pictureLocal;
    }

    /**
     * @return string
     */
    public function getPictureLocal()
    {
        return $this->pictureLocal;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
