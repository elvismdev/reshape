<?php

namespace Ninerlabs\ReshapeApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Userclassification
 */
class Userclassification
{
    /**
     * @var integer
     */
    private $sfid;

    private $userid;

    private $groupid;


    /**
     * Get sfid
     *
     * @return integer 
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    public function getUserid()
    {
        return $this->userid;
    }

    public function setGroupid($groupid)
    {
        $this->groupid = $groupid;

        return $this;
    }

    public function getGroupid()
    {
        return $this->groupid;
    }
}
