<?php

namespace Ninerlabs\ReshapeApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class User {
    private $sfid;
    private $id;
    private $name;
    private $tagline;
    private $descrShort;
    private $pictureUrl;
    private $videoId;
    private $logoUrl;
    private $profileUrl;
    private $sortpriority;
    private $email;
    private $phone;
    private $pictureLocal;
    private $logoLocal;

    /**
     * @param mixed $descrShort
     */
    public function setDescrShort($descrShort)
    {
        $this->descrShort = $descrShort;
    }

    /**
     * @return mixed
     */
    public function getDescrShort()
    {
        return $this->descrShort;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $logoUrl
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;
    }

    /**
     * @return mixed
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $pictureUrl
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;
    }

    /**
     * @return mixed
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * @param mixed $profileUrl
     */
    public function setProfileUrl($profileUrl)
    {
        $this->profileUrl = $profileUrl;
    }

    /**
     * @return mixed
     */
    public function getProfileUrl()
    {
        return $this->profileUrl;
    }

    /**
     * @param mixed $sfid
     */
    public function setSfid($sfid)
    {
        $this->sfid = $sfid;
    }

    /**
     * @return mixed
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    /**
     * @param mixed $sortpriority
     */
    public function setSortpriority($sortpriority)
    {
        $this->sortpriority = $sortpriority;
    }

    /**
     * @return mixed
     */
    public function getSortpriority()
    {
        return $this->sortpriority;
    }

    /**
     * @param mixed $tagline
     */
    public function setTagline($tagline)
    {
        $this->tagline = $tagline;
    }

    /**
     * @return mixed
     */
    public function getTagline()
    {
        return $this->tagline;
    }

    /**
     * @param mixed $videoId
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;
    }

    /**
     * @return mixed
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * @param mixed $logoLocal
     */
    public function setLogoLocal($logoLocal)
    {
        $this->logoLocal = $logoLocal;
    }

    /**
     * @return mixed
     */
    public function getLogoLocal()
    {
        return $this->logoLocal;
    }

    /**
     * @param mixed $pictureLocal
     */
    public function setPictureLocal($pictureLocal)
    {
        $this->pictureLocal = $pictureLocal;
    }

    /**
     * @return mixed
     */
    public function getPictureLocal()
    {
        return $this->pictureLocal;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
