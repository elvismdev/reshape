<?php

namespace Ninerlabs\ReshapeApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pageinfo
 */
class Pageinfo
{
    /**
     * @var integer
     */
    private $sfid;

    /**
     * @var boolean
     */
    private $isdefined;

    /**
     * @var string
     */
    private $publicalias;

    /**
     * @var string
     */
    private $tagline;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $bio;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $maindegree;

    /**
     * @var string
     */
    private $mainschool;

    /**
     * @var string
     */
    private $backcolor;

    /**
     * @var string
     */
    private $website;

    /**
     * @var integer
     */
    private $userId;


    /**
     * Get sfid
     *
     * @return integer 
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    /**
     * Set isdefined
     *
     * @param boolean $isdefined
     * @return Pageinfo
     */
    public function setIsdefined($isdefined)
    {
        $this->isdefined = $isdefined;

        return $this;
    }

    /**
     * Get isdefined
     *
     * @return boolean 
     */
    public function getIsdefined()
    {
        return $this->isdefined;
    }

    /**
     * Set publicalias
     *
     * @param string $publicalias
     * @return Pageinfo
     */
    public function setPublicalias($publicalias)
    {
        $this->publicalias = $publicalias;

        return $this;
    }

    /**
     * Get publicalias
     *
     * @return string 
     */
    public function getPublicalias()
    {
        return $this->publicalias;
    }

    /**
     * Set tagline
     *
     * @param string $tagline
     * @return Pageinfo
     */
    public function setTagline($tagline)
    {
        $this->tagline = $tagline;

        return $this;
    }

    /**
     * Get tagline
     *
     * @return string 
     */
    public function getTagline()
    {
        return $this->tagline;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Pageinfo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set bio
     *
     * @param string $bio
     * @return Pageinfo
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string 
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Pageinfo
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Pageinfo
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set maindegree
     *
     * @param string $maindegree
     * @return Pageinfo
     */
    public function setMaindegree($maindegree)
    {
        $this->maindegree = $maindegree;

        return $this;
    }

    /**
     * Get maindegree
     *
     * @return string 
     */
    public function getMaindegree()
    {
        return $this->maindegree;
    }

    /**
     * Set mainschool
     *
     * @param string $mainschool
     * @return Pageinfo
     */
    public function setMainschool($mainschool)
    {
        $this->mainschool = $mainschool;

        return $this;
    }

    /**
     * Get mainschool
     *
     * @return string 
     */
    public function getMainschool()
    {
        return $this->mainschool;
    }

    /**
     * Set backcolor
     *
     * @param string $backcolor
     * @return Pageinfo
     */
    public function setBackcolor($backcolor)
    {
        $this->backcolor = $backcolor;

        return $this;
    }

    /**
     * Get backcolor
     *
     * @return string 
     */
    public function getBackcolor()
    {
        return $this->backcolor;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Pageinfo
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Pageinfo
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
