<?php

namespace Ninerlabs\ReshapeApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Albumitem
 */
class Albumitem
{
    /**
     * @var integer
     */
    private $sfid;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $videoid;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $datetime;

    /**
     * @var string
     */
    private $thumburl;

    /**
     * @var string
     */
    private $fullsizeurl;

    /**
     * @var integer
     */
    private $filewidth;

    /**
     * @var integer
     */
    private $fileheight;

    /**
     * @var integer
     */
    private $filesize;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $fileext;

    /**
     * @var boolean
     */
    private $iscover;

    /**
     * @var integer
     */
    private $albumId;

    /**
     * @var string
     */
    private $thumbLocal;

    /**
     * @var string
     */
    private $fullsizeLocal;


    /**
     * Get sfid
     *
     * @return integer 
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Albumitem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set videoid
     *
     * @param string $videoid
     * @return Albumitem
     */
    public function setVideoid($videoid)
    {
        $this->videoid = $videoid;

        return $this;
    }

    /**
     * Get videoid
     *
     * @return string 
     */
    public function getVideoid()
    {
        return $this->videoid;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Albumitem
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set datetime
     *
     * @param string $datetime
     * @return Albumitem
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return string 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set thumburl
     *
     * @param string $thumburl
     * @return Albumitem
     */
    public function setThumburl($thumburl)
    {
        $this->thumburl = $thumburl;

        return $this;
    }

    /**
     * Get thumburl
     *
     * @return string 
     */
    public function getThumburl()
    {
        return $this->thumburl;
    }

    /**
     * Set fullsizeurl
     *
     * @param string $fullsizeurl
     * @return Albumitem
     */
    public function setFullsizeurl($fullsizeurl)
    {
        $this->fullsizeurl = $fullsizeurl;

        return $this;
    }

    /**
     * Get fullsizeurl
     *
     * @return string 
     */
    public function getFullsizeurl()
    {
        return $this->fullsizeurl;
    }

    /**
     * Set filewidth
     *
     * @param integer $filewidth
     * @return Albumitem
     */
    public function setFilewidth($filewidth)
    {
        $this->filewidth = $filewidth;

        return $this;
    }

    /**
     * Get filewidth
     *
     * @return integer 
     */
    public function getFilewidth()
    {
        return $this->filewidth;
    }

    /**
     * Set fileheight
     *
     * @param integer $fileheight
     * @return Albumitem
     */
    public function setFileheight($fileheight)
    {
        $this->fileheight = $fileheight;

        return $this;
    }

    /**
     * Get fileheight
     *
     * @return integer 
     */
    public function getFileheight()
    {
        return $this->fileheight;
    }

    /**
     * Set filesize
     *
     * @param integer $filesize
     * @return Albumitem
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }

    /**
     * Get filesize
     *
     * @return integer 
     */
    public function getFilesize()
    {
        return $this->filesize;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Albumitem
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set fileext
     *
     * @param string $fileext
     * @return Albumitem
     */
    public function setFileext($fileext)
    {
        $this->fileext = $fileext;

        return $this;
    }

    /**
     * Get fileext
     *
     * @return string 
     */
    public function getFileext()
    {
        return $this->fileext;
    }

    /**
     * Set iscover
     *
     * @param boolean $iscover
     * @return Albumitem
     */
    public function setIscover($iscover)
    {
        $this->iscover = $iscover;

        return $this;
    }

    /**
     * Get iscover
     *
     * @return boolean 
     */
    public function getIscover()
    {
        return $this->iscover;
    }

    /**
     * Set albumId
     *
     * @param integer $albumId
     * @return Albumitem
     */
    public function setAlbumId($albumId)
    {
        $this->albumId = $albumId;

        return $this;
    }

    /**
     * Get albumId
     *
     * @return integer 
     */
    public function getAlbumId()
    {
        return $this->albumId;
    }

    /**
     * Set thumbLocal
     *
     * @param string $thumbLocal
     * @return Albumitem
     */
    public function setThumbLocal($thumbLocal)
    {
        $this->thumbLocal = $thumbLocal;

        return $this;
    }

    /**
     * Get thumbLocal
     *
     * @return string 
     */
    public function getThumbLocal()
    {
        return $this->thumbLocal;
    }

    /**
     * Set fullsizeLocal
     *
     * @param string $fullsizeLocal
     * @return Albumitem
     */
    public function setFullsizeLocal($fullsizeLocal)
    {
        $this->fullsizeLocal = $fullsizeLocal;

        return $this;
    }

    /**
     * Get fullsizeLocal
     *
     * @return string 
     */
    public function getFullsizeLocal()
    {
        return $this->fullsizeLocal;
    }
}
