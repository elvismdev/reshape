<?php

namespace Ninerlabs\ReshapeApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Album
 */
class Album
{
    /**
     * @var integer
     */
    private $sfid;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $tagline;

    /**
     * @var string
     */
    private $descrShort;

    /**
     * @var string
     */
    private $pictureUrl;

    /**
     * @var string
     */
    private $videoId;

    /**
     * @var string
     */
    private $logoUrl;

    /**
     * @var string
     */
    private $profileUrl;

    /**
     * @var integer
     */
    private $sortpriority;

    /**
     * @var boolean
     */
    private $isactive;

    /**
     * @var integer
     */
    private $albumsortpriority;

    /**
     * @var \DateTime
     */
    private $createdon;

    /**
     * @var string
     */
    private $takenon;

    /**
     * @var integer
     */
    private $albumownerid;

    /**
     * @var string
     */
    private $albumdescription;

    /**
     * @var string
     */
    private $logoLocal;

    /**
     * @var string
     */
    private $pictureLocal;


    /**
     * Get sfid
     *
     * @return integer 
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Album
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Album
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tagline
     *
     * @param string $tagline
     * @return Album
     */
    public function setTagline($tagline)
    {
        $this->tagline = $tagline;

        return $this;
    }

    /**
     * Get tagline
     *
     * @return string 
     */
    public function getTagline()
    {
        return $this->tagline;
    }

    /**
     * Set descrShort
     *
     * @param string $descrShort
     * @return Album
     */
    public function setDescrShort($descrShort)
    {
        $this->descrShort = $descrShort;

        return $this;
    }

    /**
     * Get descrShort
     *
     * @return string 
     */
    public function getDescrShort()
    {
        return $this->descrShort;
    }

    /**
     * Set pictureUrl
     *
     * @param string $pictureUrl
     * @return Album
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    /**
     * Get pictureUrl
     *
     * @return string 
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * Set videoId
     *
     * @param string $videoId
     * @return Album
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return string 
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set logoUrl
     *
     * @param string $logoUrl
     * @return Album
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    /**
     * Get logoUrl
     *
     * @return string 
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     * Set profileUrl
     *
     * @param string $profileUrl
     * @return Album
     */
    public function setProfileUrl($profileUrl)
    {
        $this->profileUrl = $profileUrl;

        return $this;
    }

    /**
     * Get profileUrl
     *
     * @return string 
     */
    public function getProfileUrl()
    {
        return $this->profileUrl;
    }

    /**
     * Set sortpriority
     *
     * @param integer $sortpriority
     * @return Album
     */
    public function setSortpriority($sortpriority)
    {
        $this->sortpriority = $sortpriority;

        return $this;
    }

    /**
     * Get sortpriority
     *
     * @return integer 
     */
    public function getSortpriority()
    {
        return $this->sortpriority;
    }

    /**
     * Set isactive
     *
     * @param boolean $isactive
     * @return Album
     */
    public function setIsactive($isactive)
    {
        $this->isactive = $isactive;

        return $this;
    }

    /**
     * Get isactive
     *
     * @return boolean 
     */
    public function getIsactive()
    {
        return $this->isactive;
    }

    /**
     * Set albumsortpriority
     *
     * @param integer $albumsortpriority
     * @return Album
     */
    public function setAlbumsortpriority($albumsortpriority)
    {
        $this->albumsortpriority = $albumsortpriority;

        return $this;
    }

    /**
     * Get albumsortpriority
     *
     * @return integer 
     */
    public function getAlbumsortpriority()
    {
        return $this->albumsortpriority;
    }

    public function setCreatedon($createdon)
    {
        $this->createdon = $createdon;

        return $this;
    }

    public function getCreatedon()
    {
        return $this->createdon;
    }

    /**
     * Set takenon
     *
     * @param string $takenon
     * @return Album
     */
    public function setTakenon($takenon)
    {
        $this->takenon = $takenon;

        return $this;
    }

    /**
     * Get takenon
     *
     * @return string 
     */
    public function getTakenon()
    {
        return $this->takenon;
    }

    /**
     * Set albumownerid
     *
     * @param integer $albumownerid
     * @return Album
     */
    public function setAlbumownerid($albumownerid)
    {
        $this->albumownerid = $albumownerid;

        return $this;
    }

    /**
     * Get albumownerid
     *
     * @return integer 
     */
    public function getAlbumownerid()
    {
        return $this->albumownerid;
    }

    /**
     * Set albumdescription
     *
     * @param string $albumdescription
     * @return Album
     */
    public function setAlbumdescription($albumdescription)
    {
        $this->albumdescription = $albumdescription;

        return $this;
    }

    /**
     * Get albumdescription
     *
     * @return string 
     */
    public function getAlbumdescription()
    {
        return $this->albumdescription;
    }

    /**
     * Set logoLocal
     *
     * @param string $logoLocal
     * @return Album
     */
    public function setLogoLocal($logoLocal)
    {
        $this->logoLocal = $logoLocal;

        return $this;
    }

    /**
     * Get logoLocal
     *
     * @return string 
     */
    public function getLogoLocal()
    {
        return $this->logoLocal;
    }

    /**
     * Set pictureLocal
     *
     * @param string $pictureLocal
     * @return Album
     */
    public function setPictureLocal($pictureLocal)
    {
        $this->pictureLocal = $pictureLocal;

        return $this;
    }

    /**
     * Get pictureLocal
     *
     * @return string 
     */
    public function getPictureLocal()
    {
        return $this->pictureLocal;
    }
}
