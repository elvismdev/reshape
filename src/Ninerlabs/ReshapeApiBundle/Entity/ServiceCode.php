<?php

namespace Ninerlabs\ReshapeApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceCode
 */
class ServiceCode
{
    /**
     * @var integer
     */
    private $sfid;

    /**
     * @var integer
     */
    private $code;

    /**
     * @var string
     */
    private $name;


    /**
     * Get sfid
     *
     * @return integer 
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    /**
     * Set code
     *
     * @param integer $code
     * @return ServiceCode
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ServiceCode
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
