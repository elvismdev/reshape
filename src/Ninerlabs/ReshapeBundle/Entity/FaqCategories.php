<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FaqCategories
 */
class FaqCategories
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $catName;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set catName
     *
     * @param string $catName
     * @return FaqCategories
     */
    public function setCatName($catName)
    {
        $this->catName = $catName;

        return $this;
    }

    /**
     * Get catName
     *
     * @return string 
     */
    public function getCatName()
    {
        return $this->catName;
    }

    public function __toString()
    {
        return $this->getCatName();
    }
}
