<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * ProfessionalUser
 */
class ProfessionalUser implements AdvancedUserInterface
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $birthDate;

    /**
     * @var string
     */
    private $genre;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var integer
     */
    private $tc;

    /**
     * @var string
     */
    private $photo;

    /**
     * @var string
     */
    private $website;

    /**
     * @var string
     */
    private $facebook;

    /**
     * @var string
     */
    private $twitter;

    /**
     * @var string
     */
    private $instagram;

    /**
     * @var string
     */
    private $youtube;

    /**
     * @var string
     */
    private $trainTeach;

    /**
     * @var string
     */
    private $videoBefore;

    /**
     * @var string
     */
    private $reshapeHear;

    /**
     * @var string
     */
    private $addInfo;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ProfExp
     */
    private $profExp;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\References
     */
    private $references;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\Certifications
     */
    private $certifications;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\Education
     */
    private $education;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\Specialty
     */
    private $specialty;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ClientSpec
     */
    private $clientSpec;

    private $salt;

    private $rol;

    private $status;

    private $professional;

    private $created;

    private $complete;

    private $description;

    private $videoId;

    private $logoUrl;

    private $profileUrl;

    private $sortPriority;

    private $publicAlias;

    private $tagLine;

    private $bio;

    private $region;

    private $mainDegree;

    private $mainSchool;

    private $backColor;

    private $photoLocal;

    private $logoLocal;

    private $apiId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return ProfessionalUser
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return ProfessionalUser
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return ProfessionalUser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return ProfessionalUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return ProfessionalUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return ProfessionalUser
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return ProfessionalUser
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return ProfessionalUser
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return ProfessionalUser
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return ProfessionalUser
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return ProfessionalUser
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set birthDate
     *
     * @param string $birthDate
     * @return ProfessionalUser
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set genre
     *
     * @param string $genre
     * @return ProfessionalUser
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return string
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     * @return ProfessionalUser
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set tc
     *
     * @param integer $tc
     * @return ProfessionalUser
     */
    public function setTc($tc)
    {
        $this->tc = $tc;

        return $this;
    }

    /**
     * Get tc
     *
     * @return integer
     */
    public function getTc()
    {
        return $this->tc;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return ProfessionalUser
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return ProfessionalUser
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return ProfessionalUser
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return ProfessionalUser
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set instagram
     *
     * @param string $instagram
     * @return ProfessionalUser
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Get instagram
     *
     * @return string
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * Set youtube
     *
     * @param string $youtube
     * @return ProfessionalUser
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * Get youtube
     *
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * Set trainTeach
     *
     * @param string $trainTeach
     * @return ProfessionalUser
     */
    public function setTrainTeach($trainTeach)
    {
        $this->trainTeach = $trainTeach;

        return $this;
    }

    /**
     * Get trainTeach
     *
     * @return string
     */
    public function getTrainTeach()
    {
        return $this->trainTeach;
    }

    /**
     * Set videoBefore
     *
     * @param string $videoBefore
     * @return ProfessionalUser
     */
    public function setVideoBefore($videoBefore)
    {
        $this->videoBefore = $videoBefore;

        return $this;
    }

    /**
     * Get videoBefore
     *
     * @return string
     */
    public function getVideoBefore()
    {
        return $this->videoBefore;
    }

    /**
     * Set reshapeHear
     *
     * @param string $reshapeHear
     * @return ProfessionalUser
     */
    public function setReshapeHear($reshapeHear)
    {
        $this->reshapeHear = $reshapeHear;

        return $this;
    }

    /**
     * Get reshapeHear
     *
     * @return string
     */
    public function getReshapeHear()
    {
        return $this->reshapeHear;
    }

    /**
     * Set addInfo
     *
     * @param string $addInfo
     * @return ProfessionalUser
     */
    public function setAddInfo($addInfo)
    {
        $this->addInfo = $addInfo;

        return $this;
    }

    /**
     * Get addInfo
     *
     * @return string
     */
    public function getAddInfo()
    {
        return $this->addInfo;
    }

    /**
     * Set profExp
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ProfExp $profExp
     * @return ProfessionalUser
     */
    public function setProfExp(\Ninerlabs\ReshapeBundle\Entity\ProfExp $profExp = null)
    {
        $this->profExp = $profExp;

        return $this;
    }

    /**
     * Get profExp
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ProfExp
     */
    public function getProfExp()
    {
        return $this->profExp;
    }

    /**
     * Set references
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\References $references
     * @return ProfessionalUser
     */
    public function setReferences(\Ninerlabs\ReshapeBundle\Entity\References $references = null)
    {
        $this->references = $references;

        return $this;
    }

    /**
     * Get references
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\References
     */
    public function getReferences()
    {
        return $this->references;
    }

    /**
     * Set certifications
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\Certifications $certifications
     * @return ProfessionalUser
     */
    public function setCertifications(\Ninerlabs\ReshapeBundle\Entity\Certifications $certifications = null)
    {
        $this->certifications = $certifications;

        return $this;
    }

    /**
     * Get certifications
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\Certifications
     */
    public function getCertifications()
    {
        return $this->certifications;
    }

    /**
     * Set education
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\Education $education
     * @return ProfessionalUser
     */
    public function setEducation(\Ninerlabs\ReshapeBundle\Entity\Education $education = null)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * Get education
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\Education
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * Set specialty
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\Specialty $specialty
     * @return ProfessionalUser
     */
    public function setSpecialty(\Ninerlabs\ReshapeBundle\Entity\Specialty $specialty = null)
    {
        $this->specialty = $specialty;

        return $this;
    }

    /**
     * Get specialty
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\Specialty
     */
    public function getSpecialty()
    {
        return $this->specialty;
    }

    /**
     * Set clientSpec
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ClientSpec $clientSpec
     * @return ProfessionalUser
     */
    public function setClientSpec(\Ninerlabs\ReshapeBundle\Entity\ClientSpec $clientSpec = null)
    {
        $this->clientSpec = $clientSpec;

        return $this;
    }

    /**
     * Get clientSpec
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ClientSpec
     */
    public function getClientSpec()
    {
        return $this->clientSpec;
    }

    /**
     * @param mixed $rol
     */
    public function setRol($rol)
    {
        $this->rol = $rol;
    }

    public function setRoles($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRol()
    {
        return $this->rol;
    }

    public function getRoles()
    {
        return array($this->rol);
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function __toString()
    {
        return $this->username;
    }

    public function getNickname()
    {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {

    }

    public function isAccountNonExpired()
    {
        return $this->status;
    }

    public function isAccountNonLocked()
    {
        return $this->status;
    }

    public function isCredentialsNonExpired()
    {
        return $this->status;
    }

    public function isEnabled()
    {
        return $this->status;
    }

    /**
     * @param mixed $professional
     */
    public function setProfessional($professional)
    {
        $this->professional = $professional;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfessional()
    {
        return $this->professional;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $complete
     */
    public function setComplete($complete)
    {
        $this->complete = $complete;
    }

    /**
     * @return mixed
     */
    public function getComplete()
    {
        return $this->complete;
    }

    /**
     * @param mixed $backColor
     */
    public function setBackColor($backColor)
    {
        $this->backColor = $backColor;
    }

    /**
     * @return mixed
     */
    public function getBackColor()
    {
        return $this->backColor;
    }

    /**
     * @param mixed $bio
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
    }

    /**
     * @return mixed
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $logoUrl
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;
    }

    /**
     * @return mixed
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     * @param mixed $mainDegree
     */
    public function setMainDegree($mainDegree)
    {
        $this->mainDegree = $mainDegree;
    }

    /**
     * @return mixed
     */
    public function getMainDegree()
    {
        return $this->mainDegree;
    }

    /**
     * @param mixed $mainSchool
     */
    public function setMainSchool($mainSchool)
    {
        $this->mainSchool = $mainSchool;
    }

    /**
     * @return mixed
     */
    public function getMainSchool()
    {
        return $this->mainSchool;
    }

    /**
     * @param mixed $profileUrl
     */
    public function setProfileUrl($profileUrl)
    {
        $this->profileUrl = $profileUrl;
    }

    /**
     * @return mixed
     */
    public function getProfileUrl()
    {
        return $this->profileUrl;
    }

    /**
     * @param mixed $publicAlias
     */
    public function setPublicAlias($publicAlias)
    {
        $this->publicAlias = $publicAlias;
    }

    /**
     * @return mixed
     */
    public function getPublicAlias()
    {
        return $this->publicAlias;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $sortPriority
     */
    public function setSortPriority($sortPriority)
    {
        $this->sortPriority = $sortPriority;
    }

    /**
     * @return mixed
     */
    public function getSortPriority()
    {
        return $this->sortPriority;
    }

    /**
     * @param mixed $tagLine
     */
    public function setTagLine($tagLine)
    {
        $this->tagLine = $tagLine;
    }

    /**
     * @return mixed
     */
    public function getTagLine()
    {
        return $this->tagLine;
    }

    /**
     * @param mixed $videoId
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;
    }

    /**
     * @return mixed
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * @param mixed $logoLocal
     */
    public function setLogoLocal($logoLocal)
    {
        $this->logoLocal = $logoLocal;
    }

    /**
     * @return mixed
     */
    public function getLogoLocal()
    {
        return $this->logoLocal;
    }

    /**
     * @param mixed $photoLocal
     */
    public function setPhotoLocal($photoLocal)
    {
        $this->photoLocal = $photoLocal;
    }

    /**
     * @return mixed
     */
    public function getPhotoLocal()
    {
        return $this->photoLocal;
    }

    /**
     * @param mixed $apiId
     */
    public function setApiId($apiId)
    {
        $this->apiId = $apiId;
    }

    /**
     * @return mixed
     */
    public function getApiId()
    {
        return $this->apiId;
    }
}
