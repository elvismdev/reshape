<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AlbumItem
 */
class AlbumItem
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $videoId;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $datetime;

    /**
     * @var string
     */
    private $thumbUrl;

    /**
     * @var string
     */
    private $fullSizeUrl;

    /**
     * @var integer
     */
    private $width;

    /**
     * @var integer
     */
    private $height;

    /**
     * @var integer
     */
    private $size;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $extension;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\Album
     */
    private $album;

    private $localThumbUrl;

    private $localFullSizeUrl;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set videoId
     *
     * @param string $videoId
     * @return AlbumItem
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return string 
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AlbumItem
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set datetime
     *
     * @param string $datetime
     * @return AlbumItem
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return string 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set thumbUrl
     *
     * @param string $thumbUrl
     * @return AlbumItem
     */
    public function setThumbUrl($thumbUrl)
    {
        $this->thumbUrl = $thumbUrl;

        return $this;
    }

    /**
     * Get thumbUrl
     *
     * @return string 
     */
    public function getThumbUrl()
    {
        return $this->thumbUrl;
    }

    /**
     * Set fullSizeUrl
     *
     * @param string $fullSizeUrl
     * @return AlbumItem
     */
    public function setFullSizeUrl($fullSizeUrl)
    {
        $this->fullSizeUrl = $fullSizeUrl;

        return $this;
    }

    /**
     * Get fullSizeUrl
     *
     * @return string 
     */
    public function getFullSizeUrl()
    {
        return $this->fullSizeUrl;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return AlbumItem
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return AlbumItem
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return AlbumItem
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AlbumItem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set extension
     *
     * @param string $extension
     * @return AlbumItem
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string 
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set album
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\Album $album
     * @return AlbumItem
     */
    public function setAlbum(\Ninerlabs\ReshapeBundle\Entity\Album $album = null)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\Album 
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * @param mixed $localFullSizeUrl
     */
    public function setLocalFullSizeUrl($localFullSizeUrl)
    {
        $this->localFullSizeUrl = $localFullSizeUrl;
    }

    /**
     * @return mixed
     */
    public function getLocalFullSizeUrl()
    {
        return $this->localFullSizeUrl;
    }

    /**
     * @param mixed $localThumbUrl
     */
    public function setLocalThumbUrl($localThumbUrl)
    {
        $this->localThumbUrl = $localThumbUrl;
    }

    /**
     * @return mixed
     */
    public function getLocalThumbUrl()
    {
        return $this->localThumbUrl;
    }
}
