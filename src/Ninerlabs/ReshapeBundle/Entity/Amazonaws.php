<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Amazonaws
 */
class Amazonaws
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $awsId;

    /**
     * @var string
     */
    private $awsSecretKey;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $emailName;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set awsId
     *
     * @param string $awsId
     * @return Amazonaws
     */
    public function setAwsId($awsId)
    {
        $this->awsId = $awsId;

        return $this;
    }

    /**
     * Get awsId
     *
     * @return string
     */
    public function getAwsId()
    {
        return $this->awsId;
    }

    /**
     * Set awsSecretKey
     *
     * @param string $awsSecretKey
     * @return Amazonaws
     */
    public function setAwsSecretKey($awsSecretKey)
    {
        $this->awsSecretKey = $awsSecretKey;

        return $this;
    }

    /**
     * Get awsSecretKey
     *
     * @return string
     */
    public function getAwsSecretKey()
    {
        return $this->awsSecretKey;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $emailName
     */
    public function setEmailName($emailName)
    {
        $this->emailName = $emailName;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmailName()
    {
        return $this->emailName;
    }
}
