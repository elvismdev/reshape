<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Albumitems
 */
class Albumitems
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $videoid;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $datetime;

    /**
     * @var string
     */
    private $thumburl;

    private $thumburllocal;

    private $fullurllocal;

    /**
     * @var string
     */
    private $thumblocalpath;

    /**
     * @var string
     */
    private $fullsizeurl;

    /**
     * @var string
     */
    private $fullsizelocalpath;

    /**
     * @var integer
     */
    private $filewidth;

    /**
     * @var integer
     */
    private $fileheight;

    /**
     * @var integer
     */
    private $filesize;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $fileext;

    /**
     * @var boolean
     */
    private $iscover;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set videoid
     *
     * @param integer $videoid
     * @return Albumitems
     */
    public function setVideoid($videoid)
    {
        $this->videoid = $videoid;

        return $this;
    }

    /**
     * Get videoid
     *
     * @return integer 
     */
    public function getVideoid()
    {
        return $this->videoid;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Albumitems
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return Albumitems
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set thumburl
     *
     * @param string $thumburl
     * @return Albumitems
     */
    public function setThumburl($thumburl)
    {
        $this->thumburl = $thumburl;

        return $this;
    }

    /**
     * Get thumburl
     *
     * @return string 
     */
    public function getThumburl()
    {
        return $this->thumburl;
    }

    /**
     * Set thumblocalpath
     *
     * @param string $thumblocalpath
     * @return Albumitems
     */
    public function setThumblocalpath($thumblocalpath)
    {
        $this->thumblocalpath = $thumblocalpath;

        return $this;
    }

    /**
     * Get thumblocalpath
     *
     * @return string 
     */
    public function getThumblocalpath()
    {
        return $this->thumblocalpath;
    }

    /**
     * Set fullsizeurl
     *
     * @param string $fullsizeurl
     * @return Albumitems
     */
    public function setFullsizeurl($fullsizeurl)
    {
        $this->fullsizeurl = $fullsizeurl;

        return $this;
    }

    /**
     * Get fullsizeurl
     *
     * @return string 
     */
    public function getFullsizeurl()
    {
        return $this->fullsizeurl;
    }

    /**
     * Set fullsizelocalpath
     *
     * @param string $fullsizelocalpath
     * @return Albumitems
     */
    public function setFullsizelocalpath($fullsizelocalpath)
    {
        $this->fullsizelocalpath = $fullsizelocalpath;

        return $this;
    }

    /**
     * Get fullsizelocalpath
     *
     * @return string 
     */
    public function getFullsizelocalpath()
    {
        return $this->fullsizelocalpath;
    }

    /**
     * Set filewidth
     *
     * @param integer $filewidth
     * @return Albumitems
     */
    public function setFilewidth($filewidth)
    {
        $this->filewidth = $filewidth;

        return $this;
    }

    /**
     * Get filewidth
     *
     * @return integer 
     */
    public function getFilewidth()
    {
        return $this->filewidth;
    }

    /**
     * Set fileheight
     *
     * @param integer $fileheight
     * @return Albumitems
     */
    public function setFileheight($fileheight)
    {
        $this->fileheight = $fileheight;

        return $this;
    }

    /**
     * Get fileheight
     *
     * @return integer 
     */
    public function getFileheight()
    {
        return $this->fileheight;
    }

    /**
     * Set filesize
     *
     * @param integer $filesize
     * @return Albumitems
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }

    /**
     * Get filesize
     *
     * @return integer 
     */
    public function getFilesize()
    {
        return $this->filesize;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Albumitems
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set fileext
     *
     * @param string $fileext
     * @return Albumitems
     */
    public function setFileext($fileext)
    {
        $this->fileext = $fileext;

        return $this;
    }

    /**
     * Get fileext
     *
     * @return string 
     */
    public function getFileext()
    {
        return $this->fileext;
    }

    /**
     * Set iscover
     *
     * @param boolean $iscover
     * @return Albumitems
     */
    public function setIscover($iscover)
    {
        $this->iscover = $iscover;

        return $this;
    }

    /**
     * Get iscover
     *
     * @return boolean 
     */
    public function getIscover()
    {
        return $this->iscover;
    }
    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\Albums
     */
    private $album;


    /**
     * Set album
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\Albums $album
     * @return Albumitems
     */
    public function setAlbum(\Ninerlabs\ReshapeBundle\Entity\Albums $album = null)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\Albums 
     */
    public function getAlbum()
    {
        return $this->album;
    }
    /**
     * @var integer
     */
    private $iditem;


    /**
     * Set iditem
     *
     * @param integer $iditem
     * @return Albumitems
     */
    public function setIditem($iditem)
    {
        $this->iditem = $iditem;

        return $this;
    }

    /**
     * Get iditem
     *
     * @return integer 
     */
    public function getIditem()
    {
        return $this->iditem;
    }

    /**
     * @param mixed $fullurllocal
     */
    public function setFullurllocal($fullurllocal)
    {
        $this->fullurllocal = $fullurllocal;
    }

    /**
     * @return mixed
     */
    public function getFullurllocal()
    {
        return $this->fullurllocal;
    }

    /**
     * @param mixed $thumburllocal
     */
    public function setThumburllocal($thumburllocal)
    {
        $this->thumburllocal = $thumburllocal;
    }

    /**
     * @return mixed
     */
    public function getThumburllocal()
    {
        return $this->thumburllocal;
    }
}
