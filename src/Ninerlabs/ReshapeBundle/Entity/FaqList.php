<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FaqList
 */
class FaqList
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $question;

    /**
     * @var string
     */
    private $answer;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\FaqCategories
     */
    private $faqCat;

    private $order;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return FaqList
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return FaqList
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set faqCat
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\FaqCategories $faqCat
     * @return FaqList
     */
    public function setFaqCat(\Ninerlabs\ReshapeBundle\Entity\FaqCategories $faqCat = null)
    {
        $this->faqCat = $faqCat;

        return $this;
    }

    /**
     * Get faqCat
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\FaqCategories
     */
    public function getFaqCat()
    {
        return $this->faqCat;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }
}
