<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * References
 */
class References
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $refName;

    /**
     * @var integer
     */
    private $phone;

    /**
     * @var string
     */
    private $relationship;

    function __construct($phone, $refName, $relationship, $userid)
    {
        $this->phone = $phone;
        $this->refName = $refName;
        $this->relationship = $relationship;
        $this->userid = $userid;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set refName
     *
     * @param string $refName
     * @return References
     */
    public function setRefName($refName)
    {
        $this->refName = $refName;

        return $this;
    }

    /**
     * Get refName
     *
     * @return string 
     */
    public function getRefName()
    {
        return $this->refName;
    }

    /**
     * Set phone
     *
     * @param integer $phone
     * @return References
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return integer 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set relationship
     *
     * @param string $relationship
     * @return References
     */
    public function setRelationship($relationship)
    {
        $this->relationship = $relationship;

        return $this;
    }

    /**
     * Get relationship
     *
     * @return string 
     */
    public function getRelationship()
    {
        return $this->relationship;
    }
    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser
     */
    private $userid;


    /**
     * Set userid
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid
     * @return References
     */
    public function setUserid(\Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser 
     */
    public function getUserid()
    {
        return $this->userid;
    }
}
