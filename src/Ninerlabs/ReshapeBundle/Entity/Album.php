<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Album
 */
class Album
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $descrShort;

    /**
     * @var string
     */
    private $pictureUrl;

    /**
     * @var string
     */
    private $profileUrl;

    /**
     * @var integer
     */
    private $sortPriority;

    /**
     * @var integer
     */
    private $albumSortPriority;

    /**
     * @var string
     */
    private $created;

    /**
     * @var string
     */
    private $taken;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser
     */
    private $userid;

    private $localPictureUrl;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Album
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set descrShort
     *
     * @param string $descrShort
     * @return Album
     */
    public function setDescrShort($descrShort)
    {
        $this->descrShort = $descrShort;

        return $this;
    }

    /**
     * Get descrShort
     *
     * @return string 
     */
    public function getDescrShort()
    {
        return $this->descrShort;
    }

    /**
     * Set pictureUrl
     *
     * @param string $pictureUrl
     * @return Album
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    /**
     * Get pictureUrl
     *
     * @return string 
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * Set profileUrl
     *
     * @param string $profileUrl
     * @return Album
     */
    public function setProfileUrl($profileUrl)
    {
        $this->profileUrl = $profileUrl;

        return $this;
    }

    /**
     * Get profileUrl
     *
     * @return string 
     */
    public function getProfileUrl()
    {
        return $this->profileUrl;
    }

    /**
     * Set sortPriority
     *
     * @param integer $sortPriority
     * @return Album
     */
    public function setSortPriority($sortPriority)
    {
        $this->sortPriority = $sortPriority;

        return $this;
    }

    /**
     * Get sortPriority
     *
     * @return integer 
     */
    public function getSortPriority()
    {
        return $this->sortPriority;
    }

    /**
     * Set albumSortPriority
     *
     * @param integer $albumSortPriority
     * @return Album
     */
    public function setAlbumSortPriority($albumSortPriority)
    {
        $this->albumSortPriority = $albumSortPriority;

        return $this;
    }

    /**
     * Get albumSortPriority
     *
     * @return integer 
     */
    public function getAlbumSortPriority()
    {
        return $this->albumSortPriority;
    }

    /**
     * Set created
     *
     * @param string $created
     * @return Album
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return string 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set taken
     *
     * @param string $taken
     * @return Album
     */
    public function setTaken($taken)
    {
        $this->taken = $taken;

        return $this;
    }

    /**
     * Get taken
     *
     * @return string 
     */
    public function getTaken()
    {
        return $this->taken;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Album
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set userid
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid
     * @return Album
     */
    public function setUserid(\Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param mixed $localPictureUrl
     */
    public function setLocalPictureUrl($localPictureUrl)
    {
        $this->localPictureUrl = $localPictureUrl;
    }

    /**
     * @return mixed
     */
    public function getLocalPictureUrl()
    {
        return $this->localPictureUrl;
    }
}
