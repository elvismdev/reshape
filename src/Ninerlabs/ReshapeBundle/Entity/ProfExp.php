<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProfExp
 */
class ProfExp
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $profession;

    /**
     * @var string
     */
    private $locationBusiness;

    function __construct($locationBusiness, $profession, $timeFrom, $timeTo, $userid)
    {
        $this->locationBusiness = $locationBusiness;
        $this->profession = $profession;
        $this->timeFrom = $timeFrom;
        $this->timeTo = $timeTo;
        $this->userid = $userid;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profession
     *
     * @param string $profession
     * @return ProfExp
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Set timeto
     *
     * @param string $timeto
     * @return ProfExp
     */
    public function setTimeTo($timeto)
    {
        $this->timeTo = $timeto;

        return $this;
    }

    /**
     * Get timeto
     *
     * @return string
     */
    public function getTimeTo()
    {
        return $this->timeTo;
    }

    /**
     * Set locationBusiness
     *
     * @param string $locationBusiness
     * @return ProfExp
     */
    public function setLocationBusiness($locationBusiness)
    {
        $this->locationBusiness = $locationBusiness;

        return $this;
    }

    /**
     * Get locationBusiness
     *
     * @return string
     */
    public function getLocationBusiness()
    {
        return $this->locationBusiness;
    }
    /**
     * @var string
     */
    private $timeFrom;

    /**
     * @var string
     */
    private $timeTo;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser
     */
    private $userid;


    /**
     * Set timeFrom
     *
     * @param string $timeFrom
     * @return ProfExp
     */
    public function setTimeFrom($timeFrom)
    {
        $this->timeFrom = $timeFrom;

        return $this;
    }

    /**
     * Get timeFrom
     *
     * @return string 
     */
    public function getTimeFrom()
    {
        return $this->timeFrom;
    }

    /**
     * Set userid
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid
     * @return ProfExp
     */
    public function setUserid(\Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser 
     */
    public function getUserid()
    {
        return $this->userid;
    }
}
