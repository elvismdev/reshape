<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProfessionalUserClassification
 */
class ProfessionalUserClassification
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser
     */
    private $userid;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\AttrGroup
     */
    private $attrGroup;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid
     * @return ProfessionalUserClassification
     */
    public function setUserid(\Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set attrGroup
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\AttrGroup $attrGroup
     * @return ProfessionalUserClassification
     */
    public function setAttrGroup(\Ninerlabs\ReshapeBundle\Entity\AttrGroup $attrGroup = null)
    {
        $this->attrGroup = $attrGroup;

        return $this;
    }

    /**
     * Get attrGroup
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\AttrGroup 
     */
    public function getAttrGroup()
    {
        return $this->attrGroup;
    }
}
