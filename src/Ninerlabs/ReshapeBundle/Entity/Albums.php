<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Albums
 */
class Albums
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $tagline;

    /**
     * @var string
     */
    private $descrShort;

    /**
     * @var string
     */
    private $pictureUrl;

    /**
     * @var string
     */
    private $localpath;

    /**
     * @var integer
     */
    private $videoId;

    /**
     * @var string
     */
    private $logoUrl;

    /**
     * @var string
     */
    private $profileUrl;

    /**
     * @var integer
     */
    private $sortpriority;

    /**
     * @var boolean
     */
    private $isactive;

    /**
     * @var integer
     */
    private $coverpic;

    /**
     * @var integer
     */
    private $albumsortpriority;

    /**
     * @var \DateTime
     */
    private $createdon;

    /**
     * @var \DateTime
     */
    private $takenon;

    /**
     * @var integer
     */
    private $albumownerid;

    /**
     * @var string
     */
    private $albumdescription;

    /**
     * @var string
     */
    private $jsonObjectDump;

    /**
     * @var \DateTime
     */
    private $runnedon;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Albums
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tagline
     *
     * @param string $tagline
     * @return Albums
     */
    public function setTagline($tagline)
    {
        $this->tagline = $tagline;

        return $this;
    }

    /**
     * Get tagline
     *
     * @return string
     */
    public function getTagline()
    {
        return $this->tagline;
    }

    /**
     * Set descrShort
     *
     * @param string $descrShort
     * @return Albums
     */
    public function setDescrShort($descrShort)
    {
        $this->descrShort = $descrShort;

        return $this;
    }

    /**
     * Get descrShort
     *
     * @return string
     */
    public function getDescrShort()
    {
        return $this->descrShort;
    }

    /**
     * Set pictureUrl
     *
     * @param string $pictureUrl
     * @return Albums
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    /**
     * Get pictureUrl
     *
     * @return string
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * Set localpath
     *
     * @param string $localpath
     * @return Albums
     */
    public function setLocalpath($localpath)
    {
        $this->localpath = $localpath;

        return $this;
    }

    /**
     * Get localpath
     *
     * @return string
     */
    public function getLocalpath()
    {
        return $this->localpath;
    }

    /**
     * Set videoId
     *
     * @param integer $videoId
     * @return Albums
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return integer
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set logoUrl
     *
     * @param string $logoUrl
     * @return Albums
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    /**
     * Get logoUrl
     *
     * @return string
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     * Set profileUrl
     *
     * @param string $profileUrl
     * @return Albums
     */
    public function setProfileUrl($profileUrl)
    {
        $this->profileUrl = $profileUrl;

        return $this;
    }

    /**
     * Get profileUrl
     *
     * @return string
     */
    public function getProfileUrl()
    {
        return $this->profileUrl;
    }

    /**
     * Set sortpriority
     *
     * @param integer $sortpriority
     * @return Albums
     */
    public function setSortpriority($sortpriority)
    {
        $this->sortpriority = $sortpriority;

        return $this;
    }

    /**
     * Get sortpriority
     *
     * @return integer
     */
    public function getSortpriority()
    {
        return $this->sortpriority;
    }

    /**
     * Set isactive
     *
     * @param boolean $isactive
     * @return Albums
     */
    public function setIsactive($isactive)
    {
        $this->isactive = $isactive;

        return $this;
    }

    /**
     * Get isactive
     *
     * @return boolean
     */
    public function getIsactive()
    {
        return $this->isactive;
    }

    /**
     * Set coverpic
     *
     * @param integer $coverpic
     * @return Albums
     */
    public function setCoverpic($coverpic)
    {
        $this->coverpic = $coverpic;

        return $this;
    }

    /**
     * Get coverpic
     *
     * @return integer
     */
    public function getCoverpic()
    {
        return $this->coverpic;
    }

    /**
     * Set albumsortpriority
     *
     * @param integer $albumsortpriority
     * @return Albums
     */
    public function setAlbumsortpriority($albumsortpriority)
    {
        $this->albumsortpriority = $albumsortpriority;

        return $this;
    }

    /**
     * Get albumsortpriority
     *
     * @return integer
     */
    public function getAlbumsortpriority()
    {
        return $this->albumsortpriority;
    }

    /**
     * Set createdon
     *
     * @param \DateTime $createdon
     * @return Albums
     */
    public function setCreatedon($createdon)
    {
        $this->createdon = $createdon;

        return $this;
    }

    /**
     * Get createdon
     *
     * @return \DateTime
     */
    public function getCreatedon()
    {
        return $this->createdon;
    }

    /**
     * Set takenon
     *
     * @param \DateTime $takenon
     * @return Albums
     */
    public function setTakenon($takenon)
    {
        $this->takenon = $takenon;

        return $this;
    }

    /**
     * Get takenon
     *
     * @return \DateTime
     */
    public function getTakenon()
    {
        return $this->takenon;
    }

    /**
     * Set albumownerid
     *
     * @param integer $albumownerid
     * @return Albums
     */
    public function setAlbumownerid($albumownerid)
    {
        $this->albumownerid = $albumownerid;

        return $this;
    }

    /**
     * Get albumownerid
     *
     * @return integer
     */
    public function getAlbumownerid()
    {
        return $this->albumownerid;
    }

    /**
     * Set albumdescription
     *
     * @param string $albumdescription
     * @return Albums
     */
    public function setAlbumdescription($albumdescription)
    {
        $this->albumdescription = $albumdescription;

        return $this;
    }

    /**
     * Get albumdescription
     *
     * @return string
     */
    public function getAlbumdescription()
    {
        return $this->albumdescription;
    }

    /**
     * Set jsonObjectDump
     *
     * @param string $jsonObjectDump
     * @return Albums
     */
    public function setJsonObjectDump($jsonObjectDump)
    {
        $this->jsonObjectDump = $jsonObjectDump;

        return $this;
    }

    /**
     * Get jsonObjectDump
     *
     * @return string
     */
    public function getJsonObjectDump()
    {
        return $this->jsonObjectDump;
    }

    /**
     * Set runnedon
     *
     * @param \DateTime $runnedon
     * @return Albums
     */
    public function setRunnedon($runnedon)
    {
        $this->runnedon = $runnedon;

        return $this;
    }

    /**
     * Get runnedon
     *
     * @return \DateTime
     */
    public function getRunnedon()
    {
        return $this->runnedon;
    }
    /**
     * @var integer
     */
    private $idalbum;


    /**
     * Set idalbum
     *
     * @param integer $idalbum
     * @return Albums
     */
    public function setIdalbum($idalbum)
    {
        $this->idalbum = $idalbum;

        return $this;
    }

    /**
     * Get idalbum
     *
     * @return integer
     */
    public function getIdalbum()
    {
        return $this->idalbum;
    }
}
