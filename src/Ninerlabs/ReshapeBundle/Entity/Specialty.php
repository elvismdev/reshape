<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Specialty
 */
class Specialty
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $specialty;

    function __construct($specialty, $userid)
    {
        $this->specialty = $specialty;
        $this->userid = $userid;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set specialty
     *
     * @param string $specialty
     * @return Specialty
     */
    public function setSpecialty($specialty)
    {
        $this->specialty = $specialty;

        return $this;
    }

    /**
     * Get specialty
     *
     * @return string 
     */
    public function getSpecialty()
    {
        return $this->specialty;
    }
    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser
     */
    private $userid;


    /**
     * Set userid
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid
     * @return Specialty
     */
    public function setUserid(\Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser 
     */
    public function getUserid()
    {
        return $this->userid;
    }
}
