<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 */
class Service
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * @var integer
     */
    private $minutes;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $sortorder;

    /**
     * @var string
     */
    private $pictureUrl;

    /**
     * @var string
     */
    private $logoUrl;

    /**
     * @var string
     */
    private $videoId;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ServiceCode
     */
    private $serviceCode;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser
     */
    private $userid;

    private $localPictureUrl;

    private $localLogoUrl;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Service
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set minutes
     *
     * @param integer $minutes
     * @return Service
     */
    public function setMinutes($minutes)
    {
        $this->minutes = $minutes;

        return $this;
    }

    /**
     * Get minutes
     *
     * @return integer 
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Service
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sortorder
     *
     * @param integer $sortorder
     * @return Service
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;

        return $this;
    }

    /**
     * Get sortorder
     *
     * @return integer 
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }

    /**
     * Set pictureUrl
     *
     * @param string $pictureUrl
     * @return Service
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    /**
     * Get pictureUrl
     *
     * @return string 
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * Set logoUrl
     *
     * @param string $logoUrl
     * @return Service
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    /**
     * Get logoUrl
     *
     * @return string 
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     * Set videoId
     *
     * @param string $videoId
     * @return Service
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return string 
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set serviceCode
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ServiceCode $serviceCode
     * @return Service
     */
    public function setServiceCode(\Ninerlabs\ReshapeBundle\Entity\ServiceCode $serviceCode = null)
    {
        $this->serviceCode = $serviceCode;

        return $this;
    }

    /**
     * Get serviceCode
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ServiceCode 
     */
    public function getServiceCode()
    {
        return $this->serviceCode;
    }

    /**
     * Set userid
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid
     * @return Service
     */
    public function setUserid(\Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param mixed $localLogoUrl
     */
    public function setLocalLogoUrl($localLogoUrl)
    {
        $this->localLogoUrl = $localLogoUrl;
    }

    /**
     * @return mixed
     */
    public function getLocalLogoUrl()
    {
        return $this->localLogoUrl;
    }

    /**
     * @param mixed $localPictureUrl
     */
    public function setLocalPictureUrl($localPictureUrl)
    {
        $this->localPictureUrl = $localPictureUrl;
    }

    /**
     * @return mixed
     */
    public function getLocalPictureUrl()
    {
        return $this->localPictureUrl;
    }
}
