<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientSpec
 */
class ClientSpec
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $clientSpec;

    function __construct($clientSpec, $userid)
    {
        $this->clientSpec = $clientSpec;
        $this->userid = $userid;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clientSpec
     *
     * @param string $clientSpec
     * @return ClientSpec
     */
    public function setClientSpec($clientSpec)
    {
        $this->clientSpec = $clientSpec;

        return $this;
    }

    /**
     * Get clientSpec
     *
     * @return string 
     */
    public function getClientSpec()
    {
        return $this->clientSpec;
    }
    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser
     */
    private $userid;


    /**
     * Set userid
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid
     * @return ClientSpec
     */
    public function setUserid(\Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser 
     */
    public function getUserid()
    {
        return $this->userid;
    }
}
