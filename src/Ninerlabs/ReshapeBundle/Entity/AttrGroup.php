<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AttrGroup
 */
class AttrGroup
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $descrShort;

    /**
     * @var string
     */
    private $pictureUrl;

    /**
     * @var string
     */
    private $videoId;

    /**
     * @var string
     */
    private $logoUrl;

    /**
     * @var string
     */
    private $profileUrl;

    /**
     * @var integer
     */
    private $sortPriority;

    /**
     * @var integer
     */
    private $attrGroupParentId;

    private $localPictureUrl;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AttrGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set descrShort
     *
     * @param string $descrShort
     * @return AttrGroup
     */
    public function setDescrShort($descrShort)
    {
        $this->descrShort = $descrShort;

        return $this;
    }

    /**
     * Get descrShort
     *
     * @return string 
     */
    public function getDescrShort()
    {
        return $this->descrShort;
    }

    /**
     * Set pictureUrl
     *
     * @param string $pictureUrl
     * @return AttrGroup
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    /**
     * Get pictureUrl
     *
     * @return string 
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * Set videoId
     *
     * @param string $videoId
     * @return AttrGroup
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return string 
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set logoUrl
     *
     * @param string $logoUrl
     * @return AttrGroup
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    /**
     * Get logoUrl
     *
     * @return string 
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     * Set profileUrl
     *
     * @param string $profileUrl
     * @return AttrGroup
     */
    public function setProfileUrl($profileUrl)
    {
        $this->profileUrl = $profileUrl;

        return $this;
    }

    /**
     * Get profileUrl
     *
     * @return string 
     */
    public function getProfileUrl()
    {
        return $this->profileUrl;
    }

    /**
     * Set sortPriority
     *
     * @param integer $sortPriority
     * @return AttrGroup
     */
    public function setSortPriority($sortPriority)
    {
        $this->sortPriority = $sortPriority;

        return $this;
    }

    /**
     * Get sortPriority
     *
     * @return integer 
     */
    public function getSortPriority()
    {
        return $this->sortPriority;
    }

    /**
     * Set attrGroupParentId
     *
     * @param integer $attrGroupParentId
     * @return AttrGroup
     */
    public function setAttrGroupParentId($attrGroupParentId)
    {
        $this->attrGroupParentId = $attrGroupParentId;

        return $this;
    }

    /**
     * Get attrGroupParentId
     *
     * @return integer 
     */
    public function getAttrGroupParentId()
    {
        return $this->attrGroupParentId;
    }

    /**
     * @param mixed $localPictureUrl
     */
    public function setLocalPictureUrl($localPictureUrl)
    {
        $this->localPictureUrl = $localPictureUrl;
    }

    /**
     * @return mixed
     */
    public function getLocalPictureUrl()
    {
        return $this->localPictureUrl;
    }
}
