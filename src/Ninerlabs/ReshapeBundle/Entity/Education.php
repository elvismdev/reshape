<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Education
 */
class Education
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $education;

    /**
     * @var string
     */
    private $degree;

    /**
     * @var string
     */
    private $schoolUni;

    function __construct($degree, $education, $schoolUni, $userid)
    {
        $this->degree = $degree;
        $this->education = $education;
        $this->schoolUni = $schoolUni;
        $this->userid = $userid;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set education
     *
     * @param string $education
     * @return Education
     */
    public function setEducation($education)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * Get education
     *
     * @return string 
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * Set degree
     *
     * @param string $degree
     * @return Education
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * Get degree
     *
     * @return string 
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Set schoolUni
     *
     * @param string $schoolUni
     * @return Education
     */
    public function setSchoolUni($schoolUni)
    {
        $this->schoolUni = $schoolUni;

        return $this;
    }

    /**
     * Get schoolUni
     *
     * @return string 
     */
    public function getSchoolUni()
    {
        return $this->schoolUni;
    }
    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser
     */
    private $userid;


    /**
     * Set userid
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid
     * @return Education
     */
    public function setUserid(\Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser 
     */
    public function getUserid()
    {
        return $this->userid;
    }
}
