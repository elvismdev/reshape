<?php

namespace Ninerlabs\ReshapeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Social
 */
class Social
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $account;

    /**
     * @var string
     */
    private $key;

    /**
     * @var \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser
     */
    private $userid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Social
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set account
     *
     * @param string $account
     * @return Social
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return string 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set key
     *
     * @param string $key
     * @return Social
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string 
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set userid
     *
     * @param \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid
     * @return Social
     */
    public function setUserid(\Ninerlabs\ReshapeBundle\Entity\ProfessionalUser $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \Ninerlabs\ReshapeBundle\Entity\ProfessionalUser 
     */
    public function getUserid()
    {
        return $this->userid;
    }
}
