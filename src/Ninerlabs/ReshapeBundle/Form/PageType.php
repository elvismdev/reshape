<?php

namespace Ninerlabs\ReshapeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
          $builder
          ->add('title')
          ->add('body')
          ->add('slug')
          ;
        }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
      $resolver->setDefaults(array(
        'data_class' => 'Ninerlabs\ReshapeBundle\Entity\Page'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
      return 'ninerlabs_reshapebundle_page';
    }
  }
