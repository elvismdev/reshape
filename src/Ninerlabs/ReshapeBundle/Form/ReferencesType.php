<?php

namespace Ninerlabs\ReshapeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReferencesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('refName')
            ->add('phone')
            ->add('relationship')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ninerlabs\ReshapeBundle\Entity\References'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ninerlabs_reshapebundle_references';
    }
}
