<?php

namespace Ninerlabs\ReshapeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactUsType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', 'text')
            ->add('lastname', 'text')
            ->add('email', 'email')
            ->add('number', 'text')
            // ->add('organization', 'text')
            // ->add('youare', 'choice', array(
            //     'expanded' => true,
            //     'choices' => array(
            //         'Education Institution' => 'Education Institution',
            //         'Corporate or Government Institution' => 'Corporate or Government Institution'
            //     )
            // ))
            ->add('message', 'textarea')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ninerlabs\ReshapeBundle\Entity\ContactUs'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ninerlabs_reshapebundle_contactus';
    }
}
