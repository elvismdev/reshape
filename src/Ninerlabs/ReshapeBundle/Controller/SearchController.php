<?php

namespace Ninerlabs\ReshapeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SearchController extends Controller
{
  public function indexAction()
  {
    // Getting all profiles
    $searchAll = "http://dash.reshape.net/api/ajax/search/profileSearch?token=1234&limit=0&metaData=false";
    $response_searchAll = $this->getUrlContent($searchAll);

    $searchAtt = "http://dash.reshape.net/api/ajax/attributes/groups?token=1234";
    $response_searchAtt = $this->getUrlContent($searchAtt);

    $profiles = array();
    if (!isset($response_searchAll->error) || $response_searchAll->error == 'true') {
      $error[] = 'Requested URL Error Media.';
    } else {
      $profiles = $response_searchAll->data;
    }

    $attributes = array();
    if (!isset($response_searchAtt->error) || $response_searchAtt->error == 'true') {
      $error[] = 'Requested URL Error Media.';
    } else {
      $attributes = $response_searchAtt->attrGroups;
    }

    return $this->render('ReshapeBundle:Default:search.html.twig', array(
      'profiles' => $profiles,
      'attributes' => $attributes
      ));
  }

  /**
     * Updates search
     *
     */
  // public function updateAjaxAction(Request $request)
  // {
  //   $attrs = $request->request->get(0);

  //   $response = new Response($attrs);

  //   return $response;
  // }

  private function getUrlContent($url)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (curl_exec($ch) !== false)
          return json_decode(curl_exec($ch));

        throw $this->createNotFoundException('Response Error in: ' . $url);
      }
    }
