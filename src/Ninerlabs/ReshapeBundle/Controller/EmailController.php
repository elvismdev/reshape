<?php

namespace Ninerlabs\ReshapeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;


class EmailController
{

    private $em;
    private $rootDir;

    function __construct($em, $rootDir)
    {
        $this->em = $em;
        $this->rootDir = $rootDir;
    }

    private function getAmazonSES()
    {
        require_once($this->rootDir . '/../vendor/amazonsesmailer/AmazonSESMailer.php');

        $amazonaws = $this->em->getRepository('ReshapeBundle:Amazonaws')->findOneById(1);

        return new \AmazonSESMailer($amazonaws->getAwsId(), $amazonaws->getAwsSecretKey());
    }

    public function EmailRetrievePassword($user)
    {
        $mailer = $this->em->getRepository('ReshapeBundle:Mailer')->findOneBySource('PasswordForgot');

        $amazonaws = $this->em->getRepository('ReshapeBundle:Amazonaws')->findOneById(1);

        $amazonSESMailer = $this->getAmazonSES();
        $amazonSESMailer->ClearReplyTos();
        $amazonSESMailer->AddReplyTo($amazonaws->getEmail());

        $amazonSESMailer->SetFrom($amazonaws->getEmail(), $amazonaws->getEmailName());

        // ADD THE TO EMAILS
        $amazonSESMailer->AddAddress($user->getEmail());

        // SUBJECT
        $amazonSESMailer->Subject = $mailer->getSubject();

        // MESSAGE
        $body = $mailer->getBody();

        $body = str_replace('[fullname]', $user->getFullname(), $body);
        $body = str_replace('[code]', $user->getCode(), $body);

        $amazonSESMailer->MsgHtml($body);

        $amazonSESMailer->Send();
    }

    public function EmailAttachment($to, $subject, $body)
    {
        $amazonaws = $this->em->getRepository('ReshapeBundle:Amazonaws')->findOneById(1);

        $amazonSESMailer = $this->getAmazonSES();
        $amazonSESMailer->ClearReplyTos();
        $amazonSESMailer->AddReplyTo($amazonaws->getEmail());

        $amazonSESMailer->SetFrom($amazonaws->getEmail(), $amazonaws->getEmailName());

        // ADD THE TO EMAILS
        $amazonSESMailer->AddAddress($to);

        // SUBJECT
        $amazonSESMailer->Subject = $subject;

        // MESSAGE
        $body = $body;
        $amazonSESMailer->MsgHtml($body);

        $amazonSESMailer->Send();
    }

    public function Email($email, $subject, $body)
    {
        $amazonaws = $this->em->getRepository('ReshapeBundle:Amazonaws')->findOneById(1);

        $amazonSESMailer = $this->getAmazonSES();
        $amazonSESMailer->ClearReplyTos();
        $amazonSESMailer->AddReplyTo($amazonaws->getEmail());

        $amazonSESMailer->SetFrom($amazonaws->getEmail(), $amazonaws->getEmailName());

        // ADD THE TO EMAILS
        $amazonSESMailer->AddAddress($email);

        // SUBJECT
        $amazonSESMailer->Subject = $subject;

        $amazonSESMailer->MsgHtml($body);

        $amazonSESMailer->Send();
    }

    public function ContactUs($client, $subject, $body)
    {
        $amazonaws = $this->em->getRepository('ReshapeBundle:Amazonaws')->findOneById(1);

        $amazonSESMailer = $this->getAmazonSES();
        $amazonSESMailer->ClearReplyTos();
        $amazonSESMailer->AddReplyTo($client['email']);

        $amazonSESMailer->SetFrom($amazonaws->getEmail(), $amazonaws->getEmailName());

        // ADD THE TO EMAILS
        $amazonSESMailer->AddAddress($amazonaws->getEmail());

        // SUBJECT
        $amazonSESMailer->Subject = $subject;

        $amazonSESMailer->MsgHtml($body);

        $amazonSESMailer->Send();
    }

    public function EmailTest($email)
    {
        $amazonaws = $this->em->getRepository('ReshapeBundle:Amazonaws')->findOneById(1);

        $amazonSESMailer = $this->getAmazonSES();
        $amazonSESMailer->ClearReplyTos();
        $amazonSESMailer->AddReplyTo($amazonaws->getEmail());

        $amazonSESMailer->SetFrom($amazonaws->getEmail(), $amazonaws->getEmailName());

        // ADD THE TO EMAILS
        $amazonSESMailer->AddAddress($email);

        // SUBJECT
        $amazonSESMailer->Subject = 'Test email';

        // MESSAGE
        $body = 'Testing Amazonaws system';

        $amazonSESMailer->MsgHtml($body);

        $amazonSESMailer->Send();
    }
}