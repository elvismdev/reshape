<?php

namespace Ninerlabs\ReshapeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Ninerlabs\ReshapeBundle\Entity\FaqList;
use Ninerlabs\ReshapeBundle\Form\FaqListType;

/**
 * FaqList controller.
 *
 */
class FaqListController extends Controller
{

    /**
     * Lists all FaqList entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ReshapeBundle:FaqCategories')->findAll();

        foreach ($entities as $category) {
            $category->faqList = $em->getRepository('ReshapeBundle:FaqList')
                ->findBy(
                    array('faqCat' => $category),
                    array('order' => 'ASC', 'question' => 'ASC')
                );
        }

        return $this->render('ReshapeBundle:FaqList:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new FaqList entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new FaqList();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('faq'));
        }

        return $this->render('ReshapeBundle:FaqList:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a FaqList entity.
     *
     * @param FaqList $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(FaqList $entity)
    {
        $form = $this->createForm(new FaqListType(), $entity, array(
            'action' => $this->generateUrl('faq_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new FaqList entity.
     *
     */
    public function newAction()
    {
        $entity = new FaqList();
        $form   = $this->createCreateForm($entity);

        return $this->render('ReshapeBundle:FaqList:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a FaqList entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ReshapeBundle:FaqList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FaqList entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ReshapeBundle:FaqList:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing FaqList entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ReshapeBundle:FaqList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FaqList entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ReshapeBundle:FaqList:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a FaqList entity.
    *
    * @param FaqList $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(FaqList $entity)
    {
        $form = $this->createForm(new FaqListType(), $entity, array(
            'action' => $this->generateUrl('faq_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing FaqList entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ReshapeBundle:FaqList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FaqList entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('faq_edit', array('id' => $id)));
        }

        return $this->render('ReshapeBundle:FaqList:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a FaqList entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ReshapeBundle:FaqList')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find FaqList entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('faq'));
    }

    /**
     * Creates a form to delete a FaqList entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('faq_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function orderAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $faqs = $request->request->get('faq-list');

        for ($i=0; $i<count($faqs); $i++) {
            $entity = $em->getRepository('ReshapeBundle:FaqList')->find($faqs[$i]);

            if (!$entity) {
                continue;
            }

            $entity->setOrder($i + 1);
            $em->persist($entity);
        }

        $em->flush();

        $response = new Response();
        $output = array('success' => true);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($output));

        return $response;
    }
}
