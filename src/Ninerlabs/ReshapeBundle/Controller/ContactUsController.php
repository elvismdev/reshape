<?php

namespace Ninerlabs\ReshapeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Ninerlabs\ReshapeBundle\Entity\ContactUs;
use Ninerlabs\ReshapeBundle\Form\ContactUsType;

class ContactUsController extends Controller
{
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new ContactUs();
        $form   = $this->createCreateForm($entity);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $emailController = new EmailController($em, $this->get('kernel')->getRootDir());
                $subject = 'Contact';

                $body = $this->renderView('ReshapeBundle:ContactUs:client.html.twig', array(
                    'entity' => $entity,
                ));

                $client['email'] = $entity->getEmail();
                $client['name'] = $entity->getFirstname() . ' ' . $entity->getLastname();

                $emailController->ContactUs($client, $subject, $body);

                return $this->redirect($this->generateUrl('reshape_homepage'));
            }
        }

        return $this->render('ReshapeBundle:ContactUs:contact.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    private function createCreateForm(ContactUs $entity)
    {
        $form = $this->createForm(new ContactUsType(), $entity, array(
            'action' => $this->generateUrl('reshape_contactus'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Submit'));

        return $form;
    }
}
