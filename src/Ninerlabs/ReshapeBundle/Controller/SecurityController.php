<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Ninerlabs\ReshapeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

use Ninerlabs\ReshapeBundle\Controller\EmailController;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    public function loginAction()
    {
        $peticion = $this->getRequest();

        $sesion = $peticion->getSession();

        if ($peticion->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $peticion->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $sesion->get(SecurityContext::AUTHENTICATION_ERROR);
        }
        return $this->render('ReshapeBundle:Security:login.html.twig', array(
            'last_username' => $sesion->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        ));
    }

    public function emailActivationAction($hash, Request $request)
    {
        // 1 day measured in seconds = 60 seconds * 60 minutes * 24 hours
        //$expire = 86400;
        $em = $this->getDoctrine()->getManager();

        /*
         * hash exampe: 23_5bfe95f4fbbe191b2b10faa622b0efe0_1410530673
         * 23: User ID
         * 5bfe95f4fbbe191b2b10faa622b0efe0: User Salt
         * 1410530673: Created TimeStamp
         */
        $hash = explode('_', $hash);
        if (!isset($hash[0]) || !isset($hash[1]) || !isset($hash[2])) {
            throw $this->createNotFoundException('Wrong HASH.');
        }

        $entity = $em->getRepository('ReshapeBundle:ProfessionalUser')->find($hash[0]);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProfessionalUser entity.');
        }

        if ($entity->getSalt() != $hash[1]) {
            throw $this->createNotFoundException('Wrong User-HASH relation.');
        }

        if ($entity->getCreated() != $hash[2]) {
            throw $this->createNotFoundException('Token has expired.');
        }

        // Check to see if link has expired
        /*if ($_SERVER["REQUEST_TIME"] - $hash[2] > $expire) {
            throw $this->createNotFoundException('Token has expired.');
        }*/

        $entity->setStatus(true);

        $entity->setCreated(null);

        $em->persist($entity);
        $em->flush();

        // Log User
        // Here, "secured_area" is the name of the firewall in your security.yml
        $token = new UsernamePasswordToken($entity, $entity->getPassword(), "secured_area", $entity->getRoles());
        $this->get("security.context")->setToken($token);

        // Fire the login event
        // Logging the user in above the way we do it doesn't do this automatically
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        return $this->redirect($this->generateUrl('professionaluser_edit', array('id' => $hash[0])));
    }

    public function validateAction()
    {
        return $this->render('ReshapeBundle:Security:validate.html.twig', array());
    }
}

?>