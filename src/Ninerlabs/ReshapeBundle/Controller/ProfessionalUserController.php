<?php

namespace Ninerlabs\ReshapeBundle\Controller;

use Ninerlabs\ReshapeBundle\Entity\ProfExp;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Ninerlabs\ReshapeBundle\Entity\ProfessionalUser;
use Ninerlabs\ReshapeBundle\Form\ProfessionalUserType;

use Ninerlabs\ReshapeBundle\Entity\References;
use Ninerlabs\ReshapeBundle\Entity\Certifications;
use Ninerlabs\ReshapeBundle\Entity\Education;
use Ninerlabs\ReshapeBundle\Entity\Specialty;
use Ninerlabs\ReshapeBundle\Entity\ClientSpec;

/**
 * ProfessionalUser controller.
 *
 */
class ProfessionalUserController extends Controller
{

    /**
     * Lists all ProfessionalUser entities.
     *
     */
    public function indexAction()
    {
      $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('ReshapeBundle:ProfessionalUser')->findAll();

      return $this->render('ReshapeBundle:ProfessionalUser:index.html.twig', array(
        'entities' => $entities,
        ));
    }

    /**
     * Creates a new ProfessionalUser entity.
     *
     */
    public function createAction(Request $request)
    {
      $entity = new ProfessionalUser();
      $form = $this->createCreateForm($entity);
      $form->handleRequest($request);

      if ($form->isValid()) {
            // Default Settings
        $this->setSecurePassword($entity);
            $entity->setStatus(false); // Disabled default
            $entity->setRol('ROLE_USER');
            if ($entity->getProfessional())
              $entity->setRol('ROLE_PROFESSIONAL_USER');

            $entity->setCreated($_SERVER["REQUEST_TIME"]);
            $entity->setComplete(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // Activation email
            $domain = 'http://' . $this->container->get('router')->getContext()->getHost();
            $url = $domain . $this->generateUrl('activate', array('hash' => $entity->getId() . '_' . $entity->getSalt() . '_' . $entity->getCreated()));

            $emailController = new EmailController($em, $this->get('kernel')->getRootDir());
            $subject = 'Activation';
            $body = "Activation URL: $url";
            $emailController->Email($entity->getEmail(), $subject, $body);

            return $this->redirect($this->generateUrl('validate'));
          }

          return $this->newAction();
        }

    /**
     * Creates a form to create a ProfessionalUser entity.
     *
     * @param ProfessionalUser $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ProfessionalUser $entity)
    {
      $form = $this->createForm(new ProfessionalUserType(), $entity, array(
            // 'action' => $this->generateUrl('professionaluser_create'),
        'method' => 'POST',
        ));

      $form->add('submit', 'submit', array('label' => 'Create'));

      return $form;
    }

    /**
     * Displays a form to create a new ProfessionalUser entity.
     *
     */
    public function newAction()
    {
      $entity = new ProfessionalUser();
      $form = $this->createCreateForm($entity);

      $em = $this->getDoctrine()->getManager();

      $tc = $em->getRepository('ReshapeBundle:Page')->findOneBySlug('terms-and-conditions');

      return $this->render('ReshapeBundle:ProfessionalUser:new.html.twig', array(
        'tc' => $tc,
        'entity' => $entity,
        'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ProfessionalUser entity.
     *
     */
    public function showAction($id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('ReshapeBundle:ProfessionalUser')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find ProfessionalUser entity.');
      }

      $deleteForm = $this->createDeleteForm($id);

      return $this->render('ReshapeBundle:ProfessionalUser:show.html.twig', array(
        'entity' => $entity,
        'delete_form' => $deleteForm->createView(),
        ));
    }

    public function showUsernameAction($apiId)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('ReshapeBundle:ProfessionalUser')->findOneByApiId($apiId);

      if (!$entity) {
        $entity = $em->getRepository('ReshapeBundle:Page')->findOneBySlug($apiId);

        if (!$entity) {
          throw $this->createNotFoundException('Unable to find Page entity.');
        }

        return $this->render('ReshapeBundle:Page:view.html.twig', array(
          'entity'      => $entity,
          'page'        => true
          ));
      }

      $deleteForm = $this->createDeleteForm($entity->getId());

      return $this->render('ReshapeBundle:ProfessionalUser:show.html.twig', array(
        'entity' => $entity,
        'delete_form' => $deleteForm->createView(),
        'social' => $this->userSocial($apiId)
        ));
    }

    /**
     * Displays a form to edit an existing ProfessionalUser entity.
     *
     */
    public function editAction($id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('ReshapeBundle:ProfessionalUser')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find ProfessionalUser entity.');
      }

      $editForm = $this->createEditForm($entity);
      $deleteForm = $this->createDeleteForm($id);

      $count = 0;
      $profexp = $em->getRepository('ReshapeBundle:ProfExp')->findByUserid($entity);
      if (($c = count($profexp)) > $count)
        $count = $c;
      $ref = $em->getRepository('ReshapeBundle:References')->findByUserid($entity);
      if (($c = count($ref)) > $count)
        $count = $c;
      $cert = $em->getRepository('ReshapeBundle:Certifications')->findByUserid($entity);
      if (($c = count($cert)) > $count)
        $count = $c;
      $edu = $em->getRepository('ReshapeBundle:Education')->findByUserid($entity);
      if (($c = count($edu)) > $count)
        $count = $c;
      $spe = $em->getRepository('ReshapeBundle:Specialty')->findByUserid($entity);
      if (($c = count($spe)) > $count)
        $count = $c;
      $clspec = $em->getRepository('ReshapeBundle:ClientSpec')->findByUserid($entity);
      if (($c = count($clspec)) > $count)
        $count = $c;

      return $this->render('ReshapeBundle:ProfessionalUser:edit.html.twig', array(
        'entity' => $entity,
        'edit_form' => $editForm->createView(),
        'delete_form' => $deleteForm->createView(),
        'profexp' => $profexp,
        'ref' => $ref,
        'cert' => $cert,
        'edu' => $edu,
        'spe' => $spe,
        'clspec' => $clspec,
        'count' => $count
        ));
    }

    /**
     * Creates a form to edit a ProfessionalUser entity.
     *
     * @param ProfessionalUser $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ProfessionalUser $entity)
    {
      $form = $this->createForm(new ProfessionalUserType(true), $entity, array(
        'action' => $this->generateUrl('professionaluser_update', array('id' => $entity->getId())),
        'method' => 'PUT',
        ));

      $form->add('submit', 'submit', array('label' => 'Update'));

      return $form;
    }

    /**
     * Edits an existing ProfessionalUser entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('ReshapeBundle:ProfessionalUser')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find ProfessionalUser entity.');
      }

      $current_pass = $entity->getPassword();

      $deleteForm = $this->createDeleteForm($id);
      $editForm = $this->createEditForm($entity);
      $editForm->handleRequest($request);

        //if ($editForm->isValid()) {
        // Changing password if needed
      if ($entity->getPassword() != "") {
        $this->setSecurePassword($entity);
      } else {
        $entity->setPassword($current_pass);
      }

      $em->flush();

      $count = $request->request->get('count', 0);

      $profProfession = $request->request->get('profProfession');
      $profTime = $request->request->get('profTime');
      $profTimeto = $request->request->get('profTimeto');
      $profLocationBusiness = $request->request->get('profLocationBusiness');

      $refName = $request->request->get('refName');
      $refPhone = $request->request->get('refPhone');
      $refRelationship = $request->request->get('refRelationship');

      $certCert = $request->request->get('certCert');

      $eduEeducation = $request->request->get('eduEeducation');
      $eduDegree = $request->request->get('eduDegree');
      $eduSschoolUni = $request->request->get('eduSschoolUni');

      $speSpecialty = $request->request->get('speSpecialty');

      $clientSpecSpec = $request->request->get('clientSpecSpec');

      for ($i = 0; $i <= $count; $i++) {
        if (isset($profProfession[$i]) && isset($profTime[$i]) && isset($profTimeto[$i]) && isset($profLocationBusiness[$i]) && $profProfession[$i]) {
          $queryBuilder = $em
          ->createQueryBuilder()
          ->delete('ReshapeBundle:ProfExp', 'p')
          ->where('p.userid = :user')
          ->setParameter(':user', $entity);
          $queryBuilder->getQuery()->execute();

          $ent = new ProfExp($profLocationBusiness[$i], $profProfession[$i], $profTime[$i], $profTimeto[$i], $entity);
          $em->persist($ent);
        }

        if (isset($refName[$i]) && isset($refPhone[$i]) && $refRelationship[$i] && $refName[$i]) {
          $queryBuilder = $em
          ->createQueryBuilder()
          ->delete('ReshapeBundle:References', 'r')
          ->where('r.userid = :user')
          ->setParameter(':user', $entity);
          $queryBuilder->getQuery()->execute();

          $ent = new References($refPhone[$i], $refName[$i], $refRelationship[$i], $entity);
          $em->persist($ent);
        }

        if (isset($certCert[$i]) && $certCert[$i]) {
          $queryBuilder = $em
          ->createQueryBuilder()
          ->delete('ReshapeBundle:Certifications', 'c')
          ->where('c.userid = :user')
          ->setParameter(':user', $entity);
          $queryBuilder->getQuery()->execute();

          $ent = new Certifications($certCert[$i], $entity);
          $em->persist($ent);
        }

        if (isset($eduEeducation[$i]) && isset($eduDegree[$i]) && $eduSschoolUni[$i] && $eduEeducation[$i]) {
          $queryBuilder = $em
          ->createQueryBuilder()
          ->delete('ReshapeBundle:Education', 'e')
          ->where('e.userid = :user')
          ->setParameter(':user', $entity);
          $queryBuilder->getQuery()->execute();

          $ent = new Education($eduDegree[$i], $eduEeducation[$i], $eduSschoolUni[$i], $entity);
          $em->persist($ent);
        }

        if (isset($speSpecialty[$i]) && $speSpecialty[$i]) {
          $queryBuilder = $em
          ->createQueryBuilder()
          ->delete('ReshapeBundle:Specialty', 's')
          ->where('s.userid = :user')
          ->setParameter(':user', $entity);
          $queryBuilder->getQuery()->execute();

          $ent = new Specialty($speSpecialty[$i], $entity);
          $em->persist($ent);
        }

        if (isset($clientSpecSpec[$i]) && $clientSpecSpec[$i]) {
          $queryBuilder = $em
          ->createQueryBuilder()
          ->delete('ReshapeBundle:ClientSpec', 'c')
          ->where('c.userid = :user')
          ->setParameter(':user', $entity);
          $queryBuilder->getQuery()->execute();

          $ent = new ClientSpec($clientSpecSpec[$i], $entity);
          $em->persist($ent);
        }
      }

      $currentstep = $request->request->get('currentstep', 0);
      if ($currentstep)
        $entity->setComplete(true);

      $em->flush();

      if ($request->isXmlHttpRequest()) {
        $response = new Response();
        $output = array('success' => true, 'id' => $entity->getId());
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($output));

        return $response;
      }

      return $this->redirect($this->generateUrl('professionaluser_edit', array('id' => $id)));
        //}

        /*return $this->render('ReshapeBundle:ProfessionalUser:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            ));*/
}

    /**
     * Deletes a ProfessionalUser entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
      $form = $this->createDeleteForm($id);
      $form->handleRequest($request);

      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ReshapeBundle:ProfessionalUser')->find($id);

        if (!$entity) {
          throw $this->createNotFoundException('Unable to find ProfessionalUser entity.');
        }

        $em->remove($entity);
        $em->flush();
      }

      return $this->redirect($this->generateUrl('professionaluser'));
    }

    /**
     * Creates a form to delete a ProfessionalUser entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
      return $this->createFormBuilder()
      ->setAction($this->generateUrl('professionaluser_delete', array('id' => $id)))
      ->setMethod('DELETE')
      ->add('submit', 'submit', array('label' => 'Delete'))
      ->getForm();
    }

    private function setSecurePassword(&$entity)
    {
      $entity->setSalt(md5(time()));
      $encoder = new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('sha512', true, 10);
      $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
      $entity->setPassword($password);
    }

    public function socialsAction($apiId)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('ReshapeBundle:ProfessionalUser')->findOneByApiId($apiId);

      if (!$entity) {
        return new Response('');
      }

      return new Response(
        $entity->getFacebook() . '|' . $entity->getTwitter() . '|' . $entity->getYoutube() . '|' . $entity->getInstagram()
        );
    }

    public function socialHubAction($apiId, $token='1234')
    {

        // Saving user media
      $social_url = "http://dash.reshape.net/api/ajax/profile/social?token=$token&id=$apiId";
      $response_social = $this->getUrlContent($social_url);

      $socials = array();
      if (!isset($response_social->error) || $response_social->error == 'true') {
        $error[] = 'Requested URL Error Socials.';
      } else {
        foreach ($response_social->socialMedia->accounts as $s) {
          if ($s->isActive == 'true') {
            $socials[$s->label] = $s;
            if ($s->label == 'Facebook') {
              $fb = "https://graph.facebook.com/$s->label";
              $response_fb = $this->getUrlContent($fb);
              $s->id = $response_fb->id;
            }
          }
        }
      }

      if ($socials['Instagram']->account) {
        if (!$this->checkInstagramPrivateProfile($socials['Instagram']->account))
          $socials['Instagram']->snapWidgetHash = base64_encode("{$socials['Instagram']->account}|in|130|3|3|ffffff|yes|5|none|onStart|no|yes");
      }


      if ($this->getRequest()->get('_route') == 'reshape_user_socialhub_old') {
        $template = 'ReshapeBundle:ProfessionalUser:socialHubOld.html.twig';
      } else {
        $template = 'ReshapeBundle:ProfessionalUser:socialHub.html.twig';
      }



      return $this->render($template, array(
        'social' => $socials,
        'userInfo' => $this->userInfo($apiId)
        ));
    }

    private function checkInstagramPrivateProfile($username)
    {
      //Defining Instagram Client ID to use Instagram API's resources
      $instagramClientId = '9dab4f33019649e3a1d337ec2c3b1d80';

      $profileId = array_filter(
        $this->getUrlContent('https://api.instagram.com/v1/users/search?q='.$username.'&client_id='.$instagramClientId)->data,
        function ($e) use ($username) {
          if ($e->username == $username)
            return $e;
        }
        );

      if (!isset($profileId[0]->id))
        return true;

      $profileResponse = $this->getUrlContent('https://api.instagram.com/v1/users/'.$profileId[0]->id.'/media/recent/?client_id='.$instagramClientId);

      if ($profileResponse->meta->code == '400')
        return true;
    }

    public function homeAction($apiId, $token='1234')
    {

        // Saving user media
      $slides_url = "http://dash.reshape.net/api/ajax/profile/showcase?token=$token&id=$apiId";
      $response_slides = $this->getUrlContent($slides_url);

      $slides = array();
      if (!isset($response_slides->error) || $response_slides->error == 'true') {
        $error[] = 'Requested URL Error Slides.';
      } else {
        foreach ($response_slides->slides as $slide) {
          $slides[] = $slide;
        }
      }

      return $this->render('ReshapeBundle:ProfessionalUser:home.html.twig', array(
        'slides' => $slides,
        'userInfo' => $this->userInfo($apiId),
        'userImgs' => $this->userImgs($apiId),
        'social' => $this->userSocial($apiId)
        ));
    }

    public function bioAction($apiId, $token='1234')
    {

        // Saving user media
      $slides_url = "http://dash.reshape.net/api/ajax/profile/showcase?token=$token&id=$apiId";
      $response_slides = $this->getUrlContent($slides_url);

      $slides = array();
      if (!isset($response_slides->error) || $response_slides->error == 'true') {
        $error[] = 'Requested URL Error Slides.';
      } else {
        foreach ($response_slides->slides as $slide) {
          $slides[] = $slide;
        }
      }

      // $cert = $this->userClassif($apiId);

      // print_r($cert);
      // exit();

      return $this->render('ReshapeBundle:ProfessionalUser:bio.html.twig', array(
        'slides' => $slides,
        'userInfo' => $this->userInfo($apiId),
        'userImgs' => $this->userImgs($apiId),
        'social' => $this->userSocial($apiId),
        'userClassif' => $this->userClassif($apiId)
        ));
    }

    public function mediaAction($apiId, $token='1234')
    {

        // Saving user media
      $media_url = "http://dash.reshape.net/api/ajax/profile/media?token=$token&id=$apiId&itemSquareThumb=true";
      $response_media = $this->getUrlContent($media_url);

      $albums = array();
      if (!isset($response_media->error) || $response_media->error == 'true') {
        $error[] = 'Requested URL Error Media.';
      } else {
        $albums = $response_media->albums;
        foreach ($albums as $album) {
          shuffle($album->albumItems);
        }
      }

      return $this->render('ReshapeBundle:ProfessionalUser:media.html.twig', array(
        'albums' => $albums,
        'albums_json' => json_encode($albums),
        'userInfo' => $this->userInfo($apiId),
        'social' => $this->userSocial($apiId)
        ));
    }

    private function getUrlContent($url)
    {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      if (curl_exec($ch) !== false)
        return json_decode(curl_exec($ch));

      throw $this->createNotFoundException('Response Error in: ' . $url);
    }

    private function userInfo($apiId, $token='1234') {
      $userInfo_url = "http://dash.reshape.net/api/ajax/profile/page?token=$token&id=$apiId";
      $response_userInfo = $this->getUrlContent($userInfo_url);

      $userInfo = array();
      if (!isset($response_userInfo->error) || $response_userInfo->error == 'true') {
        $error[] = 'Requested URL Error Page Info.';
      } else {
        $userInfo = $response_userInfo->pageInfo->pageData;
      }

      return $userInfo;
    }

    private function userClassif($apiId, $token='1234') {
      $userClassif_url = "http://dash.reshape.net/api/ajax/profile/classif?token=$token&id=$apiId";
      $response_userClassif = $this->getUrlContent($userClassif_url);

      $userClassif = array();
      if (!isset($response_userClassif->error) || $response_userClassif->error == 'true') {
        $error[] = 'Requested URL Error Page Info.';
      } else {
        $userClassif = $response_userClassif->attrGroups;
      }
      return $userClassif;
    }

    private function userImgs($apiId, $token='1234') {
      $userImgs_url = "http://dash.reshape.net/api/ajax/profile/info?token=$token&id=$apiId";
      $response_userImgs = $this->getUrlContent($userImgs_url);

      $userImgs = array();
      if (!isset($response_userImgs->error) || $response_userImgs->error == 'true') {
        $error[] = 'Requested URL Error Page Imgs.';
      } else {
        $userImgs = $response_userImgs;
      }

      return $userImgs;
    }

    private function userSocial($apiId, $token='1234') {
        // Saving user media
      $social_url = "http://dash.reshape.net/api/ajax/profile/social?token=$token&id=$apiId";
      $response_social = $this->getUrlContent($social_url);

      $socials = array();
      if (!isset($response_social->error) || $response_social->error == 'true') {
        $error[] = 'Requested URL Error Socials.';
      } else {
        foreach ($response_social->socialMedia->accounts as $s) {
          if ($s->isActive == 'true') {
            $socials[$s->label] = $s;
            if ($s->label == 'Facebook') {
              $fb = "https://graph.facebook.com/$s->label";
              $response_fb = $this->getUrlContent($fb);
              $s->id = $response_fb->id;
            }
          }
        }
      }

      return $socials;
    }
  }
