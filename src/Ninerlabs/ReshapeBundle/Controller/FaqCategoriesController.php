<?php

namespace Ninerlabs\ReshapeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Ninerlabs\ReshapeBundle\Entity\FaqCategories;
use Ninerlabs\ReshapeBundle\Form\FaqCategoriesType;

/**
 * FaqCategories controller.
 *
 */
class FaqCategoriesController extends Controller
{

    /**
     * Lists all FaqCategories entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ReshapeBundle:FaqCategories')->findAll();

        return $this->render('ReshapeBundle:FaqCategories:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new FaqCategories entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new FaqCategories();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('faq-categories_show', array('id' => $entity->getId())));
        }

        return $this->render('ReshapeBundle:FaqCategories:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a FaqCategories entity.
     *
     * @param FaqCategories $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(FaqCategories $entity)
    {
        $form = $this->createForm(new FaqCategoriesType(), $entity, array(
            'action' => $this->generateUrl('faq-categories_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new FaqCategories entity.
     *
     */
    public function newAction()
    {
        $entity = new FaqCategories();
        $form   = $this->createCreateForm($entity);

        return $this->render('ReshapeBundle:FaqCategories:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a FaqCategories entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ReshapeBundle:FaqCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FaqCategories entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ReshapeBundle:FaqCategories:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing FaqCategories entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ReshapeBundle:FaqCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FaqCategories entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ReshapeBundle:FaqCategories:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a FaqCategories entity.
    *
    * @param FaqCategories $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(FaqCategories $entity)
    {
        $form = $this->createForm(new FaqCategoriesType(), $entity, array(
            'action' => $this->generateUrl('faq-categories_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing FaqCategories entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ReshapeBundle:FaqCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FaqCategories entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('faq-categories_edit', array('id' => $id)));
        }

        return $this->render('ReshapeBundle:FaqCategories:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a FaqCategories entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ReshapeBundle:FaqCategories')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find FaqCategories entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('faq-categories'));
    }

    /**
     * Creates a form to delete a FaqCategories entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('faq-categories_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
