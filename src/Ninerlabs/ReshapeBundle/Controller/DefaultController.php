<?php

namespace Ninerlabs\ReshapeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $user = $this->getUser();

        if (!$user || $user->getComplete())
            return $this->render('ReshapeBundle:Default:index.html.twig');
        else
            return $this->redirect($this->generateUrl('professionaluser_edit', array('id' => $user->getId())));
    }

    private function emailTest()
    {
        $em = $this->getDoctrine()->getManager();

        $email = 'emorales@ninerlabs.com';

        $emailController = new EmailController($em, $this->get('kernel')->getRootDir());
        $emailController->EmailTest($email);
    }
}
