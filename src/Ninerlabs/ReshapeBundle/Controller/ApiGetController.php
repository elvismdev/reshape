<?php

namespace Ninerlabs\ReshapeBundle\Controller;

use Ninerlabs\ReshapeBundle\Entity\Album;
use Ninerlabs\ReshapeBundle\Entity\AlbumItem;
use Ninerlabs\ReshapeBundle\Entity\Albums;
use Ninerlabs\ReshapeBundle\Entity\Albumitems;
use Ninerlabs\ReshapeBundle\Entity\AttrGroup;
use Ninerlabs\ReshapeBundle\Entity\ProfessionalUser;
use Ninerlabs\ReshapeBundle\Entity\ProfessionalUserClassification;
use Ninerlabs\ReshapeBundle\Entity\ServiceCode;
use Ninerlabs\ReshapeBundle\Entity\Service;
use Ninerlabs\ReshapeBundle\Entity\Slide;
use Ninerlabs\ReshapeBundle\Entity\Social;
use Ninerlabs\ReshapeBundle\Entity\UserLine;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class ApiGetController extends Controller
{
    public function getfromapiAction($token)
    {
        set_time_limit(3600); // 60 minutes

        $error = array();
        $em = $this->getDoctrine()->getManager();

        $this->importUsers($token);

        $total = $em->createQuery('SELECT count(u) FROM ReshapeBundle:ProfessionalUser u WHERE u.apiId > 0')->getSingleScalarResult();

        for ($i = 0; $i <= $total; $i += 20) {
            $error[] = $this->importUsersAssets($token, $total, $i, 20);
        }

        return new Response('<h1>Import process... finished!</h1>');
    }

    public function createAlbumAction(Request $request) {
        set_time_limit(3600); // 60 minutes
        $now = new \ DateTime('now');

        $token = $request->request->get('token', '');
        $userId = $request->request->get('userId', 0);
        $albumName = $request->request->get('albumName');
        $albumDesc = $request->request->get('albumDesc');
        $isActive = $request->request->get('isActive', 'true');

        if (!$this->isValidStr($token) || !$this->isValidStr($userId) || !$this->isValidStr($albumName)) {
            $error = array(
                'error' => true,
                'error_message' => 'Required parameters missing',
                'responseTime' => $now->format('c'),
                'origin' => ($request->headers->get('Origin') ? $request->headers->get('Origin') : '*')
                );

            return $this->returnJsonResponse($error);
        }

        $em = $this->getDoctrine()->getManager('db2');

        $isActive = ($isActive == 'true') ? true : false;

        $album = $em->getRepository('ReshapeBundle:Albums')->findOneBy(array('albumownerid' => $userId, 'name' => $albumName));
        if ($album) {
            $error = array(
                'error' => true,
                'error_message' => 'Album alredy exist',
                'responseTime' => $now->format('c'),
                'origin' => ($request->headers->get('Origin') ? $request->headers->get('Origin') : '*')
                );

            return $this->returnJsonResponse($error);
        }

        $album = new Albums();
        $album->setName($albumName);
        $album->setDescrShort($albumDesc);
        $album->setIsactive($isActive);
        $album->setRunnedon($now);
        $album->setCreatedon($now);
        $album->setTakenon($now);
        $album->setAlbumownerid($userId);
        $em->persist($album);
        $em->flush();
        $album->setIdalbum($album->getId());
        $em->persist($album);
        $em->flush();

        $result['error'] = false;
        $result['Album']['id'] = $album->getId();
        $result['Album']['name'] = $album->getName();
        $result['Album']['description'] = $album->getDescrShort();
        $result['Album']['isActive'] = $album->getIsactive();
        $result['Album']['userId'] = $album->getAlbumownerid();
        $result['responseTime'] = $now->format('c');
        $result['origin'] = ($request->headers->get('Origin') ? $request->headers->get('Origin') : '*');

        return $this->returnJsonResponse($result);
    }

    public function editAlbumAction(Request $request) {
        set_time_limit(3600); // 60 minutes
        $now = new \ DateTime('now');

        $token = $request->request->get('token', '');
        $userId = $request->request->get('userId', 0);
        $albumId = $request->request->get('albumId', 0);
        $albumName = $request->request->get('albumName');
        $albumDesc = $request->request->get('albumDesc');
        $isActive = $request->request->get('isActive', 'true');
        $delete = $request->request->get('delete', false);

        if ($delete) {
            if (!$this->isValidStr($token) || !$this->isValidStr($userId) || !$this->isValidStr($albumId)) {
                $error = array(
                    'error' => true,
                    'error_message' => 'Required parameters to delete are missing',
                    'responseTime' => $now->format('c'),
                    'origin' => ($request->headers->get('Origin') ? $request->headers->get('Origin') : '*')
                    );

                return $this->returnJsonResponse($error);
            }
        } else {
            if (!$this->isValidStr($token) || !$this->isValidStr($userId) || !$this->isValidStr($albumId) || !$this->isValidStr($albumName)) {
                $error = array(
                    'error' => true,
                    'error_message' => 'Required parameters to edit are missing',
                    'responseTime' => $now->format('c'),
                    'origin' => ($request->headers->get('Origin') ? $request->headers->get('Origin') : '*')
                    );

                return $this->returnJsonResponse($error);
            }
        }

        $em = $this->getDoctrine()->getManager('db2');

        $isActive = ($isActive == 'true') ? true : false;

        $album = $em->getRepository('ReshapeBundle:Albums')->findOneBy(array('albumownerid' => $userId, 'idalbum' => $albumId));
        if (!$album) {
            $error = array(
                'error' => true,
                'error_message' => 'Album doesnt exist or you are not allowed to modify it',
                'responseTime' => $now->format('c'),
                'origin' => ($request->headers->get('Origin') ? $request->headers->get('Origin') : '*')
                );

            return $this->returnJsonResponse($error);
        }

        $result['error'] = false;
        $result['Album']['id'] = $album->getId();
        $result['Album']['albumid'] = $album->getIdalbum();
        $result['Album']['Name'] = $album->getName();
        $result['Album']['Description'] = $album->getDescrShort();
        $result['Album']['IsActive'] = $album->getIsactive();
        $result['Album']['UserId'] = $album->getAlbumownerid();
        $result['responseTime'] = $now->format('c');
        $result['origin'] = ($request->headers->get('Origin') ? $request->headers->get('Origin') : '*');

        if ($delete) {
            $albumitems = $em->getRepository('ReshapeBundle:Albumitems')->findByAlbum($album->getId());
            $fs = new Filesystem();
            $dir = $this->get('kernel')->getRootDir() . '/../web';
            foreach ($albumitems as $i) {
                if ($this->isValidStr($i->getThumbLocalPath()) && $fs->exists($dir . $i->getThumbLocalPath()) && ($dir . $i->getThumbLocalPath() != $dir)) {
                    $fs->remove($dir . $i->getThumbLocalPath());
                }

                if ($this->isValidStr($i->getFullSizeLocalPath()) && $fs->exists($dir . $i->getFullSizeLocalPath()) && ($dir . $i->getFullSizeLocalPath() != $dir)) {
                    $fs->remove($dir . $i->getFullSizeLocalPath());
                }
            }

            $q = $em->createQuery('DELETE FROM ReshapeBundle:Albumitems a WHERE a.album = :album')->setParameter('album', $album->getId());
            $numDeleted = $q->execute();

            $em->remove($album);
            $em->flush();

            return $this->returnJsonResponse($result);
        }

        $album->setName($albumName);
        if ($this->isValidStr($albumDesc)) $album->setDescrShort($albumDesc);
        $album->setIsactive($isActive);
        $album->setRunnedon($now);
        $album->setAlbumownerid($userId);
        $em->persist($album);
        $em->flush();

        return $this->returnJsonResponse($result);
    }

    public function uploadPicAction(Request $request) {
        $now = new \ DateTime('now');
        $path = '/bundles/reshape/importAPI/media/';
        $dir = $this->get('kernel')->getRootDir() . '/../web' . $path;
        $baseurl = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . $this->getRequest()->getBasePath();

        $token = $request->request->get('token', '');
        $userId = $request->request->get('userId', 0);
        $albumId = $request->request->get('albumId', 0);
        $desc = $request->request->get('desc');
        $videoId = $request->request->get('videoId');
        $setAsCover = $request->request->get('setAsCover', 'false');

        if (!$this->isValidStr($token) || !$this->isValidStr($userId) || !$this->isValidStr($albumId)) {
            $error = array(
                'error' => true,
                'error_message' => 'Required parameters missing',
                'responseTime' => $now->format('c'),
                'origin' => ($request->headers->get('Origin') ? $request->headers->get('Origin') : '*')
                );

            return $this->returnJsonResponse($error);
        }

        $em = $this->getDoctrine()->getManager('db2');

        $album = $em->getRepository('ReshapeBundle:Albums')->findOneBy(array('albumownerid' => $userId, 'idalbum' => $albumId));
        if (!$album) {
            $error = array(
                'error' => true,
                'error_message' => 'Album doesnt exist or youre not allow to delete it',
                'responseTime' => $now->format('c'),
                'origin' => ($request->headers->get('Origin') ? $request->headers->get('Origin') : '*')
                );

            return $this->returnJsonResponse($error);
        }

        // $form = $this->createFormBuilder()
        // ->add('attachment', 'file')
        // ->add('Submit', 'submit');
        // $form = $form->getForm();

        // // if ($this->getRequest()->isMethod('POST')) {
        // $form->handleRequest($request);
        // $file = $form['attachment']->getData();

        // if (!strstr($file->getMimeType(), 'image')) {
        //     $error = array(
        //         'error' => true,
        //         'error_message' => 'Only images can be upload',
        //         'responseTime' => $now->format('c'),
        //         'origin' => ($request->headers->get('Origin') ? $request->headers->get('Origin') : '*')
        //         );

        //     return $this->returnJsonResponse($error);
        // }

        // $ext = $file->guessExtension();
        // if ($file->guessExtension() == 'jpeg')
        //     $ext = 'jpg';

        $item = new Albumitems();
        $item->setAlbum($album);
        $item->setDescription($desc);
        $item->setVideoid($videoId);
        $item->setIscover($setAsCover);
        $em->persist($item);
        $em->flush();

        $item->setIditem($item->getId());
        $em->persist($item);
        $em->flush();

        // $fs = new Filesystem();
        // exec('convert -density 150 -quality 100 -background white -alpha remove -resize 100x100' . ' ' . $file->getPathname() . ' ' . $dir . $userId . '/item_thumb' . $item->getIditem() . '.' . $ext, $result, $error);
        // $fs->copy($file->getPathname(), $dir . $userId . '/item_full_size' . $item->getIditem() . '.' . $ext);

        // list($width, $height) = getimagesize($dir . $userId . '/item_full_size' . $item->getIditem() . '.' . $ext);
        // $item->setFileext($file->guessExtension());
        // $item->setFilesize($file->getClientSize());
        $item->setDatetime($now);
        // $item->setFilewidth($width);
        // $item->setFileheight($height);
        // $item->setThumblocalpath($path . $userId . '/item_thumb' . $item->getIditem() . '.' . $ext);
        // $item->setThumburllocal($baseurl . $this->container->get('templating.helper.assets')->getUrl($path . $userId . '/item_thumb' . $item->getIditem() . '.' . $ext));
        // $item->setFullsizelocalpath($path . $userId . '/item_full_size' . $item->getIditem() . '.' . $ext);
        // $item->setFullurllocal($baseurl . $this->container->get('templating.helper.assets')->getUrl($path . $userId . '/item_full_size' . $item->getIditem() . '.' . $ext));
        $em->persist($item);
        $em->flush();

        $result['error'] = false;

        $result['AlbumItem']['id'] = $item->getId();
        $result['AlbumItem']['albumId'] = $item->getAlbum();
        // $result['AlbumItem']['description'] = $item->getDescription();
        // $result['AlbumItem']['videoId'] = $item->getVideoid();
        // $result['AlbumItem']['isCover'] = $item->getIscover();
        // $result['AlbumItem']['fileExt'] = $item->getFileext();
        // $result['AlbumItem']['fileSize'] = $item->getFilesize();
        // $result['AlbumItem']['dateTime'] = $item->getDatetime();
        // $result['AlbumItem']['fileWidth'] = $item->getFilewidth();
        // $result['AlbumItem']['fileHeight'] = $item->getFileheight();
        // $result['AlbumItem']['thumbLocalPath'] = $item->getThumblocalpath();
        // $result['AlbumItem']['thumbUrlLocal'] = $item->getThumburllocal();
        // $result['AlbumItem']['fullSizeLocalPath'] = $item->getFullsizelocalpath();
        // $result['AlbumItem']['fullUrlLocal'] = $item->getFullurllocal();

        $result['responseTime'] = $now->format('c');
        $result['origin'] = ($request->headers->get('Origin') ? $request->headers->get('Origin') : '*');

        return $this->returnJsonResponse($result);
        // } else {
        //     return $this->render('ReshapeBundle:Default:uploadPic.html.twig', array(
        //         'form' => $form->createView()
        //         ));
        // }
    }

    public function editPicAction(Request $request) {
        $now = new \ DateTime('now');
        $path = 'bundles/reshape/importAPI/media/';
        $dir = $this->get('kernel')->getRootDir() . '/../web/';
        $baseurl = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . $this->getRequest()->getBasePath();

        $token = $request->request->get('token', '');
        $userId = $request->request->get('userId', 0);
        $albumId = $request->request->get('albumId', 0);
        $picId = $request->request->get('picId', 0);
        $desc = $request->request->get('desc');
        $videoId = $request->request->get('videoId');
        $setAsCover = $request->request->get('setAsCover', 'false');
        $delete = $request->request->get('delete', false);
        $newAlbumId = $request->request->get('newAlbumId', false);

        if (!$this->isValidStr($token) || !$this->isValidStr($userId) || !$this->isValidStr($albumId) || !$this->isValidStr($picId)) {
            $error = array(
                'error' => true,
                'error_message' => 'Required parameters missing',
                'responseTime' => $now->format('c')
                );

            return $this->returnJsonResponse($error);
        }

        $em = $this->getDoctrine()->getManager('db2');

        $album = $em->getRepository('ReshapeBundle:Albums')->findOneBy(array('albumownerid' => $userId, 'idalbum' => $albumId));
        if (!$album) {
            $error = array(
                'error' => true,
                'error_message' => 'Album doesnt exist or youre not allow to delete it',
                'responseTime' => $now->format('c')
                );

            return $this->returnJsonResponse($error);
        }

        $item = $em->getRepository('ReshapeBundle:Albumitems')->findOneBy(array('iditem' => $picId, 'album' => $album->getId()));
        if (!$item) {
            $error = array(
                'error' => true,
                'error_message' => 'Picture doesnt belong to the Album especified',
                'responseTime' => $now->format('c')
                );

            return $this->returnJsonResponse($error);
        }

        if ($delete) {
            $fs = new Filesystem();

            if ($fs->exists($dir . $item->getThumblocalpath()))
                $fs->remove($dir . $item->getThumblocalpath());

            if ($fs->exists($dir . $item->getFullsizelocalpath()))
                $fs->remove($dir . $item->getFullsizelocalpath());

            $em->remove($item);
            $em->flush();

            return new Response('Deleted succefully');
        }

        if ($newAlbumId) {
            $album = $em->getRepository('ReshapeBundle:Albums')->findOneBy(array('albumownerid' => $userId, 'idalbum' => $newAlbumId));
            if (!$album) {
                $error = array(
                    'error' => true,
                    'error_message' => 'Destination Album doesnt exist or youre not allow to delete it',
                    'responseTime' => $now->format('c')
                    );

                return $this->returnJsonResponse($error);
            } else {
                $item->setAlbum($album);
                $em->persist($item);
                $em->flush();

                return new Response('Moved succefully');
            }
        }

        $form = $this->createFormBuilder()
        ->add('attachment', 'file')
        ->add('Submit', 'submit');
        $form = $form->getForm();

        if ($this->getRequest()->isMethod('POST')) {
            $form->handleRequest($request);
            $file = $form['attachment']->getData();

            if (!strstr($file->getMimeType(), 'image')) {
                $error = array(
                    'error' => true,
                    'error_message' => 'Only images can be upload',
                    'responseTime' => $now->format('c')
                    );

                return $this->returnJsonResponse($error);
            }

            $ext = $file->guessExtension();
            if ($file->guessExtension() == 'jpeg')
                $ext = 'jpg';

            $item->setDescription($desc);
            $item->setVideoid($videoId);
            $item->setIscover($setAsCover);
            $em->persist($item);
            $em->flush();

            $fs = new Filesystem();
            exec('convert -density 150 -quality 100 -background white -alpha remove -resize 100x100' . ' ' . $file->getPathname() . ' ' . $dir . $path . $userId . '/item_thumb' . $item->getIditem() . '.' . $ext, $result, $error);
            $fs->copy($file->getPathname(), $dir . $path . $userId . '/item_full_size' . $item->getIditem() . '.' . $ext);

            list($width, $height) = getimagesize($dir . $path . $userId . '/item_full_size' . $item->getIditem() . '.' . $ext);
            $item->setFileext($file->guessExtension());
            $item->setFilesize($file->getClientSize());
            $item->setDatetime($now);
            $item->setFilewidth($width);
            $item->setFileheight($height);
            $item->setThumblocalpath($path . $userId . '/item_thumb' . $item->getIditem() . '.' . $ext);
            $item->setThumburllocal($baseurl . $this->container->get('templating.helper.assets')->getUrl($path . $userId . '/item_thumb' . $item->getIditem() . '.' . $ext));
            $item->setFullsizelocalpath($path . $userId . '/item_full_size' . $item->getIditem() . '.' . $ext);
            $item->setFullurllocal($baseurl . $this->container->get('templating.helper.assets')->getUrl($path . $userId . '/item_full_size' . $item->getIditem() . '.' . $ext));
            $em->persist($item);
            $em->flush();

            return new Response('Uploaded and modified succefully');
        } else {
            return $this->render('ReshapeBundle:Default:uploadPic.html.twig', array(
                'form' => $form->createView(),
                'album' => $album,
                'item' => $item
                ));
        }
    }

    private function returnJsonResponse($result) {
        $response = new Response();
        $response->headers->set('Access-Control-Allow-Credentials', 'true');
        $response->headers->set('Access-Control-Allow-Origin', $result['origin']);
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');
        $response->setContent(json_encode($result));

        return $response;
    }

    private function isValidStr($str) {
        $str = urldecode($str);

        if (is_null($str) || empty($str) || ctype_space($str))
            return false;
        return true;
    }

    public function getAjaxMediaAction($token='1234', $userId) {
        set_time_limit(3600); // 60 minutes

        $em = $this->getDoctrine()->getManager('db2');

        $albums_ = $em->getRepository('ReshapeBundle:Albums')->findByAlbumownerid($userId);

        if (!$albums_)
            return new Response('<h1>No user found!</h1>');

        $albums = array();
        foreach ($albums_ as $album) {
            $a['id'] = $album->getIdalbum();
            $a['name'] = $album->getName();
            $a['tagLine'] = $album->getTagLine();
            $a['descr_short'] = $album->getDescrshort();
            $a['picture_url'] = $album->getPictureurl();
            $a['video_id'] = $album->getVideoid();
            $a['logo_url'] = $album->getLogourl();
            $a['profile_url'] = $album->getProfileurl();
            $a['sortPriority'] = $album->getSortpriority();
            $a['isActive'] = $album->getIsactive();
            $a['coverPic'] = null;
            $a['albumSortPriority'] = $album->getAlbumsortpriority();
            $a['createdOn'] = $album->getCreatedon()->format('c');
            $a['takenOn'] = $album->getTakenon()->format('c');
            $a['albumOwnerId'] = $album->getAlbumownerid();
            $a['albumDescription'] = $album->getAlbumdescription();

            $albumitems_ = $em->getRepository('ReshapeBundle:Albumitems')->findByAlbum($album);
            $items = array();

            if ($albumitems_) {
                foreach ($albumitems_ as $item) {
                    $i['id'] = $item->getIditem();
                    $i['videoId'] = $item->getVideoid();
                    $i['description'] = $item->getDescription();
                    $i['dateTime'] = $item->getDateTime()->format('c');
                    $i['thumbUrl'] = $item->getThumburllocal();
                    $i['fullSizeUrl'] = $item->getFullurllocal();
                    $i['fileWidth'] = $item->getFilewidth();
                    $i['fileHeight'] = $item->getFileheight();
                    $i['fileSize'] = $item->getFilesize();
                    $i['fileName'] = $item->getFileName();
                    $i['fileExt'] = $item->getFileext();
                    if ($item->getIscover()) {
                        $coverpic = new \stdClass();
                        $coverpic->id = $item->getIditem();
                        $coverpic->videoId = $item->getVideoid();
                        $coverpic->description = $item->getDescription();
                        $coverpic->dateTime = $item->getDateTime()->format('c');
                        $coverpic->thumbUrl = $item->getThumburllocal();
                        $coverpic->fullSizeUrl = $item->getFullurllocal();
                        $coverpic->fileWidth = $item->getFilewidth();
                        $coverpic->fileHeight = $item->getFileheight();
                        $coverpic->fileSize = $item->getFilesize();
                        $coverpic->fileName = $item->getFileName();
                        $coverpic->fileExt = $item->getFileext();
                        $a['coverPic'] = $coverpic;
                        $a['picture_url'] = $item->getFullurllocal();
                    }
                    $i['isCover'] = $item->getIscover();
                    $items[] = $i;
                }
            }
            $a['albumItems'] = $items;
            $albums ['albums'][] = $a;
        }

        $response = new Response();
        $output = array('success' => true);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($albums));

        return $response;
    }

    public function getMediaAction($userId, $token='1234') {
        set_time_limit(3600); // 60 minutes

        $em = $this->getDoctrine()->getManager('db2');
        $baseurl = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . $this->getRequest()->getBasePath();

        // Saving user albums
        $albums_url = "http://dash.reshape.net/api/ajax/profile/media?token=$token&id=$userId&isActive=null&itemSquareThumb=true";
        $response_albums = $this->getUrlContent($albums_url);

        if (!isset($response_albums->error) || $response_albums->error == 'true') {
            return new Response('<h1>Requested URL Error Albums.!</h1>');
        }

        foreach ($response_albums->albums as $a) {
            $album = $em->getRepository('ReshapeBundle:Albums')->findOneByIdalbum($a->id);
            $path = '/bundles/reshape/importAPI/media/';

            if (!$album) {
                $album = new Albums();
            }

            $album->setIdalbum($a->id);
            $album->setName($a->name);
            $album->setTagline($a->tagLine);
            $album->setDescrShort($a->descr_short);
            $album->setPictureUrl($a->picture_url);
            $album->setVideoId($a->video_id);
            $album->setLogoUrl($a->logo_url);
            $album->setProfileUrl($a->profile_url);
            $album->setSortpriority($a->sortPriority);
            $album->setIsactive(($a->isActive == 'true') ? true : false);
            $album->setCoverpic(json_encode($a->coverPic, JSON_UNESCAPED_SLASHES));
            $album->setAlbumsortpriority($a->albumSortPriority);
            $album->setCreatedon(new \DateTime($a->createdOn));
            $album->setTakenon(new \DateTime($a->takenOn));
            $album->setAlbumownerid($a->albumOwnerId);
            $album->setAlbumdescription($a->albumDescription);
            $album->setJsonObjectDump(json_encode($response_albums, JSON_UNESCAPED_SLASHES));
            $album->setRunnedOn(new \DateTime('now'));

            $em->persist($album);
            $em->flush();

            if ($file = $this->download_image($album->getPictureUrl(), $userId, 'album' . $album->getIdalbum(), $path)) {
                $album->setLocalpath($path . $userId . $file);
                $em->persist($album);
                $em->flush();
            }

            foreach ($a->albumItems as $i) {
                $item = $em->getRepository('ReshapeBundle:Albumitems')->findOneByIditem($i->id);

                if (!$item) {
                    $item = new AlbumItems();
                }

                $item->setIditem($i->id);
                $item->setVideoId($i->videoId);
                $item->setDescription($i->description);
                $item->setDatetime(new \DateTime($i->dateTime));
                $item->setThumburl($i->thumbUrl);
                $item->setFullsizeurl($i->fullSizeUrl);
                $item->setFilewidth($i->fileWidth);
                $item->setFileheight($i->fileHeight);
                $item->setFilesize($i->fileSize);
                $item->setFilename($i->fileName);
                $item->setFileext($i->fileExt);
                $item->setIscover(($i->isCover == 'true') ? true : false);
                $item->setAlbum($album);

                $em->persist($item);
                $em->flush();

                if ($file = $this->download_image($item->getThumburl(), $userId, 'item_thumb' . $item->getIditem(), $path)) {
                    $ext = $item->getFileext();
                    if ($ext == 'jpeg') $ext = 'jpg';

                    $item->setThumblocalpath($path . $userId. $file);
                    $item->setThumburllocal($baseurl . $this->container->get('templating.helper.assets')->getUrl($path . $userId . '/item_thumb' . $item->getIditem() . '.' . $ext));
                }
                if ($file = $this->download_image($item->getFullsizeurl(), $userId, 'item_full_size' . $item->getIditem(), $path)) {
                    $ext = $item->getFileext();
                    if ($ext == 'jpeg') $ext = 'jpg';

                    $item->setFullsizelocalpath($path . $userId . $file);
                    $item->setFullurllocal($baseurl . $this->container->get('templating.helper.assets')->getUrl($path . $userId . '/item_full_size' . $item->getIditem() . '.' . $ext));
                }

                $em->persist($item);
                $em->flush();
            }
        }

        return new Response('<h1>Import process... finished!</h1>');
    }

    private function importUsersAssets($token, $total, $start = 0, $limit = 20)
    {
        $error = array();

        $em = $this->getDoctrine()->getManager();

        $users = $em->createQuery('SELECT u FROM ReshapeBundle:ProfessionalUser u WHERE u.apiId > 0')
        ->setFirstResult($start)
        ->setMaxResults($limit)
        ->getResult();

        foreach ($users as $user) {
            $uid = $user->getApiId();

            // Saving user slides
            $slides_url = "http://dash.reshape.net/api/ajax/profile/showcase?token=$token&id=$uid";
            $response_slides = $this->getUrlContent($slides_url);

            if (!isset($response_slides->error) || $response_slides->error == 'true') {
                $error[] = 'Requested URL Error Slides.';
                continue;
            }

            foreach ($response_slides->slides as $s) {
                $slide = $em->getRepository('ReshapeBundle:Slide')->findOneByTitle($s->title);

                if ($slide) {
                    if ($file = $this->download_image($slide->getPictureUrl(), $user->getId(), 'slide' . $slide->getId())) {
                        $slide->setLocalPictureUrl('/bundles/reshape/profile/' . $user->getId() . $file);
                        $em->persist($slide);
                        $em->flush();
                    }

                    continue;
                }

                $slide = new Slide();
                $slide->setTitle($s->title);
                $slide->setDescription($s->description);
                $slide->setSortOrder($s->sortorder);
                $slide->setPictureUrl($s->picture_url);
                $slide->setVideoId($s->video_id);
                $slide->setUserId($user);

                $em->persist($slide);
                $em->flush();

                if ($file = $this->download_image($slide->getPictureUrl(), $user->getId(), 'slide' . $slide->getId())) {
                    $slide->setLocalPictureUrl('/bundles/reshape/profile/' . $user->getId() . $file);
                    $em->persist($slide);
                    $em->flush();
                }
            }

            // Saving user services
            $services_url = "http://dash.reshape.net/api/ajax/profile/services?token=$token&id=$uid";
            $response_services = $this->getUrlContent($services_url);

            if (!isset($response_services->error) || $response_services->error == 'true') {
                $error[] = 'Requested URL Error Services.';
                continue;
            }

            foreach ($response_services->serviceCodes as $c) {
                $code = $em->getRepository('ReshapeBundle:ServiceCode')->findOneByCode($c->code);

                if ($code) {
                    continue;
                }

                $code = new ServiceCode();
                $code->setCode($c->code);
                $code->setName($c->name);

                $em->persist($code);
            }
            $em->flush();

            foreach ($response_services->services as $s) {
                $service = $em->getRepository('ReshapeBundle:Service')->findOneByName($s->name);

                if ($service) {
                    if ($file = $this->download_image($service->getPictureUrl(), $user->getId(), 'service_picture' . $service->getId()))
                        $service->setLocalPictureUrl('/bundles/reshape/profile/' . $user->getId() . $file);
                    if ($file = $this->download_image($service->getLogoUrl(), $user->getId(), 'service_logo' . $service->getId()))
                        $service->setLocalLogoUrl('/bundles/reshape/profile/' . $user->getId() . $file);

                    continue;
                }

                $service = new Service();
                $service->setName($s->name);
                $code = $em->getRepository('ReshapeBundle:ServiceCode')->findOneByCode($s->typeCode);
                $service->setServiceCode($code);
                $service->setPrice($s->price);
                $service->setMinutes($s->minutes);
                $service->setDescription($s->description);
                $service->setSortOrder($s->sortorder);
                $service->setPictureUrl($s->picture_url);
                $service->setLogoUrl($s->logo_url);
                $service->setUserId($user);

                $em->persist($service);
                $em->flush();

                if ($file = $this->download_image($service->getPictureUrl(), $user->getId(), 'service_picture' . $service->getId()))
                    $service->setLocalPictureUrl('/bundles/reshape/profile/' . $user->getId() . $file);
                if ($file = $this->download_image($service->getLogoUrl(), $user->getId(), 'service_logo' . $service->getId()))
                    $service->setLocalLogoUrl('/bundles/reshape/profile/' . $user->getId() . $file);

                $em->persist($service);
                $em->flush();
            }

            // Saving user profile classification
            $classifications_url = "http://dash.reshape.net/api/ajax/profile/classif?token=$token&id=$uid";
            $response_classifications = $this->getUrlContent($classifications_url);

            if (!isset($response_classifications->error) || $response_classifications->error == 'true') {
                $error[] = 'Requested URL Error Classifications.';
                continue;
            }

            foreach ($response_classifications->attrGroups as $c) {
                $attrGroup = $em->getRepository('ReshapeBundle:AttrGroup')->findOneByName($c->name);

                if (!$attrGroup) {
                    $attrGroup = new AttrGroup();
                    $attrGroup->setName($c->name);
                    $attrGroup->setDescrShort($c->descr_short);
                    $attrGroup->setPictureUrl($c->picture_url);
                    $attrGroup->setLogoUrl($c->logo_url);
                    $attrGroup->setProfileUrl($c->profile_url);
                    $attrGroup->setSortPriority($c->sortPriority);

                    $em->persist($attrGroup);
                    $em->flush();
                }

                if ($file = $this->download_image($attrGroup->getPictureUrl(), $user->getId(), 'group' . $attrGroup->getId())) {
                    $attrGroup->setLocalPictureUrl('/bundles/reshape/profile/' . $user->getId() . $file);
                    $em->persist($attrGroup);
                    $em->flush();
                }

                foreach ($c->attributes as $s) {
                    $son = $em->getRepository('ReshapeBundle:AttrGroup')->findOneBy(
                        array(
                            'name' => $s->name,
                            'attrGroupParentId' => $attrGroup->getId())
                        );

                    if ($son) {
                        if ($file = $this->download_image($son->getPictureUrl(), $user->getId(), 'group' . $son->getId())) {
                            $son->setLocalPictureUrl('/bundles/reshape/profile/' . $user->getId() . $file);
                            $em->persist($son);
                            $em->flush();
                        }

                        continue;
                    } else {
                        $son = new AttrGroup();
                        $son->setName($s->name);
                        $son->setDescrShort($s->descr_short);
                        $son->setPictureUrl($s->picture_url);
                        $son->setLogoUrl($s->logo_url);
                        $son->setProfileUrl($s->profile_url);
                        $son->setSortPriority($s->sortPriority);
                        $son->setAttrGroupParentId($attrGroup->getId());
                    }
                    $em->persist($son);
                    $em->flush();

                    if ($file = $this->download_image($son->getPictureUrl(), $user->getId(), 'group' . $son->getId())) {
                        $son->setLocalPictureUrl('/bundles/reshape/profile/' . $user->getId() . $file);
                        $em->persist($son);
                        $em->flush();
                    }
                }

                // Relation User<->Group
                $classification = $em->getRepository('ReshapeBundle:ProfessionalUserClassification')->findOneBy(
                    array(
                        'attrGroup' => $attrGroup,
                        'userid' => $user)
                    );

                if ($classification) {
                    continue;
                }

                $classification = new ProfessionalUserClassification();
                $classification->setAttrGroup($attrGroup);
                $classification->setUserid($user);

                $em->persist($classification);
                $em->flush();
            }

            // Saving user albums
            $albums_url = "http://dash.reshape.net/api/ajax/profile/media?token=$token&id=$uid";
            $response_albums = $this->getUrlContent($albums_url);

            if (!isset($response_albums->error) || $response_albums->error == 'true') {
                $error[] = 'Requested URL Error Albums.';
                continue;
            }

            foreach ($response_albums->albums as $a) {
                $album = $em->getRepository('ReshapeBundle:Album')->findOneByName($a->name);

                if (!$album) {
                    $album = new Album();
                    $album->setName($a->name);
                    $album->setDescrShort($a->descr_short);
                    $album->setPictureUrl($a->picture_url);
                    /*
                     * vide_id
                     * logo_url
                     */
                    $album->setProfileUrl($a->profile_url);
                    $album->setSortPriority($a->sortPriority);
                    $album->setAlbumSortPriority($a->albumSortPriority);
                    $album->setCreated($a->createdOn);
                    $album->setTaken($a->takenOn);
                    $album->setDescription($a->albumDescription);
                    $album->setUserid($user);

                    $em->persist($album);
                    $em->flush();
                }

                if ($file = $this->download_image($album->getPictureUrl(), $user->getId(), 'album' . $album->getId())) {
                    $album->setLocalPictureUrl('/bundles/reshape/profile/' . $user->getId() . $file);
                    $em->persist($album);
                    $em->flush();
                }

                foreach ($a->albumItems as $i) {
                    $item = $em->getRepository('ReshapeBundle:AlbumItem')->findOneByName($i->fileName);

                    if ($item) {
                        if ($file = $this->download_image($item->getThumbUrl(), $user->getId(), 'item_thumb' . $item->getId()))
                            $item->setLocalThumbUrl('/bundles/reshape/profile/' . $user->getId() . $file);
                        if ($file = $this->download_image($item->getFullSizeUrl(), $user->getId(), 'item_full_size' . $item->getId()))
                            $item->setLocalFullSizeUrl('/bundles/reshape/profile/' . $user->getId() . $file);

                        continue;
                    }

                    $item = new AlbumItem();
                    $item->setVideoId($i->videoId);
                    $item->setDescription($i->description);
                    $item->setDatetime($i->dateTime);
                    $item->setThumbUrl($i->thumbUrl);
                    $item->setFullSizeUrl($i->fullSizeUrl);
                    $item->setWidth($i->fileWidth);
                    $item->setHeight($i->fileHeight);
                    $item->setSize($i->fileSize);
                    $item->setName($i->fileName);
                    $item->setExtension($i->fileExt);
                    $item->setAlbum($album);

                    $em->persist($item);
                    $em->flush();

                    if ($file = $this->download_image($item->getThumbUrl(), $user->getId(), 'item_thumb' . $item->getId()))
                        $item->setLocalThumbUrl('/bundles/reshape/profile/' . $user->getId() . $file);
                    if ($file = $this->download_image($item->getFullSizeUrl(), $user->getId(), 'item_full_size' . $item->getId()))
                        $item->setLocalFullSizeUrl('/bundles/reshape/profile/' . $user->getId() . $file);

                    $em->persist($item);
                    $em->flush();
                }
            }

            // Saving user media
            $social_url = "http://dash.reshape.net/api/ajax/profile/social?token=$token&id=$uid";
            $response_social = $this->getUrlContent($social_url);

            if (!isset($response_social->error) || $response_social->error == 'true') {
                $error[] = 'Requested URL Error Socials.';
                continue;
            }

            foreach ($response_social->socialMedia->accounts as $s) {
                $social = $em->getRepository('ReshapeBundle:Social')->findOneBy(array('account' => $s->account, 'userid' => $user));

                if ($social) {
                    continue;
                }

                $social = new Social();
                $social->setLabel($s->label);
                $social->setAccount($s->account);
                $social->setKey($s->key);
                $social->setUserid($user);

                $em->persist($social);
            }
            $em->flush();

            // Saving user lines
            $full_url = "http://dash.reshape.net/api/ajax/profile/full?token=$token&id=$uid";
            $response_full = $this->getUrlContent($full_url);

            if (!isset($response_full->error) || $response_full->error == 'true') {
                $error[] = 'Requested URL Error Full Profile.';
                continue;
            }

            foreach ($response_full->page->pageInfo->pageData->lineItems as $i) {
                $line = $em->getRepository('ReshapeBundle:UserLine')->findOneBy(array('name' => $i->name, 'userid' => $user));

                if ($line) {
                    continue;
                }

                $line = new UserLine();
                $line->setName($i->name);
                $line->setType($i->type);
                $line->setUserid($user);

                $em->persist($line);
            }
            $em->flush();
        }

        return $error;
    }

    private function importUsers($token)
    {
        $em = $this->getDoctrine()->getManager();

        $users_url = "http://dash.reshape.net/api/ajax/search/activeUsers?token=$token";
        $response_users = $this->getUrlContent($users_url);

        if (!isset($response_users->error) || $response_users->error == 'true')
            throw $this->createNotFoundException('Requested URL Error Users.');

        foreach ($response_users->users as $u) {
            $user = $em->getRepository('ReshapeBundle:ProfessionalUser')->findOneByEmail($u->email);

            if (!$user) {
                $user = new ProfessionalUser();
            }

            // Saving single user
            $u_name = explode(' ', $u->name);

            $user->setFirstName($u_name[0]);
            $user->setLastName($u_name[1] ? $u_name[1] : $u_name[0]);
            $user->setDescription($u->descr_short);
            $user->setPhoto($u->picture_url);
            $user->setLogoUrl($u->logo_url);
            $user->setProfileUrl($u->profile_url);
            $user->setSortPriority($u->sortPriority);
            $user->setEmail($u->email);
            $user->setTc(0);
            $user->setSalt('');
            $user->setRol('ROLE_PROFESSIONAL_USER');
            $user->setStatus(false);
            $user->setCreated($_SERVER["REQUEST_TIME"]);
            $user->setComplete(false);
            $user->setPassword('12345');
            $this->setSecurePassword($user);
            $user->setApiId($u->id);

            $em->persist($user);
            $em->flush();

            if ($file = $this->download_image($user->getPhoto(), $user->getId(), 'photo'))
                $user->setPhotoLocal('/bundles/reshape/profile/' . $user->getId() . $file);
            if ($file = $this->download_image($user->getLogoUrl(), $user->getId(), 'logo'))
                $user->setLogoLocal('/bundles/reshape/profile/' . $user->getId() . $file);

            $em->persist($user);
            $em->flush();
        }
    }

    private function getUrlContent($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (curl_exec($ch) !== false)
            return json_decode(curl_exec($ch));

        throw $this->createNotFoundException('Response Error in: ' . $url);
    }

    private function setSecurePassword(&$entity)
    {
        $entity->setSalt(md5(time()));
        $encoder = new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('sha512', true, 10);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }

    private function download_image($image_url, $folder, $image_file, $path='/bundles/reshape/profile/')
    {
        $extension = pathinfo($image_url);
        if (!isset($extension['extension']))
            $extension = '.jpg';
        else
            $extension = '.' . strtolower($extension['extension']);

        $dir = $this->get('kernel')->getRootDir() . '/../web' . $path . $folder;
        $dir_file = '/' . $image_file . $extension;

        $fs = new Filesystem();

        if (!$fs->exists($dir))
            $fs->mkdir($dir);

        if ($fs->exists($dir . $dir_file)) {
            $fs->remove($dir . $dir_file);
        }

        $fp = fopen($dir . $dir_file, 'w+'); // open file handle

        $ch = curl_init($image_url);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // enable if you want
        curl_setopt($ch, CURLOPT_FILE, $fp); // output to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000); // some large value to allow curl to run for a long time
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
        // curl_setopt($ch, CURLOPT_VERBOSE, true);   // Enable this line to see debug prints
        curl_exec($ch);

        curl_close($ch); // closing curl handle
        fclose($fp); // closing file handle

        return $dir_file;
    }
}
